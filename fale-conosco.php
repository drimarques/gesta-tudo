<?php
$arquivo = $_GET["arquivo"];
if (isset($arquivo) && file_exists($arquivo)) {
    switch (strtolower(substr(strrchr(basename($arquivo), "."), 1))) {
        case "pdf": $tipo = "application/pdf";
            break;
        case "exe": $tipo = "application/octet-stream";
            break;
        case "zip": $tipo = "application/zip";
            break;
        case "doc": $tipo = "application/msword";
            break;
        case "xls": $tipo = "application/vnd.ms-excel";
            break;
        case "ppt": $tipo = "application/vnd.ms-powerpoint";
            break;
        case "gif": $tipo = "image/gif";
            break;
        case "png": $tipo = "image/png";
            break;
        case "jpg": $tipo = "image/jpg";
            break;
        case "mp3": $tipo = "audio/mpeg";
            break;
        case "php": // deixar vazio por seurança
        case "htm": // deixar vazio por seurança
        case "html": // deixar vazio por seurança
    }
    header("Content-Type: " . $tipo); // informa o tipo do arquivo ao navegador
    header("Content-Length: " . filesize($arquivo)); // informa o tamanho do arquivo ao navegador
    header("Content-Disposition: attachment; filename=" . basename($arquivo)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
    readfile($arquivo); // lê o arquivo
    exit; // aborta pós-ações 
}
?>
<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
        <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, contato, fale conosco, conosco">
        <meta name="author" content="Adriano Marques">
        <title>GestaTudo - Fale conosco</title>
        <link href="css/fale-conosco.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="img/logomini.png">
        <script src="jQuery/jquery-2.1.1.js"></script>
    </head>
    <body>
      <div class="centralizer">
         <header>
            <?php
            require_once('config/conexao.php');
            require_once('config/funcoes.php');
            ?>

            <div id="centro">
                <!--<a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>" id="logo">-->
                <a href="base.php?p=home" id="logo">
                    <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                </a>
                
                
                <div id="div_formulario">
                    <img src="img/mail.png">
                    <div id="title"><h2>Fale conosco</h2></div>
                    
                    <div style="clear: both"></div>
                    
                    <!---->
                    <div class="notificacao no" id="nome_null">Por favor, nos diga seu nome</div>
                    <div class="notificacao no" id="email_null">Por favor, informe seu e-mail</div>
                    <div class="notificacao no" id="assunto_null">Por favor, selecione um assunto</div>
                    <div class="notificacao no" id="mensagem_null">Por favor, digite sua mensagem</div>
                    <div class="notificacao no" id="email_invalido">Formato de e-mail inválido</div>
                    <div class="notificacao no" id="caracteres_especiais">Este campo não aceita números, espaços ou caracteres especiais</div>
                    <div class="notificacao ok" id="sucesso">Recebemos seu e-mail, obrigado por entrar em contato conosco!</div>
                    
                    <form method="post" name="formulario_contato">
                        <table>
<!--                            <caption>Fale conosco</caption>-->
                            <tbody>
                                <tr>
                                    <td><label for="nome">Nome:</label></td>
                                    <td><input type="text" id="nome" name="nome" maxlength="50" class="texto tamanho" autofocus placeholder="Digite seu nome" required oninvalid="setCustomValidity('Digite seu nome.')" onchange="try {setCustomValidity('');} catch (e) {}"></td>
                                </tr>
                                <tr>
                                    <td><label for="email">E-mail:</label></td>
                                    <td><input type="email" id="email" name="email" maxlength="255" class="texto tamanho" placeholder="Digite aqui seu e-mail" required oninvalid="setCustomValidity('Digite seu e-mail.')" onchange="try {setCustomValidity('');} catch (e) {}"></td>
                                </tr>
                                <tr>
                                    <td><label for="assunto">Assunto:</label></td>
                                    <td><select name="assunto" class="texto" id="assunto" required oninvalid="setCustomValidity('Selecione um assunto.')" onchange="try {setCustomValidity('');} catch (e) {}">
                                        <option selected disabled value="">Selecione:</option>
                                        <option value="Reportar um erro">Reportar um erro</option>
                                        <option value="Dúvidas">Dúvidas</option>
                                        <option value="Sugestões">Sugestões</option>
                                        <option value="Contato">Contato</option>
                                        <option value="Outro assunto">Outro</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td id='labelmsg'><label for='mensagem'>Mensagem:</label></td>
                                    <td><textarea name="mensagem" id="mensagem" rows="6" cols="32" maxlength="1200" placeholder="Digite aqui sua mensagem"></textarea></td>
                                </tr>
                                
                                <tr>
                                    <td id='linhaBtn' colspan="2"><input type="submit" value="Enviar" id="btnEnviar" name="btnEnviar"></td>
                                </tr>
                            </tbody>

                            
                        </table>
                    </form>
                </div>
                
                
                <?php
                    if(isset($_POST['btnEnviar'])){
                        $nomePessoa = $_POST['nome'];
                        $emailPessoa = $_POST['email'];
                        $assunto = $_POST['assunto'];
                        $mensagemPessoa = $_POST['mensagem'];
                        
                        
                        $erro = false;

                        //Tratando campo nome
                        if($nomePessoa == ""){
                            ?>
                            <script>
                                $('#nome_null').slideDown('slow');
                                $("#nome").css({"border": "solid 1px red"});

                                document.formulario_contato.nome.value = "<?php echo $nomePessoa ?>";
                                document.formulario_contato.email.value = "<?php echo $emailPessoa ?>";
                                document.formulario_contato.mensagem.value = "<?php echo $mensagemPessoa ?>";
                                $("#assunto option[value='<?php echo $assunto ?>']").attr("selected", true);

                            </script>
                            <?php
                            $erro = true;
                        }
                        else{
                            $caracteres_especiais = verifica_caracteres2($nomePessoa);
                            if($caracteres_especiais){
                                ?>
                                <script>
                                    $('#caracteres_especiais').slideDown('slow');
                                    $("#nome").css({"border": "solid 1px red"});

                                    document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                    document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                    document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                    document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                    document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                    document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                    document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                </script>
                                <?php
                                $erro = true;
                            }
                        }
                        
                        
                        if($erro == false){
                            if($emailPessoa == ""){
                                ?>
                                <script>
                                    $('#email_null').slideDown('slow');
                                    $("#email").css({"border": "solid 1px red"});

                                    document.formulario_contato.nome.value = "<?php echo $nomePessoa ?>";
                                    document.formulario_contato.email.value = "<?php echo $emailPessoa ?>";
                                    document.formulario_contato.mensagem.value = "<?php echo $mensagemPessoa ?>";
                                    $("#assunto option[value='<?php echo $assunto ?>']").attr("selected", true);

                                </script>
                                <?php
                                $erro = true;
                            }
                            elseif (!filter_var($emailPessoa, FILTER_VALIDATE_EMAIL)){
                            ?>
                                <script>
                                    $('#email_invalido').slideDown('slow');
                                    $("#email").css({"border": "solid 1px red"});

                                    document.formulario_contato.nome.value = "<?php echo $nomePessoa ?>";
                                    document.formulario_contato.email.value = "<?php echo $emailPessoa ?>";
                                    document.formulario_contato.mensagem.value = "<?php echo $mensagemPessoa ?>";
                                    $("#assunto option[value='<?php echo $assunto ?>']").attr("selected", true);
                                </script>
                                <?php
                                $erro = true;        
                            }
                        }
                        
                        if($erro == false){
                            if($assunto == ""){
                                ?>
                                <script>
                                    $('#assunto_null').slideDown('slow');
                                    $("#assunto").css({"border": "solid 1px red"});

                                    document.formulario_contato.nome.value = "<?php echo $nomePessoa ?>";
                                    document.formulario_contato.email.value = "<?php echo $emailPessoa ?>";
                                    document.formulario_contato.mensagem.value = "<?php echo $mensagemPessoa ?>";
                                    $("#assunto option[value='<?php echo $assunto ?>']").attr("selected", true);

                                </script>
                                <?php
                                $erro = true;
                            }
                        }

                        if($erro == false){
                             if($mensagemPessoa == ""){
                                ?>
                                <script>
                                    $('#mensagem_null').slideDown('slow');
                                    $("#mensagem").css({"border": "solid 1px red"});

                                    document.formulario_contato.nome.value = "<?php echo $nomePessoa ?>";
                                    document.formulario_contato.email.value = "<?php echo $emailPessoa ?>";
                                    document.formulario_contato.mensagem.value = "<?php echo $mensagemPessoa ?>";
                                    $("#assunto option[value='<?php echo $assunto ?>']").attr("selected", true);

                                </script>
                                <?php
                                $erro = true;
                            }
                        }
                        
                        
                        if($erro == false){
                            enviaEmailFaleConosco($nomePessoa, $emailPessoa, $assunto, $mensagemPessoa);
                            ?>
                                <script>
                                    $('#sucesso').slideDown('slow');
                                </script>
                            <?php
                        }
                        
                    }
                ?>
                
                
                <div id="download">
                    <b>Está com dúvidas?</b><br><br>
                    Consulte nosso <a id="manual" href="?arquivo=downloads/Manual-do-Usuario_GestaTudo.pdf">manual</a> do usuário completo.
                </div>             
                                
            </div>

         </header>
      </div>
      <footer><!--Rodapé-->
         <!--<span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=".date('m'). "&ano=".date('Y')."' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>-->
          <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
         <br>
         <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
      </footer>
   </body>
</html>