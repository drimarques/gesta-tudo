﻿<!DOCTYPE HTML>
<html lang="pt-br">
   <head>
      <title>GestaTudo - Esqueci a senha</title>
      <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
      <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, esqueceu a senha, esqueci">
      <meta name="author" content="Adriano Marques">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="css/estilo.esqueci-a-senha.css" rel="stylesheet" type="text/css">
      <link rel="shortcut icon" href="img/logomini.png">
      <script src="jQuery/jquery-2.1.1.js"></script>
   </head>
   <body>
      <div class="centralizer">
         <header>
            <?php
            require_once('config/conexao.php');
            require_once('config/funcoes.php');
            ?>

            <div>
                <!--<a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>" id="logo">-->
                <a href="base.php?p=home" id="logo">
                    <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                </a>
            </div>

            <form name="esqueci-a-senha" method="post">
               <label>E-mail:</label>
               <input type="email" name="txt_esqueceu_senha" maxlength="255" placeholder="Digite seu e-mail" id="txt_esqueceu_senha" autofocus required oninvalid="setCustomValidity('Por favor, preencha com seu e-mail.')" onchange="try {
                             setCustomValidity('')
                          } catch (e) {
                          }" class="texto transparente">
               <input type="submit" name="btn_recuperar_senha" value="Recuperar senha" id="btn_recuperar_senha" class="transparente">

               <div id="email_enviado" class="notificacao ok"><p><?php echo 'Em instantes você recebá um e-mail com sua nova senha.' ?></p></div>
               <div id="email_nao_cadastrado" class="notificacao no"><p><?php echo 'O e-mail <u>' . $_POST['txt_esqueceu_senha'] . '</u><br> ainda não está cadastrado em nosso sistema.' ?></p></div>
            </form>

            <?php
            if (isset($_POST['btn_recuperar_senha'])) {
               $email = $_POST['txt_esqueceu_senha'];
               $existe = verifica_email_cadastrado($email);

               if ($existe == FALSE) {
                  ?>
                  <script>
                     $(document).ready(function() {
                        $('#email_nao_cadastrado').slideDown('slow');
                     });
                  </script>
                  <?php
               }

               if ($existe == TRUE) {
                  gera_senha_aleatoria($email);
                  $enviou = email_recupera_senha($email);
                  if ($enviou) {
                     ?>
                     <script>
                        $(document).ready(function() {
                           $('#email_enviado').slideDown('slow');
                        });
                     </script>
                     <?php
                  }
               }
            }
            ?>
         </header>
      </div>
      <footer><!--Rodapé-->
         <!--<span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=".date('m'). "&ano=".date('Y')."' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>-->
          <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
         <br>
         <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
      </footer>
   </body>
</html>