<?php
    session_start();
    if (!isset($_SESSION['usuario_logado'])) {
        header("Location: index.php");
        exit;
    }
?>

﻿<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <title>GestaTudo - Acesse seu e-mail</title>
        <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
        <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, acesso, email">
        <meta name="author" content="Adriano Marques">
        <link rel="shortcut icon" href="img/logomini.png">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/estilo.acesse-seu-email.css" rel="stylesheet" type="text/css">
        <script src="jQuery/jquery-2.1.1.js"></script>
        
        <?php
            require_once('config/conexao.php');
            require_once('config/funcoes.php');
            $email = $_SESSION['usuario_logado'];
            $busca_usu_logado = mysql_query("SELECT * FROM usuarios WHERE email = '$email'");
            $usuario = mysql_fetch_array($busca_usu_logado);
            
            if($usuario[email] == ""){
                header('Location: logout.php');
            }
            
            
        ?>
        
    </head>
    <body>
        <div class="centralizer">
            <section>

                <div id="centro">
                    
                    <div id="logo">
                        <a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>">
                        <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                        </a>
                    </div>
                    
                   
                    <div id="mensagem">
                        <img src="img/mail.png">
                        <h3>E-mail não confirmado.</h3> 
                        <p>Um link de confirmação de e-mail foi enviado para você.</p>
                        <p>Ao acessar o link sua conta estará ativa!</p>
                    </div>
                    
                    <form method="post">
                        <input type="submit" id="ReenviarEmail" name="reenviarEmail" value="Reenviar e-mail">
                        <div id="emailReenviadoSucesso">E-mail reenviado para <span class="underline"><?php echo $usuario[email] ?></span> com sucesso!</div>
                        
                        <?php
                            if(isset($_POST['reenviarEmail'])){
                                email_alterouEmailPrincipal($usuario[id], $usuario[nome], $usuario[sobrenome], $usuario[email], $usuario[chave_email]);
                                ?>
                                <script>
                                    $("#emailReenviadoSucesso").slideToggle('normal');
                                </script>
                                <?php
                            }
                        ?>
                        
                    </form>
                </div>
            </section>
        </div>
        <footer><!--Rodapé-->
            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=".date('m'). "&ano=".date('Y')."' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
            <br>
            <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
        </footer>
    </body>
</html>