<?php

require_once ('../config/conexao.php');

date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');

$data_atual = date('Y-m-d');
$daqui_3_dias = date('Y-m-d', strtotime("+3 days", strtotime("$data_atual")));

$sql_usuarios = "SELECT * FROM usuarios WHERE permite_notificacoes = 1 and ativo = 1 ORDER BY id ASC";
$busca = mysql_query($sql_usuarios);



//Enquanto não chegar ao fim os usuários... a gente vai verificando
while($usuario = mysql_fetch_array($busca)){
    
    //echo $usuario[id]."<br>".$usuario[email]."<br>";
    $haContasAPagar3Dias = false;
    $haContasAReceber3Dias = false;
    
    $tabela_despesa = "<table
                            style='
                                font-family: Roboto, Tahoma, sans-serif;
                                font-size:12px;
                                min-width: 500px;
                                padding-left: 5px;
                                text-align: left;
                            '
                       > 
                    <thead>
                        <th>Categoria</th>
                        <th>Descri&ccedil;&atilde;o</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </thead>";


    $tabela_receita = "<table
                            style='
                                font-family: Roboto, Tahoma, sans-serif;
                                font-size:12px;
                                min-width: 500px;
                                padding-left: 5px;
                                text-align: left;
                            '
                       > 
                    <thead>
                        <th>Categoria</th>
                        <th>Descri&ccedil;&atilde;o</th>
                        <th>Data</th>
                        <th>Valor</th>
                    </thead>";
    
    //Buscando despesas
    $sql_despesas_3dias = "SELECT * FROM movimentacoes WHERE id_usuario = '$usuario[id]' AND mov_tipo = 'Despesa' AND pago_recebido = 0 AND data = '$daqui_3_dias' order by data asc";
    $busca_despesas = mysql_query($sql_despesas_3dias);
    $tem_despesas_3dias = mysql_num_rows($busca_despesas);
    
    //Se tem alguma despesa atrasada, cria a tabela
    if($tem_despesas_3dias != 0 ){
        $haContasAPagar3Dias = true;
        
        while ($retorno_despesas = mysql_fetch_array($busca_despesas)) {          
            $tabela_despesa.="<tbody>
                                    <tr>
                                        <td style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                        '>$retorno_despesas[categoria]</td>
                                        <td style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                        '>$retorno_despesas[descricao]</td>
                                        <td
                                            style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                            '>" 
                                        . date("d/m/Y", strtotime($retorno_despesas[data])) . "</td>
                                        <td
                                            style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                            color: red; 
                                            font-weight: bold;
                                        '>R$ -" . number_format($retorno_despesas[valor], 2, ',', '.') . "</td>
                                    </tr>
                                </tbody>";
        }
        
        $tabela_despesa .= "</table><br>";
        
    }
    
    
    
    
    
    
    
    
    //Buscando receitas
    $sql_receitas_3dias = "SELECT * FROM movimentacoes WHERE id_usuario = '$usuario[id]' AND mov_tipo = 'Receita' AND pago_recebido = 0 AND data = '$daqui_3_dias' order by data asc";
    $busca_receitas = mysql_query($sql_receitas_3dias);
    $tem_receitas_3dias = mysql_num_rows($busca_receitas);
    
    //Se tem alguma despesa atrasada, cria a tabela
    if($tem_receitas_3dias != 0 ){
        $haContasAReceber3Dias = true;
        
        while ($retorno_receitas = mysql_fetch_array($busca_receitas)) {          
            $tabela_receita.="<tbody>
                                    <tr>
                                        <td style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                        '>$retorno_receitas[categoria]</td>
                                        <td style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                        '>$retorno_receitas[descricao]</td>
                                        <td
                                            style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                            '>" 
                                        . date("d/m/Y", strtotime($retorno_receitas[data])) . "</td>
                                        <td
                                            style='
                                            padding: 5px 0;
                                            border-bottom-style: dashed;
                                            border-bottom-width: 1px;
                                            border-bottom-color: #cccccc;
                                            color: blue; 
                                            font-weight: bold;
                                        '>R$ " . number_format($retorno_receitas[valor], 2, ',', '.') . "</td>
                                    </tr>
                                </tbody>"; 
        }
        
        $tabela_receita .= "</table><br>";
    }
    
    
    
    //Terminando de mostar as 2 tabelas, enviamos o e-mail
    if( ($haContasAPagar3Dias) == true && ($haContasAReceber3Dias == true) ){
        enviaEmailDespesasEReceitas3Dias($usuario[nome], $usuario[sobrenome], $usuario[email], $tabela_despesa, $tabela_receita);
    }
    elseif( $haContasAPagar3Dias == true ){
        enviaEmailDespesas3Dias($usuario[nome], $usuario[sobrenome], $usuario[email], $tabela_despesa);
    }
    elseif( $haContasAReceber3Dias == true ){
        enviaEmailReceitas3Dias($usuario[nome], $usuario[sobrenome], $usuario[email], $tabela_receita);
    }
    
}

?>

<script>
    window.close();
</script>

<?php



//E-mail pra caso o cara tenha contas a pagar e receber atrasadas
function enviaEmailDespesasEReceitas3Dias($nome, $sobrenome, $destinatario, $tabela_despesa, $tabela_receita) {
    $assunto = "3 dias para o vencimento.";
    $assunto = '=?UTF-8?B?'.base64_encode($assunto).'?=';
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8; charset=iso-8859-1\r\n";
    $headers .= "From: \"GestaTudo\"\r\n";
    //$headers .= "Cc: adrianomfpassos@gmail.com\r\n";    
    
    $mensagem = "   <div style='
                            margin: 0 auto;
                            padding: 10px 40px 0 40px;
                            width: 520px;
                            border: solid 1px #cccccc;
                            border-bottom: none;
                            background-color: #FFF;
                            text-align: center;
                            border-radius: 30px 30px 0px 0px;
                            '>

                        <a href='http://localhost/GestaTudo/'><img src=\"http://gestatudo.besaba.com/img/logomini.png\" style='
                                                                                            width: 100px;
                                                                                            height: 100px;
                                                                                            display: block;
                                                                                            margin: 0 auto;
                                                                                            '></a>
                    </div>
                    
                    
                    <div
                        style='
                            margin: 0 auto;
                            padding: 20px 40px;
                            width: 520px;
                            border: solid 1px #cccccc;
                            border-top: none;
                            background-color: #FFF;
                            text-align: normal;
                            font-family: Roboto, Tahoma, sans-serif;
                            font-size: 14px;
                        '>
                Ol&aacute; $nome $sobrenome,<br><br>
                O motivo deste e-mail &eacute; para notific&aacute;-lo que voc&ecirc; tem movimenta&ccedil;&otilde;es que vencem daqui 3 dias. <br><br>

                <span style='background-color: rgba(255,99,71, 0.1); padding: 4px 9px;'>CONTAS A PAGAR</span><br><br>   
                    $tabela_despesa
                        
                <span style='background-color: rgba(173,216,230, 0.2); padding: 4px 9px;'>CONTAS A RECEBER</span><br><br>   
                    $tabela_receita
                    
                Caso tenha alguma d&uacute;vida a nossa equipe est&aacute; sempre &agrave; disposi&ccedil;&atilde;o, basta enviar um e-mail para <span style='color: blue; text-decoration: underline;'>contatogestatudo@gmail.com</span>. <br><br>
		
                Atenciosamente,<br><br>
		Equipe GestaTudo.
                </div>
                    <div style='
                            margin: 0 auto;
                            padding: 10px 0 10px 35px;
                            border: solid 1px #cccccc;
                            border-top: none;
                            width: 565px;
                            background-color: #FFF;
                            text-align: justify;
                            border-radius: 0px 0px 30px 30px;
                            '>
                    <a href='http://gestatudo.besaba.com/'><img src=\"http://gestatudo.besaba.com/img/logonome.png\" style='
                                                                                        width: 200px;
                                                                                        display: block;
                                                                                        '></a>
                    </div>
                </div>";
    
    mail($destinatario, $assunto, $mensagem, $headers);

}


//E-mail pra caso o cara tenha somente contas a receber atrasadas
function enviaEmailReceitas3Dias($nome, $sobrenome, $destinatario, $tabela_receita){
    $assunto = "Contas a receber daqui 3 dias.";
    $assunto = '=?UTF-8?B?'.base64_encode($assunto).'?=';
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8; charset=iso-8859-1\r\n";
    $headers .= "From: \"GestaTudo\"\r\n";
    //$headers .= "Cc: adrianomfpassos@gmail.com\r\n";
    
    $mensagem = "   <div style='
                            margin: 0 auto;
                            padding: 10px 40px 0 40px;
                            width: 520px;
                            border: solid 1px #cccccc;
                            border-bottom: none;
                            background-color: #FFF;
                            text-align: center;
                            border-radius: 30px 30px 0px 0px;
                            '>

                        <a href='http://localhost/GestaTudo/'><img src=\"http://gestatudo.besaba.com/img/logomini.png\" style='
                                                                                            width: 100px;
                                                                                            height: 100px;
                                                                                            display: block;
                                                                                            margin: 0 auto;
                                                                                            '></a>
                    </div>
                    
                    
                    <div
                        style='
                            margin: 0 auto;
                            padding: 20px 40px;
                            width: 520px;
                            border: solid 1px #cccccc;
                            border-top: none;
                            background-color: #FFF;
                            text-align: normal;
                            font-family: Roboto, Tahoma, sans-serif;
                            font-size: 14px;
                        '>
                Ol&aacute; $nome $sobrenome,<br><br>
                O motivo deste e-mail &eacute; para notific&aacute;-lo que voc&ecirc; tem receitas para receber daqui 3 dias. <br><br>

                <span style='background-color: rgba(173,216,230, 0.2); padding: 4px 9px;'>CONTAS A RECEBER</span><br><br>   
                    $tabela_receita
                    
                Caso tenha alguma d&uacute;vida a nossa equipe est&aacute; sempre &agrave; disposi&ccedil;&atilde;o, basta enviar um e-mail para <span style='color: blue; text-decoration: underline;'>contatogestatudo@gmail.com</span>. <br><br>
		
                Atenciosamente,<br><br>
		Equipe GestaTudo.
                </div>
                    <div style='
                            margin: 0 auto;
                            padding: 10px 0 10px 35px;
                            border: solid 1px #cccccc;
                            border-top: none;
                            width: 565px;
                            background-color: #FFF;
                            text-align: justify;
                            border-radius: 0px 0px 30px 30px;
                            '>
                    <a href='http://gestatudo.besaba.com/'><img src=\"http://gestatudo.besaba.com/img/logonome.png\" style='
                                                                                        width: 200px;
                                                                                        display: block;
                                                                                        '></a>
                    </div>
                </div>";
    
    mail($destinatario, $assunto, $mensagem, $headers);
}


//E-mail pra caso o cara tenha somente contas a pagar atrasadas
function enviaEmailDespesas3Dias($nome, $sobrenome, $destinatario, $tabela_despesa){
    $assunto = "Contas a pagar em até 3 dias.";
    $assunto = '=?UTF-8?B?'.base64_encode($assunto).'?=';
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8; charset=iso-8859-1\r\n";
    $headers .= "From: \"GestaTudo\"\r\n";
    //$headers .= "Cc: adrianomfpassos@gmail.com\r\n";    
    
    $mensagem = "   <div style='
                            margin: 0 auto;
                            padding: 10px 40px 0 40px;
                            width: 520px;
                            border: solid 1px #cccccc;
                            border-bottom: none;
                            background-color: #FFF;
                            text-align: center;
                            border-radius: 30px 30px 0px 0px;
                            '>

                        <a href='http://localhost/GestaTudo/'><img src=\"http://gestatudo.besaba.com/img/logomini.png\" style='
                                                                                            width: 100px;
                                                                                            height: 100px;
                                                                                            display: block;
                                                                                            margin: 0 auto;
                                                                                            '></a>
                    </div>
                    
                    
                    <div
                        style='
                            margin: 0 auto;
                            padding: 20px 40px;
                            width: 520px;
                            border: solid 1px #cccccc;
                            border-top: none;
                            background-color: #FFF;
                            text-align: normal;
                            font-family: Roboto, Tahoma, sans-serif;
                            font-size: 14px;
                        '>
                Ol&aacute; $nome $sobrenome,<br><br>
                O motivo deste e-mail &eacute; para notific&aacute;-lo que voc&ecirc; tem despesas que vencem daqui <br><b>3 dias</b>. <br><br>
                        
                <span style='background-color: rgba(255,99,71, 0.1); padding: 4px 9px;'>CONTAS A PAGAR</span><br><br>   
                    $tabela_despesa
                    
                Caso tenha alguma d&uacute;vida a nossa equipe est&aacute; sempre &agrave; disposi&ccedil;&atilde;o, basta enviar um e-mail para <span style='color: blue; text-decoration: underline;'>contatogestatudo@gmail.com</span>. <br><br>
		
                Atenciosamente,<br><br>
		Equipe GestaTudo.
                </div>
                    <div style='
                            margin: 0 auto;
                            padding: 10px 0 10px 35px;
                            border: solid 1px #cccccc;
                            border-top: none;
                            width: 565px;
                            background-color: #FFF;
                            text-align: justify;
                            border-radius: 0px 0px 30px 30px;
                            '>
                    <a href='http://gestatudo.besaba.com/'><img src=\"http://gestatudo.besaba.com/img/logonome.png\" style='
                                                                                        width: 200px;
                                                                                        display: block;
                                                                                        '></a>
                    </div>
                </div>";
    
    mail($destinatario, $assunto, $mensagem, $headers);
}




