<!DOCTYPE HTML>
<?php
date_default_timezone_set ( 'Brazil/East' );
date_default_timezone_set ( 'America/Sao_Paulo' );

session_start ();
if (! isset ( $_SESSION ['usuario_logado'] )) {
	header ( "Location: index.php" );
	exit ();
}

$p = $_GET ['p'];

if (empty ( $p )) {
	$pagina = 'home';
} else {
	$pagina = $p;
}

$nome_pagina = null;
if ($pagina == "home") {
	$imghome = "img/home2.png";
	$nome_pagina = "Home";
	$a1 = "fundo";
} elseif ($pagina == "movimentacoes") {
	$imghome = "img/home2.png";
	$nome_pagina = "Movimentações";
	$a2 = "fundo";
} elseif (($pagina == "relatorio-7-dias") || ($pagina == "relatorio-30-dias") || ($pagina == "relatorio-3-meses") || ($pagina == "relatorio-por-periodo") || ($pagina == "relatorio-geral")) {
	$imghome = "img/home2.png";
	$nome_pagina = "Relatórios";
	$a3 = "fundo";
}// elseif ($pagina == "banco") {
// $imghome = "img/home2.png";
// $nome_pagina = "Acesse seu banco";
// $a4 = "fundo";
// }
elseif ($pagina == "metas") {
	$imghome = "img/home2.png";
	$nome_pagina = "Metas";
	$a4 = "fundo";
} elseif ($pagina == "configuracoes") {
	$imghome = "img/home2.png";
	$nome_pagina = "Configurações";
	$a5 = "fundo";
} elseif ($pagina == "perfil") {
	$imghome = "img/home2.png";
	$nome_pagina = "Configurações de perfil";
	$a5 = "fundo";
} elseif ($pagina == "senha") {
	$imghome = "img/home2.png";
	$nome_pagina = "Alterar senha";
	$a5 = "fundo";
} elseif ($pagina == "categorias") {
	$imghome = "img/home2.png";
	$nome_pagina = "Minhas categorias";
	$a5 = "fundo";
} else {
	$imghome = "img/home2.png";
	$nome_pagina = "Página não encontrada";
}
?>


<html lang="pt-br">
<head>
<title>GestaTudo - <?php echo $nome_pagina ?></title>
<!--ucwords é pra colocar a primeira letra do nome da pagina em maiuculo-->
<meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
<meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, home, menu">
<meta name="author" content="Adriano Marques">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<script src="jQuery/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>

<link href="css/estilo.base.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="img/logomini.png">

<!--        <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900italic,900,300italic,300,100italic,100' rel='stylesheet' type='text/css'>-->

<script src="jQuery/jquery-2.1.1.js"></script>

<!--Caixas de dialogo "popup"-->
<!--        <link rel="stylesheet" href="jQuery/jquery-ui-dialog/jquery-ui.css">
        <script src="jQuery/jquery-1.10.2.js"></script>
        <script src="jQuery/jquery-ui-dialog/jquery-ui.js"></script>
        
        Date picker, só colocar classe "data" no campo  type="text"
        <link href="jQuery/jquery-ui-date-picker/jquery-ui.css" rel="stylesheet" type="text/css">
        <script src="jQuery/jquery-ui-date-picker/jquery-ui.js"></script> -->

<!--Date picker e dialog-->
<link href="jQuery/jquery-ui-custom/jquery-ui.css" rel="stylesheet"
	type="text/css">
<script src="jQuery/jquery-ui-custom/jquery-ui.js"></script>

<script src="jQuery/jquery.maskedinput.min.js"></script>
        

        <?php
								if ($pagina == "home") {
									$a1 = "fundo";
								} elseif ($pagina == "movimentacoes") {
									$a2 = "fundo";
								} elseif ($pagina == "relatorios") {
									$a3 = "fundo";
								} elseif ($pagina == "metas") {
									$a4 = "fundo";
								} elseif (($pagina == "perfil") || ($pagina == "senha")) {
									$a5 = "fundo";
								}
								?>

        <script>
            $(document).ready(function() {
                
                
                $(".data").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'S�bado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    //yearRange: '2000:2100',
                    nextText: 'Pr�ximo',
                    prevText: 'Anterior'
                });
                
                
                $("#data_inicio").mask("99/99/9999");
                $("#data_fim").mask("99/99/9999");
                
                
                
                
                
                
                $("#foto_usuario").mouseover(function(){
                    $("#foto_usuario").fadeTo("normal",0.60); //fadeTo é para ficar transparente até um determinado nivel de transparencia (velocidade, nivel de transparencia)	
		});
                $("#foto_usuario").mouseout(function(){
                    $("#foto_usuario").fadeTo("slow", 1); //fadeTo é para ficar transparente até um determinado nivel de transparencia (velocidade, nivel de transparencia)	
		});
                

//                $("#cam").mouseover(function() {
//                    $("#notifica_foto").fadeIn("normal");
//                });
//
//                $("#cam").mouseout(function() {
//                    $("#notifica_foto").fadeOut("fast");
//                });

                $("#movimentacoes").mouseover(function() {
                    $("#movimentacoes_drop").css({"color": "#000"});
                });

                $("#movimentacoes").mouseout(function() {
                    $("#movimentacoes_drop").css({"color": "#FFF"});
                });

                $("#nome-usuario").mouseover(function() {
                    $("#nome_drop").css({"color": "#FFF"});
                });



                $("#dropdown-menu-movimentacoes").mouseover(function() {
                    $("#movimentacoes").css({
                        "text-decoration": "none",
                        "color": "#000",
                        "background-color": "#F8F8FF",
                        "border": "#cccccc 1px solid"
                    });

                    $("#movimentacoes_drop").css({"color": "#000"});
                });
                
                
                

                $("#dropdown-menu-nome").mouseover(function() {
                    $("#nome_usu").css({
                        "text-decoration": "none",
                        "background-color": "#006600",
                        "cursor": "pointer"
                    });
                    
                    $(".caret").css({
                        "color": "#FFF"
                    });
                });
                
                
                
                
                $("#por_periodo").click(function() {
                    $("#popup_filtra_periodo").dialog({
                        width: 360,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            //ver como da o refresh
                        }
                    });
                });
                
            });
        </script>
</head>
<body>
	<header>
		<!--Cabeçalho-->
		<div class="centralizer">
                <?php
																require_once ('config/conexao.php');
																require_once ('config/funcoes.php');
																require_once ('config/funcoes.movimentacoes.php');
																require_once ('config/funcoes.home.php');
																require_once ('config/funcoes.relatorios.php');
																require_once ('config/funcoes.categorias.php');
																require_once ('config/funcoes.metas.php');
																
																$email = $_SESSION ['usuario_logado'];
																$busca_usu_logado = mysql_query ( "SELECT * FROM usuarios WHERE email = '$email'" );
																$usuario = mysql_fetch_array ( $busca_usu_logado );
																$id_usuario = $usuario ['id'];
																$nome = $usuario ['nome'];
																$sobrenome = $usuario ['sobrenome'];
																
																// Verificando senha padrão, se é padrão volta pra tela de redefinir senha
																$senha_aleatoria = verifica_senha_aleatoria ( $id_usuario );
																if ($senha_aleatoria == TRUE) {
																	$_SESSION ['usuario_logado'] = $email;
																	header ( 'Location: redefine-senha.php' );
																}
																
																if ($usuario ['ativo'] == 0) {
																	$_SESSION ['usuario_logado'] = $email;
																	header ( 'Location: acesse-seu-email.php' );
																}
																
																// script responsável por carregar a foto do uauário
																if ($usuario ['foto'] == NULL) {
																	$foto_usuario = 'user.png';
																} else {
																	$foto_usuario = $usuario ['foto'];
																}
																?>
                
                
                
                
                
                    <?php
																				$saldo_atual = retorna_saldo_atual ( $id_usuario );
																				if ($saldo_atual >= 0) {
																					$classe_variavel = "dinheiro_azul";
																				} else {
																					$classe_variavel = "dinheiro_vermelho";
																				}
																				?>
                        <span id='saldo_atual'>
                                <?php
																																echo "<span>Saldo atual: </span><br>";
																																echo "<span class='$classe_variavel'>R$ " . number_format ( $saldo_atual, 2, ',', '.' ) . "</span>";
																																?> 
                        </span>





			<!--                <a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>" id="logo">-->
			<a href="base.php?p=home" id="logo"> <img src="img/logogestatudo.png"
				alt="Logo GestaTudo">
			</a> <a href="base.php?p=perfil">
				<div id="foto_usuario">
					<img src="img_perfil_usuarios/<?php echo $foto_usuario ?>"
						title="Clique aqui para alterar suas configurações de perfil.">
				</div> <!--                <a href="base.php?p=perfil"><img src="img/cam2.png" id="cam">
                    <div id="notifica_foto"><p>Atualizar foto do perfil</p></div>-->
			</a>
		</div>
	</header>

	<nav>
		<!--Menu-->
		<div class="centralizer">
			<ul id="tabs">
				<div id="home" class="<?php echo $a1 ?>">
					<!--                        <li id="home_click"><a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>"><img src="<?php echo $imghome ?>"></a><li>-->
					<li id="home_click"><a href="base.php?p=home"><img
							src="<?php echo $imghome ?>"></a>
					
					<li>
				
				</div>

				<div class="dropdown <?php echo $a2 ?>">
					<!--                        <li id="movimentacoes"><a href="<?php //echo "?p=movimentacoes" ?>">Movimentações</a></li>-->
					<li id="movimentacoes"><a
						href="<?php
						
$ultimo_dia = retorna_dia_final_mes ( date ( 'm' ) );
						echo "?p=movimentacoes&dt_de=" . date ( 'Y-m-01' ) . "&dt_ate=" . date ( 'Y-m-' ) . $ultimo_dia?>">Movimentações</a></li>
					<!--<a data-toggle="dropdown" id="movimentacoes">Movimentações <span id="movimentacoes_drop"></span></a>
                        <ul class="dropdown-menu" id="dropdown-menu-movimentacoes" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?php //echo '?p=receita' ?>"><img src="img/new.png">Nova receita</a></li>
                            <li><a href="<?php //echo '?p=despesa' ?>"><img src="img/new2.png">Nova despesa</a></li>
                        </ul>-->
				</div>



				<!--                    <div class="dropdown <?php echo $a4 ?>">
                        <li><a href="<?php echo '?p=banco' ?>">Acesse seu banco</a></li>
                    </div>-->

				<div class="dropdown <?php echo $a4 ?>">
					<li><a href="<?php echo '?p=metas' ?>">Metas</a></li>
				</div>

				<div class="dropdown <?php echo $a3 ?>">
					<a data-toggle="dropdown">Relatórios <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<li><a href="<?php echo '?p=relatorio-7-dias' ?>">Últimos 7 dias</a></li>
						<li><a href="<?php echo '?p=relatorio-30-dias' ?>">Últimos 30
								dias</a></li>
						<li><a href="<?php echo '?p=relatorio-3-meses' ?>">Últimos 3
								meses</a></li>
						<li><a href="<?php echo '?p=relatorio-geral' ?>">Extrato geral</a></li>
						<li id="por_periodo"><a>Por período</a></li>
					</ul>
				</div>


				<div class="dropdown <?php echo $a5 ?>" id="nome-usuario">
					<a data-toggle="dropdown" id="nome_usu"><?php echo "$nome $sobrenome" ?> <span
						id="nome_drop" class="caret"></span></a>
					<ul class="dropdown-menu" id="dropdown-menu-nome" role="menu"
						aria-labelledby="dLabel">
						<li><a href="<?php echo '?p=configuracoes' ?>"><img
								src="img/config2.png">Configurações</a></li>
						<li><a href="logout.php"><img src="img/logout.png">Sair</a></li>
					</ul>
				</div>

			</ul>
		</div>
	</nav>



	<div id="popup_filtra_periodo" title="Filtrar extrato">
		<form method="post" name="form_relatorio_por_periodo" action="">
			<input type="text" name="data_inicio" id="data_inicio"
				class="texto data" placeholder="Data inicial"> - <input type="text"
				name="data_fim" id="data_fim" class="texto data"
				placeholder="Data final"> <span class="notificacao_por_periodo no"
				id="datas_por_periodonull">Preencha todos os campos.</span> <span
				class="notificacao_por_periodo no" id="datapor_periodo_invalida">Data
				inválida.</span> <span class="notificacao_por_periodo no"
				id="periodo_busca_por_periodo_invalido">A data inicial deve ser
				menor do que a data final.</span> <input type="submit"
				name="btn_buscar_relat_por_periodo"
				id="btn_buscar_relat_por_periodo" class="texto" value="Buscar">
		</form>
            
            <?php
												if (isset ( $_POST ['btn_buscar_relat_por_periodo'] )) {
													$data_inicio = $_POST ['data_inicio'];
													$data_fim = $_POST ['data_fim'];
													
													$erro = false;
													
													// echo $data_de . '<br>';
													// echo $data_ate . '<br>';
													// Se as datas tiverem brancas
													if (($data_inicio == "") || ($data_fim == "")) {
														$erro = true;
														?>
                    <script>
                        $("#popup_filtra_periodo").dialog({
                            width: 360,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false
                        });
                        
                        
                        $('#datas_por_periodonull').slideDown('fast');
                        $("#data_inicio").css({"border": "solid 1px red"});
                        $("#data_fim").css({"border": "solid 1px red"});
                        document.form_relatorio_por_periodo.data_inicio.value = "<?php echo $data_inicio ?>";
                        document.form_relatorio_por_periodo.data_fim.value = "<?php echo $data_fim ?>";
                    </script>
                <?php
													} 													

													// Senão tiverem brancas, verifica se são válidas
													else {
														
														// Data DE:
														$data_inicio = explode ( "/", "$data_inicio" );
														$dia_de = $data_inicio [0];
														$mes_de = $data_inicio [1];
														$ano_de = $data_inicio [2];
														
														if (! (checkdate ( $mes_de, $dia_de, $ano_de ))) {
															$erro = true;
															?>
                                    <script>
                                        $("#popup_filtra_periodo").dialog({
                                            width: 360,
                                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                            resizable: false
                                        });
                                        $('#datapor_periodo_invalida').slideDown('fast');
                                        $("#data_inicio").css({"border": "solid 1px red"});
                        <?php
															$data_inicio = "$dia_de/";
															$data_inicio .= "$mes_de/";
															$data_inicio .= $ano_de;
															?>
                                        document.form_relatorio_por_periodo.data_inicio.value = "<?php echo $data_inicio ?>";
                                        document.form_relatorio_por_periodo.data_fim.value = "<?php echo $data_fim ?>";
                                    </script>
                        <?php
														}
														
														// Data ATE:
														$data_fim = explode ( "/", "$data_fim" );
														$dia_ate = $data_fim [0];
														$mes_ate = $data_fim [1];
														$ano_ate = $data_fim [2];
														
														if (! (checkdate ( $mes_ate, $dia_ate, $ano_ate ))) {
															$erro = true;
															?>
                                    <script>
                                        $("#popup_filtra_periodo").dialog({
                                            width: 360,
                                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                            resizable: false
                                        });
                                        $('#datapor_periodo_invalida').slideDown('fast');
                                        $("#data_fim").css({"border": "solid 1px red"});
                        <?php
															$data_de = "$dia_de/";
															$data_de .= "$mes_de/";
															$data_de .= $ano_de;
															
															$data_ate = "$dia_ate/";
															$data_ate .= "$mes_ate/";
															$data_ate .= $ano_ate;
															?>
                                        document.form_relatorio_por_periodo.data_inicio.value = "<?php echo $data_inicio ?>";
                                        document.form_relatorio_por_periodo.data_fim.value = "<?php echo $data_fim ?>";
                                    </script>
                        <?php
														}
														
														if ($erro == false) {
															
															$data_inicio = $ano_de;
															$data_inicio .= "-$mes_de";
															$data_inicio .= "-$dia_de";
															
															$data_fim = $ano_ate;
															$data_fim .= "-$mes_ate";
															$data_fim .= "-$dia_ate";
															
															if ($data_inicio > $data_fim) {
																?>
                            <script>
                                $("#popup_filtra_periodo").dialog({
                                    width: 360,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false
                                });
                                $('#periodo_busca_por_periodo_invalido').slideDown('fast');
                                $("#data_inicio").css({"border": "solid 1px red"});
                                $("#data_fim").css({"border": "solid 1px red"});
                            <?php
																$data_inicio = "$dia_de/";
																$data_inicio .= "$mes_de/";
																$data_inicio .= $ano_de;
																
																$data_fim = "$dia_ate/";
																$data_fim .= "$mes_ate/";
																$data_fim .= $ano_ate;
																?>
                                document.form_relatorio_por_periodo.data_inicio.value = "<?php echo $data_inicio ?>";
                                document.form_relatorio_por_periodo.data_fim.value = "<?php echo $data_fim ?>";
                            </script>
                            <?php
															} 															// Se tiver tudo certo com as datas joga pro relatório
															else {
																header ( 'Location: base.php?p=relatorio-por-periodo&dt_inicio=' . $data_inicio . '&dt_fim=' . $data_fim . '' );
															}
														}
													}
												}
												
												?>
            
        </div>



	<section class="centro">
		<div class="centralizer">
                <?php
																// se o arquivo existir, a gente inclui ele aqui
																if (file_exists ( 'paginas/' . $pagina . '.php' )) {
																	include ("paginas/$pagina.php");
																} else {
																	include ("pagina-nao-encontrada.php");
																}
																?>
            </div>
	</section>
	</div>
	<!--Fazendo uma gambiarra no CSS-->
	<div class="clear"></div>

	<footer>
		<!--Rodapé-->
        <?php //echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=".date('m'). "&ano=".date('Y')."' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?>
        <?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?>
        <br> <a href="quem-somos.php">Quem somos</a> - <a
			href="fale-conosco.php">Fale conosco</a>
	</footer>

</body>
</html>