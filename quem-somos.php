<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <title>GestaTudo - Quem somos</title>
        <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
        <meta name="keywords" content="quem somos, quem, somos, gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web">
        <meta name="author" content="Adriano Marques">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/quem-somos.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="img/logomini.png">
        <script src="jQuery/jquery-2.1.1.js"></script>
    </head>
    <body>


        <div class="centralizer">
            <header><!--cabeçalho-->
                <a href="base.php?p=home" id="logo">                        
                    <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                </a>
            </header>

            <section>
                <h2>Quem somos</h2>
                <br>
                <p>O Gesta Tudo foi criado para fins de um projeto acadêmico por um grupo de universitários, baseado na ideia de que uma pessoa consiga controlar-se e organizar-se financeiramente, através de uma ferramenta que seja de fácil utilização, objetiva e atrativa.</p>
                <p>A partir dessa concepção procuramos nos aprofundar para a obtenção de um amplo conhecimento sobre o controle financeiro pessoal, indispensável para o desenvolvimento do projeto.</p>
                <p>Com o consumismo que vivenciamos na sociedade atual, um grande número de pessoas acumulam despesas todos os dias. De acordo com o levantamento feito em abril de 2014 pela Confederação Nacional do Comércio de Bens, Serviços e Turismo (CNC), 62,3% das famílias brasileiras estão endividadas.</p>
                <p>O Gesta Tudo foi desenvolvido pensando nas dificuldades financeiras que tantas pessoas vivem, auxiliando-as a ter um controle a curto ou a longo prazo de suas receitas e despesas, sabendo conciliá-las de forma que haja ao menos um equilíbrio entre elas.</p>
                
                    <div class="imagens" id="img1">
                        <a href="http://www.facebook.com/wilson.oliveira.37051" target="_blank"><img src="img/admins/1393067_639823079373360_387798345_n.jpg"></a>
                        
                        <span id="nome1">
                            Wilson Oliveira
                        </span>
                    </div>
                    
                    <div class="imagens" id="img2">
                        <a href="" target=""><img src="img/admins/IMG_3775.JPG"></a>
                        
                        <span id="nome2">
                            Jackis Aguiar
                        </span>
                    </div>
                    
                    <div class="imagens" id="img3">
                        <a href="http://www.facebook.com/adrianomarques9" target="_blank"><img src="img/admins/3x4.jpg"></a>
                        
                        <span id="nome3">
                            Adriano Marques
                        </span>
                    </div>
                    
                    <div class="imagens" id="img4">
                        <a href="http://www.facebook.com/nicolas.degouy" target="_blank"><img src="img/admins/296810_104415056336460_1558131559_n.jpg"></a>
                        
                        <span id="nome4">
                            Nicolas Maurice
                        </span>
                    </div>
                    
                    <div class="imagens" id="img5">
                        <a href="http://www.facebook.com/romerito.santos.14" target="_blank"><img src="img/admins/avatar.jpg"></a>
                        
                        <span id="nome5">
                            Romerito Prado
                        </span>
                    </div>
                    
                
                <div id="nomes">
<!--                    <span id="nome1">
                        Wilson Oliveira
                    </span>
                    
                    <span id="nome2">
                        Jackis Aguiar
                    </span>
                    
                    <span id="nome3">
                        Adriano Marques
                    </span>
                    
                    <span id="nome4">
                        Nicolas Maurice
                    </span>
                    
                    <span id="nome5">
                        Romerito Prado
                    </span>-->
                    
                    
                    
                </div>
            </section>

        </div>


        <footer><!--Rodapé-->
<!--            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=" . date('m') . "&ano=" . date('Y') . "' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>-->
            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
            <br>
            <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
        </footer>

        <script src="js/quem-somos.js"></script>
    </body>
</html>