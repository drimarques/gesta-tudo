$(function() {


    /*Filtrando por despesas*/
    $("#label_despesas").click(function() {
        $("#filtros_despesas").show("normal");
        $("#filtros_receitas").hide("fast");

//        $('.linha_receita').hide('fast');
//        $('.linha_despesa').show('normal');
        
        /*Deixando negrito o elemento selecionado*/
        $("#label_despesas").css({"font-weight":"bold"});
        $("#label_receitas").css({"font-weight":"normal"});
        $("#label_todas").css({"font-weight":"normal"});
    });



    /*Filtrando por receitas*/
    $("#label_receitas").click(function() {
        $("#filtros_receitas").show("normal");
        $("#filtros_despesas").hide("fast");

        //$('.linha_despesa').hide('fast');
        //$('.linha_receita').show('normal');
        
        
        /*Deixando negrito o elemento selecionado*/
        $("#label_despesas").css({"font-weight":"normal"});
        $("#label_receitas").css({"font-weight":"bold"});
        $("#label_todas").css({"font-weight":"normal"});
    });





    /*Filtrando por despesas não pagas*/
    $("#label_nao_pagas").click(function() {
        $(".despesa_nao_paga").show("normal");
        $(".despesa_paga").hide("fast");
        $('.linha_receita').hide('fast');
        
        $("#label_nao_pagas").css({"font-weight":"bold"});
        $("#label_pagas").css({"font-weight":"normal"});
        $("#label_todas_despesas").css({"font-weight":"normal"});
    });

    /*Filtrando por despesas pagas*/
    $("#label_pagas").click(function() {
        $(".despesa_nao_paga").hide("fast");
        $(".despesa_paga").show("normal");
        $('.linha_receita').hide('fast');
        
        $("#label_nao_pagas").css({"font-weight":"normal"});
        $("#label_pagas").css({"font-weight":"bold"});
        $("#label_todas_despesas").css({"font-weight":"normal"});
    });





    /*Mostrando todas despesas*/
    $("#label_todas_despesas").click(function() {
        $(".despesa_nao_paga").show("normal");
        $(".despesa_paga").show("normal");
        $('.linha_receita').hide('fast');
        
        
        $("#label_nao_pagas").css({"font-weight":"normal"});
        $("#label_pagas").css({"font-weight":"normal"});
        $("#label_todas_despesas").css({"font-weight":"bold"});
    });




    /*Filtrando por receitas não recebidas*/
    $("#label_nao_recebidas").click(function() {
        $(".receita_nao_recebida").show("normal");
        $(".receita_recebida").hide("fast");
        $('.linha_despesa').hide('fast');
        
        $("#label_nao_recebidas").css({"font-weight":"bold"});
        $("#label_recebidas").css({"font-weight":"normal"});
        $("#label_todas_receitas").css({"font-weight":"normal"});
        
    });


    /*Filtrando por receitas recebidas*/
    $("#label_recebidas").click(function() {
        $(".receita_nao_recebida").hide("fast");
        $(".receita_recebida").show("normal");
        $('.linha_despesa').hide('fast');
        
        $("#label_nao_recebidas").css({"font-weight":"normal"});
        $("#label_recebidas").css({"font-weight":"bold"});
        $("#label_todas_receitas").css({"font-weight":"normal"});
    });

    /*Mostrando todas receitas*/
    $("#label_todas_receitas").click(function() {
        $(".receita_nao_recebida").show("normal");
        $(".receita_recebida").show("normal");
        $('.linha_despesa').hide('fast');
        
        $("#label_nao_recebidas").css({"font-weight":"normal"});
        $("#label_recebidas").css({"font-weight":"normal"});
        $("#label_todas_receitas").css({"font-weight":"bold"});
    });




    /*Mostrando tudo...*/
    $("#label_todas").click(function() {
        $('.linha_receita').show('fast');
        $('.linha_despesa').show('fast');
        
        $('#filtros_receitas').hide('fast');
        $('#filtros_despesas').hide('fast');
        
        /*Deixando negrito o elemento selecionado*/
        $("#label_despesas").css({"font-weight":"normal"});
        $("#label_receitas").css({"font-weight":"normal"});
        
        $("#label_todas").css({"font-weight":"bold"});
    });



});