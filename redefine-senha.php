﻿<!DOCTYPE HTML>
<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}
?>
<html lang="pt-br">
    <head>
        <title>GestaTudo - Redefinir senha</title>
        <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
        <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, senha">
        <meta name="author" content="Adriano Marques">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/redefine-senha.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="img/logomini.png">
        <script src="jQuery/jquery-2.1.1.js"></script>
        
        <script>
            $(function($) {    
        //Tratando focus nos campos
                $("#nova_senha").focus(function(){
                    $('#notifica_senha').slideDown('normal');
                });
                
                $("#nova_senha").blur(function(){
                    $('#notifica_senha').slideUp('normal');
                });
                
            });
        </script>
        
    </head>
    <body>
        <div class="centralizer">
            <header>
                <?php
                require_once('config/conexao.php');
                require_once('config/funcoes.php');
                $email = $_SESSION['usuario_logado'];
                $busca_usu_logado = mysql_query("SELECT * FROM usuarios WHERE email = '$email'");
                $usuario = mysql_fetch_array($busca_usu_logado);
                if($usuario['ativo'] == 0){
                    header('Location: acesse-seu-email.php');
                }
                ?>

                <div id="centro">
                    <div id="titulo"><img src="img/password2.png"><h2>Por favor, altere sua senha.</h2></div>
                    
                    <div class="clear"></div>
<!--
                    <a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>"><div id="logo">
                        Logo
                    </div></a>-->
                    <a href="base.php?p=home&mes=<?php echo date('m')?>&ano=<?php echo date('Y')?>" id="logo">
                            <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                    </a>

                    <form method="post" id="form-redefine-senha">
                        <table cellspacing="1" id="tabela-redefine-senha">  
                            <tfoot>
                                <tr>
                                    <td><input type="submit" value="Salvar" name="btn_redefine_senha" id="btn_redefine_senha"></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td><label for="nova_senha">Nova senha:</label></td>
                                </tr>

                                <tr>
                                    <td><input type="password" id="nova_senha" autofocus name="nova_senha" class="texto" placeholder="Digite sua nova senha" required oninvalid="setCustomValidity('Por favor, digite sua nova senha.')" onchange="try {
                                                setCustomValidity('');
                                            } catch (e) {

                                            }"></td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2"><span id='notifica_senha' class="notifica_preechimento">Sua senha deve ter no mínimo<br> 8 caracteres e pelo menos 1 número.</span></td>
                                </tr>
                                
                                <tr>
                                    <td><label for="confirmar_senha">Confirmar:</label></td>
                                </tr>

                                <tr>
                                    <td><input type="password" id="confirmar_senha" name="confirmar_senha" class="texto" placeholder="Confirmar nova senha" required oninvalid="setCustomValidity('Por favor, confirme sua nova senha.')" onchange="try {
                                                setCustomValidity('');
                                            } catch (e) {

                                            }"></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
                                    <!--Fazendo uma gambiarra no CSS-->
                    <div class="clear"></div>
<!--                      Notificações-->
                    <div id="senha_sucesso" class="notificacao ok">Senha alterada com sucesso.</div>
                    <div id="erro_senhas" class="notificacao no">As senhas não correspondem.</div>
                    <div id="campos_vazios" class="notificacao no">Por favor, preencha todos os campos.</div>
                    <div id="nova_senha_menor8" class="notificacao no">Sua senha deve ter no mínimo 8 caracteres.</div>
                    <div id="nova_senha_sem_numero" class="notificacao no">Sua senha deve ter pelo menos 1 número.</div>
                    
<!--                    <img src="img/loading.svg" id="loading">-->
                
            </header>
            
            <?php
                //Tratando ação do botão salvar
                if(isset($_POST['btn_redefine_senha'])){
                    $id = $usuario['id'];
                    $nova_senha = $_POST['nova_senha'];
                    $confirmar_senha = $_POST['confirmar_senha'];
                    $erro_refine_senha = false;
                    
                    if($nova_senha == "" || $confirmar_senha == ""){
                        ?>
                        <script>
                            $('#campos_vazios').slideDown('slow');
                        </script>
                        <?php
                        $erro_refine_senha = true;
                    }
                    elseif(strlen($nova_senha) < 8){
                            ?>
                            <script>
                                $('#nova_senha_menor8').slideDown('slow');
                            </script>
                            <?php
                            $erro_refine_senha = true;
                    }
                    else{
                        $tem_numero = verificaExisteNumeros($nova_senha);
                        if($tem_numero == FALSE){
                             ?>
                            <script>
                                $('#nova_senha_sem_numero').slideDown('slow');
                            </script>
                            <?php
                            $erro_refine_senha = true;
                        }
                    }
                    
                    
                    
                    
                    
                    if($erro_refine_senha == false){
                        if($nova_senha != $confirmar_senha){
                            ?>
                            <script>
                                $('#erro_senhas').slideDown('slow');
                            </script>
                            <?php
                            $erro_refine_senha = true;
                        }
                    }
                    
                    
                    
                    
                    if($erro_refine_senha == false){
                        altera_senha($id, $nova_senha);
                        ?>
                        <script>
//                            $('#senha_sucesso').slideDown('slow');
//                            $('#loading').slideDown('normal');
                        </script>
                       <?php
//                       $mes = date('m');
//                       $ano = date('Y');
                       //Após mensagem de salvo com sucesso, 3 segundos ele direciona pra home, isso foi mt foda
//                       echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=home&mes='.$mes.'&ano='.$ano.'  "';
//                       header('Location: base.php?p=home&mes='.date("m").'&ano='.date("Y").'');
                         header('Location: base.php?p=home');
                    }
                }
            ?>
            
        </div>
        <footer><!--Rodapé-->
            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=".date('m'). "&ano=".date('Y')."' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
            <br>
            <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
        </footer>
    </body>
</html>