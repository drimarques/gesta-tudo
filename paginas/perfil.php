﻿<!DOCTYPE html>
<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}
//script responsável por carregar a foto do uauário
if ($usuario['foto'] == NULL) {
    $foto_usuario = 'user.png';
} else {
    $foto_usuario = $usuario['foto'];
}

//script responsável por atualizar o sexo do usuário
if ($usuario['sexo'] == NULL) {
    $sexo_usu = "Selecione:";
} elseif ($usuario['sexo'] == 'M') {
    $sexo_usu = "Masculino";
} else {
    $sexo_usu = "Feminino";
}
?>

<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link href="css/estilo.perfil.css" rel="stylesheet" type="text/css"> 
        <script src="jQuery/jquery-2.1.1.js"></script>
        <script src="jQuery/jquery.maskedinput.min.js"></script> 

        <!--      Mascara de telefone-->
        <script src="jQuery/jquery.maskedinput.min.js"></script> 

        <!--      Responsável pelo datepicker-->
        <link href="jQuery/jquery-ui-custom/jquery-ui.css" rel="stylesheet" type="text/css">
        <script src="jQuery/jquery-ui-custom/jquery-ui.js"></script> 

        <script>
            $(function($) {
                $("#telefone").mask("(99) 9999-9999");
                $("#celular").mask("(99) 99999-9999");
                
                $("#nascimento").mask("99/99/9999");
                
                $(".data_nasc").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    yearRange: '1900: 1998',
                    changeMonth: true,
                    changeYear: true
                });
                
                
                $("#btnCancelar_AltEmail").click(function() {
                    location.href = "http://localhost/GestaTudo/base.php?p=perfil";
                });
                

            });
        </script>      	
    </head>
    <body>
        <div id="cabecalho" class="cor_de_fundo">
            <div id="titulo">
                <img src="img/configuser.png">
                <h2>Configurações do perfil</h2>
                <p>Atenção: campos com * são obrigatórios.</p>
            </div>
        </div>

        <div id="centro_perfil" class="cor_de_fundo">
            <div id="salvando" class="notificacao ok"><?php echo "Salvando..." ?></div>
            <img src="img/loading.svg" id="loading">

            <div id="menor_idade" class="notificacao no"><?php echo "Insira uma data anterior há 1998." ?></div>
            <div id="data_invalida" class="notificacao no"><?php echo "Data inválida." ?></div>
            <div id="erro_caracteres_especiais" class="notificacao no"><?php echo "Este campo não aceita números nem caracteres especiais." ?></div>
            <div id="ed_nome_null" class="notificacao no"><?php echo "Por favor, informe o seu nome." ?></div>
            <div id="min_3" class="notificacao no"><?php echo "Este campo deve ter no mínimo 3 caracteres." ?></div>
            <div id="ed_sobrenome_null" class="notificacao no"><?php echo "Por favor, informe o seu sobrenome." ?></div>
            <div id="ed_email_null" class="notificacao no"><?php echo "Por favor, informe o seu e-mail principal." ?></div>
            <div id="ed_email_invalido" class="notificacao no"><?php echo "Formato de e-mail inválido." ?></div>
            
            
            <div id="popup_alterar_email_principal" title="Atenção!">
                <h4>Ao alterar o seu e-mail principal, enviaremos um novo e-mail de confirmação para reativar sua conta.</h4>
                <div id="botoes_salvar_email">
                    <form method="post">
                        <input type="submit" value="Ok, entendi." name="btnEntendi" id="btnEntendi">
                        <input type="button" value="Cancelar" name="btnCancelar_AltEmail" id="btnCancelar_AltEmail">
                    </form>     
                </div>
                
                 <div id="ed_email_ja_cadastrado" class="notificacao no"><?php echo "Este e-mail já está cadastrado em nosso sistema." ?></div>
            </div>
            

            <form method="post" id="form_atualiza_dados" enctype="multipart/form-data" name="form_atualiza_dados">
                <table id="dados">
                    <thead>
                        <tr>
                            <td><label for="txt_nome"><span>*</span>Nome:</label></th>
                            <td><label for="txt_sobrenome" id="senha"><span>*</span>Sobrenome:</label></th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <td><input type="submit" name="btn_salvar" id="btn_salvar" value="Salvar" class=""></td>
<!--                            <td><a href="<?php echo '?p=senha' ?>"><input type="button" name="btn_alterar_senha" id="btn_alterar_senha" value="Alterar senha" class=""></a></td>-->
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td><input type="text" id="txt_nome" name="txt_nome" maxlength="20" required oninvalid="setCustomValidity('Campo obrigatório.')" onchange="try {
                                        setCustomValidity('');
                                    } catch (e) {
                                    }" value="<?php echo $usuario['nome'] ?>" class="texto txt_pequeno"></td>
                            <td><input type="text" id="txt_sobrenome" name="txt_sobrenome" maxlength="20" required oninvalid="setCustomValidity('Campo obrigatório.')" onchange="try {
                                        setCustomValidity('');
                                    } catch (e) {
                                    }" value="<?php echo $usuario['sobrenome'] ?>" class="texto txt_pequeno"></td>
                        </tr> 

                        <tr>
                            <td><label for="nascimento"><span>*</span>Data de nascimento:</label></td>
                            <td><label for="sexo">Sexo:</label></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="nascimento" class="texto txt_pequeno data_nasc" id="nascimento" value="<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>" required oninvalid="setCustomValidity('Por favor, informe sua data de nascimento.')" onchange="try {
                                        setCustomValidity('');
                                    } catch (e) {
                                    }"></td>

                            <td><select name="sexo" class="texto txt_pequeno" id="sexo">
                                    <option selected disabled><?php echo $sexo_usu ?></option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Feminino</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><label for="txt_email"><span>*</span>E-mail:</label></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="email" name="txt_email" maxlength="255" id="txt_email" required oninvalid="setCustomValidity('Por favor, digite um e-mail válido.')" onchange="try {
                                        setCustomValidity('');
                                    } catch (e) {
                                    }" value="<?php echo $usuario['email'] ?>" class="texto txt_grande"></td>
                        </tr>
<!--                        <tr>
                            <td colspan="2"><label for="txt_conf_senha">E-mail secundário:</label></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="email" placeholder="Digite seu e-mail secundário" id="txt_email_sec"name="txt_email_sec" class="texto txt_grande" value="<?php //echo $usuario['email_sec'] ?>"></td>
                        </tr>-->
                        <tr>
                            <td><label for="telefone">Telefone:</label></td>
                            <td><label for="telefone">Celular:</label></td>
                        </tr>
                        <tr>
                            <td><input type="text" name="telefone" id="telefone" placeholder="Digite seu telefone" class="texto txt_pequeno" value="<?php echo $usuario['telefone'] ?>"></td>
                            <td><input type="text" name="celular" id="celular" placeholder="Didite seu celular" class="texto txt_pequeno" value="<?php echo $usuario['celular'] ?>"></td>  
                        </tr>                                                     
                    </tbody>                              
                </table>

                <table id="foto">
                    <thead>
                        <tr>
                            <td align="center"><a href="#envia_foto">
                                    <img src="img_perfil_usuarios/<?php echo $foto_usuario ?>">
                                </a>
                            </td>
                        </tr>	
                    </thead>
                    <tfoot>
                        <tr>
                            <td><input type="file" name="envia_foto" accept="image/jpeg, image/x-png"></td>
                        </tr>
                    </tfoot>
                </table>
            </form>
            
            <form method="post">
                <input type="submit" name="btnExcluirFoto" id="btnExcluirFoto" value="Excluir foto do perfil">
            </form>
            
        
        <?php
            //Botão excluir foto do perfil
            if (isset($_POST['btnExcluirFoto'])) {
                $id_usuario = $usuario['id'];
                excluiFotoPerfil($id_usuario);
                header('Location: base.php?p=perfil');
            }
        
        
            //Botão salvar as alterações de perfil
            if (isset($_POST['btn_salvar'])) {
                $id_usuario = $usuario['id'];
                $nome = $_POST['txt_nome'];
                $sobrenome = $_POST['txt_sobrenome'];
                $nascimento = $_POST['nascimento'];
                $sexo = $_POST['sexo'];
                $email = $_POST['txt_email'];
                //$email2 = $_POST['txt_email_sec'];
                $telefone = $_POST['telefone'];
                $celular = $_POST['celular'];

                //Tratando caso o cara altere o e-mail principal
                if($usuario[email] != $email){
                    header('Location: base.php?p=perfil&newEmailPrinc=true&value='.$email);
                }


                $erro_atualiza_perfil = false;

                if ($sexo == NULL) {
                    $sexo = $usuario['sexo'];
                }






                $foto = $_FILES['envia_foto']['name'];
                //Se o usuário selecionar uma foto, faz o upload e atualiza no banco o nome do arquivo
                if ($foto != NULL) {
                    $uploaddir = 'img_perfil_usuarios/';
                    move_uploaded_file($_FILES['envia_foto']['tmp_name'], $uploaddir . $_FILES['envia_foto']['name']);

                    $rename = time().'.jpg';

                    $nome_antigo = 'img_perfil_usuarios/'.$foto;
                    $nome_novo = 'img_perfil_usuarios/'.$rename;

                    rename($nome_antigo, $nome_novo);
                    atualiza_foto($id_usuario, $rename);
                }

                //Tratando campo nome
                if($nome == ""){
                    ?>
                    <script>
                        $('#ed_nome_null').slideDown('slow');
                        $("#txt_nome").css({"border": "solid 1px red"});

                        document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                        document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                        document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                        document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                        document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                        document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                        document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";

                    </script>
                    <?php
                    $erro_atualiza_perfil = true;
                }
                elseif(strlen($nome) < 3){
                    ?>
                    <script>
                        $('#min_3').slideDown('slow');
                        $("#txt_nome").css({"border": "solid 1px red"});

                        document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                        document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                        document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                        document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                        document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                        document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                        document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                    </script>
                    <?php
                    $erro_atualiza_perfil = true;
                }
                else{
                    $caracteres_especiais = verifica_caracteres($nome);
                    if($caracteres_especiais){
                        ?>
                        <script>
                            $('#erro_caracteres_especiais').slideDown('slow');
                            $("#txt_nome").css({"border": "solid 1px red"});

                            document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                            document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                            document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                            document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                            document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                            document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                            document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                        </script>
                        <?php
                        $erro_atualiza_perfil = true;
                    }
                }



                //Tratando campo sobrenome
                if($erro_atualiza_perfil == false){
                    if($sobrenome == ""){
                        ?>
                        <script>
                            $('#ed_sobrenome_null').slideDown('slow');
                            $("#txt_sobrenome").css({"border": "solid 1px red"});

                            document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                            document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                            document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                            document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                            document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                            document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                            document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                        </script>
                        <?php
                        $erro_atualiza_perfil = true;
                    }
                    elseif(strlen($sobrenome) < 3){
                        ?>
                        <script>
                            $('#min_3').slideDown('slow');
                            $("#txt_sobrenome").css({"border": "solid 1px red"});

                            document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                            document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                            document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                            document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                            document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                            document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                            document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                        </script>
                        <?php
                        $erro_atualiza_perfil = true;
                    }
                    else{
                        $caracteres_especiais = verifica_caracteres($sobrenome);
                        if($caracteres_especiais){
                            ?>
                            <script>
                                $('#erro_caracteres_especiais').slideDown('slow');
                                $("#txt_sobrenome").css({"border": "solid 1px red"});

                                document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                                document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                                document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                                document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                                document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                                document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                            </script>
                            <?php
                            $erro_atualiza_perfil = true;
                        }
                    }
                }



                //Tratando campo e-mail
                if($erro_atualiza_perfil == false){
                    if($email == ""){
                        ?>
                        <script>
                            $('#ed_email_null').slideDown('slow');
                            $("#txt_email").css({"border": "solid 1px red"});

                            document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                            document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                            document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                            document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                            document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                            document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                            document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                        </script>
                        <?php
                        $erro_atualiza_perfil = true;
                    }
                    // Também verifica se não existe nenhum erro anterior
                    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    ?>
                        <script>
                            $('#ed_email_invalido').slideDown('slow');
                            $("#txt_email").css({"border": "solid 1px red"});

                            document.form_atualiza_dados.txt_nome.value = "<?php echo $nome ?>";
                            document.form_atualiza_dados.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                            document.form_atualiza_dados.nascimento.value = "<?php echo date("d/m/Y", strtotime($usuario[nascimento])) ?>";
                            document.form_atualiza_dados.txt_email.value = "<?php echo $email ?>";
                            document.form_atualiza_dados.txt_email_sec.value = "<?php echo $email2 ?>";
                            document.form_atualiza_dados.telefone.value = "<?php echo $telefone ?>";
                            document.form_atualiza_dados.celular.value = "<?php echo $celular ?>";
                        </script>
                        <?php
                        $erro_atualiza_perfil = true;        
                    }
                }
            
                if($erro_atualiza_perfil == false){

                    $nascimento = explode("/", "$nascimento");
                    $dia = $nascimento[0];
                    $mes = $nascimento[1];
                    $ano = $nascimento[2];
                    if (checkdate($mes, $dia, $ano)) {
                        if ($ano > 1998) {
                            ?>
                            <script>
                                $('#menor_idade').slideDown('slow');
                                $("#nascimento").css({"border": "solid 1px red"});
                            </script>
                            <?php
                        } 
                        else {  
                            
                            $nascimento = $ano;
                            $nascimento .= "-$mes";
                            $nascimento .= "-$dia";
                            //$update = atualiza_cadastro($id_usuario, $nome, $sobrenome, $nascimento, $sexo, $email2, $telefone, $celular);
                            $update = atualiza_cadastro($id_usuario, $nome, $sobrenome, $nascimento, $sexo, $telefone, $celular);
                            if ($update) {
                                ?>                       	
                                <script>
                                    $('#salvando').slideDown('normal');
                                    $('#loading').slideDown('normal');
                                </script>
                                <?php
                                
                                //2 segundos pra página atualizar com as alterações
                                echo '<meta http-equiv="refresh" content="1">';
                            }
                        }
                    }
                    else{
                        ?>
                        <script>
                            $('#data_invalida').slideDown('slow');
                            $("#nascimento").css({"border": "solid 1px red"});
                        </script>
                        <?php 
                    }
                }  
            }
        
        
        
        
        
        
        
        
        
        
        if($_GET['newEmailPrinc'] == 'true' && $_GET['value'] != null  ) {
            ?>
            <script>
                $("#popup_alterar_email_principal").dialog({
                    width: 480,
                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                    resizable: false,
                    close: function() {
                        location.href = "http://localhost/GestaTudo/base.php?p=perfil";
                    }
                });
            </script>
            <?php
        }
        
        //Se o cara quiser mesmo alterar o e-mail principal
        if(isset($_POST['btnEntendi'])) {
            $novoEmail = $_GET['value'];
            
            $jaCadastrado = verifica_email_cadastrado($novoEmail);
            
            if($jaCadastrado){
                ?>
                <script>
                    $("#popup_alterar_email_principal").dialog({
                        width: 480,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            location.href = "http://localhost/GestaTudo/base.php?p=perfil";
                        }
                    });
                    
                    $("#ed_email_ja_cadastrado").slideToggle("normal");
                </script>
                <?php
            }
            else{
                $alteroueEnviou = alteraEmailPrincipal($id_usuario, $novoEmail);

                if($alteroueEnviou){
                    //echo '<meta http-equiv="refresh" content="1">';
                    header('Location: index.php');
                }
            }
            
        }
        
        
        ?> 
                        
                        
        </div>
    </body>
</html>