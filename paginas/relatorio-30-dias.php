<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}
?>

<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/relatorios.css" rel="stylesheet" type="text/css">

        <script>
            $(document).ready(function() {

                

            });
        </script>

    </head>
    <body>
        <?php
            $id_usuario = $usuario['id'];
        ?>

        <div id="topo_relatorio" class="cor_de_fundo">
            <h3 class="">Extrato últimos 30 dias</h3>
        </div>
        
            <div id="relatorio" class="cor_de_fundo">
                <?php 
                    exibe_relatorio_30dias($id_usuario);
                ?>
            </div>
           

    </body>
</html>