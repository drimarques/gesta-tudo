<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}

?>

<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/relatorios.css" rel="stylesheet" type="text/css">

        <script>
            $(document).ready(function() {

                

            });
        </script>

    </head>
    <body>
        <?php
            $id_usuario = $usuario['id'];
            $data_inicio = $_GET['dt_inicio'];
            $data_fim = $_GET['dt_fim'];
            $trata_erro_datas = false;

            //Se as datas tiverem brancas
            if (($data_inicio == null) || ($data_fim == null)) {
                header('Location: base.php?p=home');
                $trata_erro_datas = true;
            }

            //Senão tiverem brancas, verifica se são válidas
            else {

                //Data DE:
                $data_inicio = explode("-", "$data_inicio");
                $dia_de = $data_inicio[2];
                $mes_de = $data_inicio[1];
                $ano_de = $data_inicio[0];

                //Data ATE:
                $data_fim = explode("-", "$data_fim");
                $dia_ate = $data_fim[2];
                $mes_ate = $data_fim[1];
                $ano_ate = $data_fim[0];

                if (!(checkdate($mes_de, $dia_de, $ano_de))) {
                    header('Location: base.php?p=home');
                    $trata_erro_datas = true;
                }

                if (!(checkdate($mes_ate, $dia_ate, $ano_ate))) {
                    header('Location: base.php?p=home');
                    $trata_erro_datas = true;
                }
            }

            if ($trata_erro_datas == false) {
                $data_inicio = $ano_de;
                $data_inicio .= "-$mes_de";
                $data_inicio .= "-$dia_de";

                $data_fim = $ano_ate;
                $data_fim .= "-$mes_ate";
                $data_fim .= "-$dia_ate";

                if ($data_inicio > $data_fim) {
                    header('Location: base.php?p=home');
                }
            }
        ?>

        <div id="topo_relatorio" class="cor_de_fundo">
            <h3 class="">Extrato do período: <?php echo date("d/m/Y", strtotime($data_inicio))." à ".date("d/m/Y", strtotime($data_fim)) ?></h3>
        </div>
        
            <div id="relatorio" class="cor_de_fundo">
                <?php 
                    exibe_relatorio_por_periodo($id_usuario, $data_inicio, $data_fim);
                ?>
            </div>
           

    </body>
</html>
