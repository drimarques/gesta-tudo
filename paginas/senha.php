﻿<!DOCTYPE HTML>
<?php
   session_start();
   if(!isset($_SESSION['usuario_logado'])){
      header("Location: index.php");
      exit;
   }
?>
<html lang="pt-br">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
      <link href="css/estilo.perfil.css" rel="stylesheet" type="text/css"> 

      <script>
          $(function($) {
          //Tratando focus nos campos
                $("#senha_nova").focus(function(){
                    $('#notifica_senha').slideDown('normal');
                });
                
                $("#senha_nova").blur(function(){
                    $('#notifica_senha').slideUp('normal');
                });
          });
      </script>
      
      
   </head>
  <body>
      <div id="central_senha" class="cor_de_fundo">  
      
        <div id="cabecalho">
           <div id="titulo-altera-senha">
              <img src="img/password.png">
              <h2>Alterar senha</h2>
           </div>
        </div>
        <form method="post" id="form_altera_senha" name="form_altera_senha">
           <table cellspacing="1" id="altera-senha">  
              <thead>
                 <tr>
                    <td><label for="senha_atual"><span>*</span>Senha atual:</label></td>
                 </tr>
              </thead>
              <tfoot>
                 <tr>
                    <td><input type="submit" value="Salvar" name="btn_salvar_senha" id="btn_salvar_senha"></td>
                 </tr>
              </tfoot>
  <!--               <tr>
                    <td><div class="notificacao ok" id="senha_sucesso"><?php echo "Senha alterada com sucesso!" ?></div></td>
                 </tr>

                 <tr>
                    <td><div class="notificacao no" id="erro_atual"><?php echo "Senha atual incorreta." ?></div></td>
                 </tr>
                 <tr>
                    <td><div class="notificacao no" id="erro_nova_senha"><?php echo "As senhas não correspondem." ?></div></td>
                 </tr>
              </tfoot>-->

              <tbody>
                 <tr>
                    <td><input type="password" id="senha_atual" name="senha_atual" maxlength="255" class="texto padding_texto tamanho-senha" placeholder="Senha atual" required oninvalid="setCustomValidity('Senha atual obrigatória.')" onchange="try{setCustomValidity('');}catch(e){}"></td>
                 </tr>

                 <tr>
                    <td><label for="senha_nova"><span>*</span>Nova senha:</label></td>
                 </tr>

                 <tr>
                    <td><input type="password" id="senha_nova" name="senha_nova" maxlength="255" class="texto padding_texto tamanho-senha" placeholder="Nova senha" required oninvalid="setCustomValidity('Digite sua nova senha.')" onchange="try{setCustomValidity('');}catch(e){}"></td>
                 </tr>
                 <tr>
                     <td><div id="notifica_senha" class="notifica_preechimento">No mínimo 8 caracteres e pelo <br> menos 1 número.</div></td>
                 </tr>

                 <tr>
                    <td><label for="senha_nova2"><span>*</span>Confirmar nova senha:</label></td>
                 </tr>

                 <tr>
                    <td><input type="password" id="senha_nova2" name="senha_nova2" maxlength="255" class="texto padding_texto tamanho-senha" placeholder="Novamente" required oninvalid="setCustomValidity('Confirme sua nova senha.')" onchange="try{setCustomValidity('');}catch(e){}"></td>
                 </tr>
              </tbody>
           </table>
        </form>

        <!--NOTIFICAÇÕES-->
        <div class="notificacao ok" id="senha_sucesso"><?php echo "Senha alterada com sucesso!" ?></div>
        <div class="notificacao no" id="erro_nova_senha"><?php echo "As senhas não correspondem." ?></div>
        <div class="notificacao no" id="erro_atual"><?php echo "Senha atual incorreta." ?></div>
        <div class="notificacao no" id="senha_atual_null"><?php echo "Senha atual obrigatória." ?></div>
        <div class="notificacao no" id="senha_nova_null"><?php echo "Nova senha obrigatória." ?></div>
        <div class="notificacao no" id="conf_nova_senha_null"><?php echo "Por favor confirme sua senha." ?></div>
        <div class="notificacao no" id="menor_8"><?php echo "Sua senha deve ter no mínimo 8 caracteres." ?></div>
        <div class="notificacao no" id="nao_numero"><?php echo "Sua senha deve ter no mínimo 8 caracteres e pelo menos 1 número." ?></div>


              <?php
                 if(isset($_POST['btn_salvar_senha'])){
                    $id_usuario = $usuario['id'];
                    $email = $usuario['email'];
                    $senha_atual = $_POST['senha_atual'];
                    $novasenha = $_POST['senha_nova'];
                    $novasenha2 = $_POST['senha_nova2'];

                    $erro_senha_atual = FALSE;


                          //SE A SENHA ATUAL FOR NULL
                          if($senha_atual == ""){
                              ?>
                              <script>
                                  $('#senha_atual_null').slideDown('slow');
                                  $('#senha_atual').css({"border":"red 1px solid"});
                                  document.form_altera_senha.senha_atual.value = "<?php echo $senha_atual ?>";
                                  document.form_altera_senha.senha_nova.value = "<?php echo $novasenha ?>";
                                  document.form_altera_senha.senha_nova2.value = "<?php echo $novasenha2 ?>";
                              </script>
                              <?php
                              $erro_senha_atual = TRUE;
                          }


                          //Verificando se a senha atual é igual a senha do usuário
                          if($erro_senha_atual == false){
                              if($usuario['senha'] != $senha_atual){
                                $erro_senha_atual = TRUE;
                                ?>
                                <script>
                                    $('#erro_atual').slideDown('slow');
                                    $('#senha_atual').css({"border":"red 1px solid"});
                                      document.form_altera_senha.senha_atual.value = "<?php echo $senha_atual ?>";
                                      document.form_altera_senha.senha_nova.value = "<?php echo $novasenha ?>";
                                      document.form_altera_senha.senha_nova2.value = "<?php echo $novasenha2 ?>";
                                </script>
                                <?php
                              }
                          }



                          //Tratando nova senha
                          if($erro_senha_atual == false){
                              if($novasenha == ""){
                                  ?>
                                  <script>
                                      $('#senha_nova_null').slideDown('slow');
                                      $('#senha_nova').css({"border":"red 1px solid"});
                                      document.form_altera_senha.senha_atual.value = "<?php echo $senha_atual ?>";
                                      document.form_altera_senha.senha_nova.value = "<?php echo $novasenha ?>";
                                      document.form_altera_senha.senha_nova2.value = "<?php echo $novasenha2 ?>";
                                  </script>
                                  <?php
                                  $erro_senha_atual = TRUE;
                              }
                              elseif(strlen($novasenha) < 8){
                                  ?>
                                  <script>
                                      $('#nao_numero').slideDown('slow');
                                      $('#senha_nova').css({"border":"red 1px solid"});
                                      document.form_altera_senha.senha_atual.value = "<?php echo $senha_atual ?>";
                                      document.form_altera_senha.senha_nova.value = "<?php echo $novasenha ?>";
                                      document.form_altera_senha.senha_nova2.value = "<?php echo $novasenha2 ?>";
                                  </script>
                                  <?php
                                  $erro_senha_atual = TRUE;
                              }
                              else{
                                  $tem_numero = verificaExisteNumeros($novasenha);
                                  if($tem_numero == FALSE){
                                      ?>
                                      <script>
                                          $('#nao_numero').slideDown('slow');
                                          $('#senha_nova').css({"border":"red 1px solid"});
                                          document.form_altera_senha.senha_atual.value = "<?php echo $senha_atual ?>";
                                          document.form_altera_senha.senha_nova.value = "<?php echo $novasenha ?>";
                                          document.form_altera_senha.senha_nova2.value = "<?php echo $novasenha2 ?>";
                                      </script>
                                      <?php
                                      $erro_senha_atual = TRUE;
                                  }
                              }
                          }



                          //Verificando se as novas senhas são iguais
                          if($erro_senha_atual == FALSE){
                            if($novasenha != $novasenha2){
                             $erro_senha_atual = TRUE;
                            ?>
                             <script>
                                  $('#erro_nova_senha').slideDown('slow');
                                  $('#senha_nova').css({"border":"red 1px solid"});
                                  $('#senha_nova2').css({"border":"red 1px solid"});
                                  document.form_altera_senha.senha_atual.value = "<?php echo $senha_atual ?>";
                                  document.form_altera_senha.senha_nova.value = "<?php echo $novasenha ?>";
                                  document.form_altera_senha.senha_nova2.value = "<?php echo $novasenha2 ?>";
                             </script>
                             <?php
                             }
                          }






                          if($erro_senha_atual == FALSE){
                             $atualiza_senha = altera_senha($id_usuario, $novasenha);

                             if($atualiza_senha){
                                email_nova_senha($email,$novasenha);
                                ?>
                                   <script>
                                         $('#senha_sucesso').slideDown('slow');	
                                   </script>
                                <?php
                             }
                          }







                 }
              ?>
                                 
      </div>
</body>
</html>