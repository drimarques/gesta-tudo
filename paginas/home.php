<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}
?>
<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/estilo.home.css" rel="stylesheet" type="text/css">
        <script src="highCharts/highcharts.js"></script>
        <script src="highCharts/highcharts-3d.js"></script>
        <script src="highCharts/modules/exporting.js"></script>

        <?php
        $id_usuario = $usuario['id'];
        $dia_atual = date('d');
        $mes_atual = date('m');
        $ano_atual = date('Y');
        //echo $total;
        //echo '<br>Saldo atual: R$ '.number_format($total, 2, ',' , '.');  
//            if(!isset($_POST['btn_buscar'])){
//                $mes = $_GET['mes'];
//                $ano = $_GET['ano'];
//                
//                if( (($mes > 12 || $mes < 1)) || ($ano < 2014) ){
//                    $mes = date('m');
//                    $ano = date('Y');
//                    header('Location: base.php?p=home&mes='.$mes.'&ano='.$ano.'');
//                }
//                
//            }
//            if(isset($_POST['btn_buscar'])){
//                $mes = $_POST['mes'];
//                $ano = $_POST['ano'];
//                header('Location: base.php?p=home&mes='.$mes.'&ano='.$ano.'');
//            }


        /* Obtendo dados pro gráfico */
        /* Receitas */
        $receitasJan = retorna_receitas_mes($id_usuario, 1, $ano_atual);
        $receitasFev = retorna_receitas_mes($id_usuario, 2, $ano_atual);
        $receitasMar = retorna_receitas_mes($id_usuario, 3, $ano_atual);
        $receitasAbr = retorna_receitas_mes($id_usuario, 4, $ano_atual);
        $receitasMai = retorna_receitas_mes($id_usuario, 5, $ano_atual);
        $receitasJun = retorna_receitas_mes($id_usuario, 6, $ano_atual);
        $receitasJul = retorna_receitas_mes($id_usuario, 7, $ano_atual);
        $receitasAgo = retorna_receitas_mes($id_usuario, 8, $ano_atual);
        $receitasSet = retorna_receitas_mes($id_usuario, 9, $ano_atual);
        $receitasOut = retorna_receitas_mes($id_usuario, 10, $ano_atual);
        $receitasNov = retorna_receitas_mes($id_usuario, 11, $ano_atual);
        $receitasDez = retorna_receitas_mes($id_usuario, 12, $ano_atual);

        /* Despesas */
        $despesasJan = retorna_despesas_mes($id_usuario, 1, $ano_atual);
        $despesasFev = retorna_despesas_mes($id_usuario, 2, $ano_atual);
        $despesasMar = retorna_despesas_mes($id_usuario, 3, $ano_atual);
        $despesasAbr = retorna_despesas_mes($id_usuario, 4, $ano_atual);
        $despesasMai = retorna_despesas_mes($id_usuario, 5, $ano_atual);
        $despesasJun = retorna_despesas_mes($id_usuario, 6, $ano_atual);
        $despesasJul = retorna_despesas_mes($id_usuario, 7, $ano_atual);
        $despesasAgo = retorna_despesas_mes($id_usuario, 8, $ano_atual);
        $despesasSet = retorna_despesas_mes($id_usuario, 9, $ano_atual);
        $despesasOut = retorna_despesas_mes($id_usuario, 10, $ano_atual);
        $despesasNov = retorna_despesas_mes($id_usuario, 11, $ano_atual);
        $despesasDez = retorna_despesas_mes($id_usuario, 12, $ano_atual);
        ?>

        <script>
            $(function () {
                $('#grafico_geral').highcharts({
                    chart: {
                        type: 'column',
                        options3d: {
                            enabled: true,
                            alpha: 10,
                            beta: 0,
                            viewDistance: 25,
                            depth: 50
                        },
                        marginTop: 60,
                        marginRight: 40,
                        style: {
                            fontFamily: 'Roboto, Tahoma, sans-serif'
                        }
                        
                    },
                    title: {
                        text: 'Total de receitas e despesas por mês - <?php echo date ('Y');?>'

                    },
                    xAxis: {
                        categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
                    },
                    yAxis: {
                        allowDecimals: true,
                        min: 0,
                        title: {
                            text: 'Valor'
                        }
                    },
                    tooltip: {
                        headerFormat: '<b> {point.key} </b> <br>',
                        pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: R$ {point.y:f}'
                    },
                    plotOptions: {
                        column: {
//                            stacking: 'normal',
                            depth: 50
                        }
                    },
                    series: [{
                            name: 'Receitas',
                            data: [
                            <?php echo $receitasJan ?>,
                            <?php echo $receitasFev ?>,
                            <?php echo $receitasMar ?>,
                            <?php echo $receitasAbr ?>,
                            <?php echo $receitasMai ?>,
                            <?php echo $receitasJun ?>,
                            <?php echo $receitasJul ?>,
                            <?php echo $receitasAgo ?>,
                            <?php echo $receitasSet ?>,
                            <?php echo $receitasOut ?>,
                            <?php echo $receitasNov ?>,
                            <?php echo $receitasDez ?>
                            ],
//                            stack: '0',
                            color: '#099aff'
                        }, {
                            name: 'Despesas',
                            data: [
                            <?php echo $despesasJan ?>,
                            <?php echo $despesasFev ?>,
                            <?php echo $despesasMar ?>,
                            <?php echo $despesasAbr ?>,
                            <?php echo $despesasMai ?>,
                            <?php echo $despesasJun ?>,
                            <?php echo $despesasJul ?>,
                            <?php echo $despesasAgo ?>,
                            <?php echo $despesasSet ?>,
                            <?php echo $despesasOut ?>,
                            <?php echo $despesasNov ?>,
                            <?php echo $despesasDez ?>
                            ],
//                            stack: '1',
                            color: '#ff4b51'
                        }]
                });
                
                
                
                $("#previsao_do_mes").mouseover(function(){
//                    $("#notificacaoDePrevisao").css({"display":"block"});
                    $("#notificacaoDePrevisao").slideDown("normal");
		});
                
                $("#lado_esquerdo").mouseover(function(){
//                    $("#notificacaoDePrevisao").css({"display":"none"});
                    $("#notificacaoDePrevisao").slideUp("normal");
		});
                
            });
            
        </script>


    </head>
    <body>


        <!--Pra funcionar normal no firefox, no IE..-->
        <div class="clear2"></div>

        <div id="top" class="cor_de_fundo">
            <section id='sessao_busca'>
                <!--                <form name="busca" id="busca" method="post">
                                    <select id="mes" name="mes" class="texto">
                                        <option value="01">Janeiro</option>
                                        <option value="02">Fevereiro</option>
                                        <option value="03">Março</option>
                                        <option value="04">Abril</option>
                                        <option value="05">Maio</option>
                                        <option value="06">Junho</option>
                                        <option value="07">Julho</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option>
                                    </select>
                
                                    <select id="ano" name="ano" class="texto">
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                
                                    <button type="submit" name="btn_buscar" id="btn_buscar" title="Pesquisar">Buscarimg src="img/search.png"></button>
                                </form>-->
            </section>
                <?php
                $nome_mes = retorna_nome_mes($mes_atual);

                echo "<div id='div_titulo_mes'>
                        <h3 id='titulo_mes'>" . $dia_atual . " de " . $nome_mes . " de " . $ano_atual . "</h3>
                      </div>";
                //$mes = retorna_numero_mes($mes);
                ?>
        </div>



        <div class="clear2"></div>

        <div id='lado_esquerdo' class="cor_de_fundo">

            <div id="contas_a_pagar">
                <h3>CONTAS A PAGAR</h3>
                    <?php
                        retorna_contas_a_pagar($id_usuario);
                    ?>
            </div>
            
            <div id="grafico_geral"></div>
            <?php
                $sql = "SELECT * FROM movimentacoes where id_usuario = '$id_usuario' and pago_recebido = '1'";
                $busca = mysql_query($sql);
                $tem_registros = mysql_num_rows($busca);

                if($tem_registros == 0){
                    ?>
                        <script>
                            //$("#grafico_geral").css({"display":"none"});
                        </script>
                    <?php
                }
            ?>

            
        </div>


        <div id="lado_direito" class="cor_de_fundo">
            
            <div id="contas_a_receber">
                <h3>CONTAS A RECEBER</h3>
                    <?php
                        retorna_contas_a_receber($id_usuario);
                    ?>
            </div>
            
            

            <div id="previsao_do_mes">
                <h3>PREVISÃO</h3>

                <div id='previsao_receita'>
                    <span class="dinheiro_azul">
                        <?php
                            $total_receitas = retorna_total_receitas($id_usuario, $mes_atual, $ano_atual);
                            echo "R$ " . number_format($total_receitas, 2, ',', '.');
                        ?>
                    </span>
                     <div>Total de receitas pendentes</div>
                </div>

                <div id='previsao_despesa'>
                    <span class="dinheiro_vermelho">
                        <?php
                            $total_despesas = retorna_total_despesas($id_usuario, $mes_atual, $ano_atual);
                            echo "R$ -" . number_format($total_despesas, 2, ',', '.');
                        ?>
                    </span>
                    <div>Total de despesas pendentes</div>
                </div>
                
                <div id="previsao_saldo">
                    <?php
                        $total_saldoPrevisto = retorna_saldo_previsto($id_usuario, $mes_atual, $ano_atual);
                        if ($total_saldoPrevisto >= 0) {
                            $classe_variavel = "dinheiro_azul";
                        } 
                        else {
                            $classe_variavel = "dinheiro_vermelho";
                        }
                        echo "<span class='$classe_variavel'>R$ " . number_format($total_saldoPrevisto, 2, ',', '.') . "</span>";
                    ?>
                    <div>Saldo previsto para o próximo mês</div>
                </div>
                
                <div class='clear2'></div>
                <div id="notificacaoDePrevisao">Se você receber e pagar todas movimentações de <?php echo retorna_nome_mes($mes_atual) ?>, seu saldo será de <?php echo "R$ ".number_format($total_saldoPrevisto, 2, ',', '.') ?> no início de 
                    <?php 
                        if ($mes_atual == 12){
                            echo retorna_nome_mes(1);
                        }
                        else{
                            echo retorna_nome_mes($mes_atual+1);
                        }
                    ?>.</div>
            </div> 
        </div>

        
        <!--Calculando se tem contas a pagar ou receber, para vizualizar melhor o layout-->
        <?php 
            $temMovsPendentes = verificaMovimentacoesPendentes($id_usuario, $mes_atual, $ano_atual);
            
            if($temMovsPendentes == FALSE){
                ?>
                <script>
                    $("#contas_a_pagar").css({"height":"150px"});
                    $("#contas_a_receber").css({"height":"150px"});
                </script>
                <?php
            }
            else{
                ?>
                <script>
                    $(".sem_resultados").css({"margin-top":"120px"});
                </script>
                <?php
            }
            
        ?>
        
        
    </body>
</html>