﻿<!DOCTYPE HTML>
<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}
?>
<html lang="pt-br">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link href="css/estilo.movimentacoes.css" rel="stylesheet" type="text/css">
        <script src="js/movimentacoes.filtros.js"></script>

        <!--Mascara R$ 000,00 no valor-->
        <script src="jQuery/jquery.maskMoney.js"></script>

        <script>
            $(function() {
                
                $(".data").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true,
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    yearRange: '2000:2100',
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });
                
                
                $("#recebimento").mask("99/99/9999");
                $("#vencimento").mask("99/99/9999");
                $("#receitas_de").mask("99/99/9999");
                $("#receitas_ate").mask("99/99/9999");
                

                $("#valor_despesa").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
                $("#valor_receita").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
                $("#valor_atual").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
                $("#valor_atual_fixa").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
                //$("#dt_recebimento").css({"display":"none"});




                $("#receita_unica").click(function() {
                    $("#receita_ate").hide("normal");
                    $("#dia_fixo_receita").hide("normal");
                    $("#dt_recebimento").show("normal");
                    $("#checkBox_recebido").show("normal"); 
                });

                $("#receita_fixa").click(function() {
                    $("#receita_ate").show("normal");
                    $("#dia_fixo_receita").show("normal");
                    $("#dt_recebimento").hide("normal");
                    $("#checkBox_recebido").hide("normal"); 
                });







                /* ************************** Responsável pela chamada do popup "nova receita" **********************************/
                $("#chama_receita").click(function() {
                    $("#popup_nova_receita").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $_GET['dt_de'] ?>&dt_ate=<?php echo $_GET['dt_ate'] ?> ";
                            $("#popup_nova_categoria_receita").dialog('close');
                        }
                    });
                });

                //responsável pela chamada do popup "nova categoria receita"
                $("#add_receita").click(function() {
                    $("#popup_nova_categoria_receita").dialog({
                        height: 160,
                        width: 245,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $_GET['dt_de'] ?>&dt_ate=<?php echo $_GET['dt_ate'] ?> ";
                        }
                    });
                });

                //********************************************************************************************************************

                $("#despesa_unica").click(function() {
                    $("#despesa_ate").hide("normal");
                    $("#dia_fixo_despesa").hide("normal");
                    $("#dt_vencimento").show("normal");
                    $("#checkBox_pago").show("normal");  
                });

                $("#despesa_fixa").click(function() {
                    $("#despesa_ate").show("normal");
                    $("#dia_fixo_despesa").show("normal");
                    $("#dt_vencimento").hide("normal");
                    $("#checkBox_pago").hide("normal");                    
                });

                //responsável pela chamada do popup "nova receita"
                $("#chama_despesa").click(function() {
                    $("#popup_nova_despesa").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $_GET['dt_de'] ?>&dt_ate=<?php echo $_GET['dt_ate'] ?> ";
                            $("#popup_nova_categoria_despesa").dialog('close');
                        }
                    });
                });

                //responsável pela chamada do popup "nova categoria receita"
                $("#add_despesa").click(function() {
                    $("#popup_nova_categoria_despesa").dialog({
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        height: 160,
                        width: 245,
                        close: function() {
                            //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $_GET['dt_de'] ?>&dt_ate=<?php echo $_GET['dt_ate'] ?> ";
                        }
                    });
                });


            });
        </script>
    </head>        
    <body>
        <?php
        /* function converte_data($data){
          $d=explode("-",$data);
          $data=$d[2]."/".$d[1]."/".$d[0];
          return $data;
          }
          echo converte_data($usuario['teste_data']);
          echo "Total: R$ ".number_format($usuario['valor'],2,',','.');

          $mes = '09';
          <?php echo date("Y-$mes-01")?> */
            $id_usuario = $usuario['id'];
                
        
        ?>   

        <!---------------------------- POPUP NOVA DESPESA ---------------------------------------------->
        <div id="popup_nova_despesa" title="Nova Despesa">
            <div id="bloco_despesa">
                
                <!--Erros-->
                <div id="erro_dt_vencimento" class="notificacao no erro_lancamento">Informe a data do vencimento.</div>
                <div id="erro_informe_valor_despesa" class="notificacao no erro_lancamento">Informe o valor da despesa.</div>
                <div id="erro_informe_datafim_despesa" class="notificacao no erro_lancamento">Informe a data final.</div>
                <div id="erro_informe_dia_vencimento" class="notificacao no erro_lancamento">Informe o dia do vencimento.</div>
                <div id="data_vencimento_invalida" class="notificacao no erro_lancamento">Data inválida.</div>
                <div id="informe_categoria_despesa" class="notificacao no erro_lancamento">Informe uma categoria.</div>
                <div id="erro_data_fim_menor_que_data_atual" class="notificacao no erro_lancamento">A data final deve ser maior que a data atual.</div>
                <div id="erro_data_fim_menor_que_data_atual2" class="notificacao no erro_lancamento">Se o mês final for o mês atual, o dia do vencimento deve ser maior do que o dia atual.</div>
                
                <!--/Erros-->

                <form method="post" enctype="multipart/form-data" name="nova_despesa">
                    <ul>
                        <li class="tipo"><input type="radio" checked name="tipo" value="unica" id="despesa_unica"> <label for="despesa_unica">Única</label></li>
                        <li class="tipo"><input type="radio" name="tipo" value="fixa" id="despesa_fixa"> <label for="despesa_fixa">Fixa</label></li>
                    </ul>



                    <!--                    <div id="despesa_ate">
                                            <label for="despesa_fixa_ate">Até quando?</label>
                                            <br>
                                            <input type="month" name="despesa_fixa_ate" id="despesa_fixa_ate" class="texto" min="<?php //echo date('Y-m')  ?>">
                                        </div>-->



                    <div id="despesa_ate">
                        <label>Até quando?</label>
                        <br>
                        <select id="mes_fim_despesa" name="mes_fim_despesa" class="texto">
                            <option selected="" value="" disabled="">Mês</option>
                            <option value="01">Janeiro</option>
                            <option value="02">Fevereiro</option>
                            <option value="03">Março</option>
                            <option value="04">Abril</option>
                            <option value="05">Maio</option>
                            <option value="06">Junho</option>
                            <option value="07">Julho</option>
                            <option value="08">Agosto</option>
                            <option value="09">Setembro</option>
                            <option value="10">Outubro</option>
                            <option value="11">Novembro</option>
                            <option value="12">Dezembro</option>
                        </select>
                        &nbsp
                        <select id="ano_fim_despesa" name="ano_fim_despesa" class="texto">
                            <option selected="" value="" disabled="">Ano</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                        </select>
                    </div>

                    <div class="espaco">
                        <label for="categoria_despesa" class="espaco">Categoria:</label>
                        <br>
                        <select name="categoria_despesa" class="texto txt_small" id="categoria_despesa" required oninvalid="setCustomValidity('Selecione uma categoria.')" onchange="try {
                                    setCustomValidity('');
                                } catch (e) {
                                }">
                            <option selected disabled value="">Selecione:</option>
                            <?php
                            $sql = mysql_query("SELECT * FROM categorias_despesas");

                            while ($retorno = mysql_fetch_array($sql)) {
                                $categoria = $retorno['categoria'];
                                ?>
                                <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                <?php
                            }

                            $sql_categoria_usuario = mysql_query("SELECT * FROM categorias_despesas_usuario WHERE id_usuario = '$id_usuario'");

                            $tem_categoria = mysql_num_rows($sql_categoria_usuario);
                            if ($tem_categoria >= 1) {
                                echo '<option disabled>Personalizadas:</option>';
                            }


                            while ($retorno = mysql_fetch_array($sql_categoria_usuario)) {
                                $categoria = $retorno['categoria'];
                                ?>
                                <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <a id="add_despesa">Adicionar</a>
                    </div>

                    <div class="espaco">
                        <label for="txt_descricao">Descrição:</label>
                        <br>
                        <input type="text" name="txt_descricao" id="txt_descricao" placeholder="Breve descrição (opcional)" class="texto txt_large" maxlength="20">
                    </div>

                    <div id="dt_vencimento" class="espaco">
                        <label for="vencimento">Data do vencimento:</label>
                        <br>
                        <input type="text" class="data texto txt_small" name="vencimento" id="vencimento" placeholder="dd/mm/aaaa">
                    </div>

                    <div id="dia_fixo_despesa" class="espaco">
                        <label for="dia_vencimento">Dia do vencimento:</label>
                        <br>
                        <input type="number" id="dia_vencimento" name="dia_vencimento" class="texto txt_small" min="1" max="31" placeholder="Dia">
                    </div>

                    <div class="espaco">
                        <label for="valor_despesa">Valor:</label>
                        <br>
                        <input type="text" name="valor_despesa" id="valor_despesa" placeholder="Valor" class="texto txt_small" required>
                        
                        <span id="checkBox_pago">
                            <label for="pago">Pago?</label>
                            <input type="checkbox" name="pago" id="pago">
                        </span>
                    </div>

                    <div class="espaco"> <!--Gambiarra pra espaço entre os botões-->
                    </div>

                    <input type="submit" value="Salvar" name="btn_salvar_despesa" id="btn_salvar_despesa" class="">
                    <input type="reset" value="Limpar" name="btn_cancelar" id="btn_cancelar" class="">
                </form> 
            </div>
        </div>


        <!-----------------------------------BLOCO RESPONSÁVEL PELO BOTÃO SALVAR NOVA DESPESA ----------------------->
        <?php
        if (isset($_POST['btn_salvar_despesa'])) {
           
            $id_usuario = $usuario['id'];
            $tipo = $_POST['tipo'];

            $mes_fim = $_POST['mes_fim_despesa'];
            $ano_fim = $_POST['ano_fim_despesa'];
            $data_fim = "$ano_fim-$mes_fim";

            $categoria = $_POST['categoria_despesa'];
            $descricao = $_POST['txt_descricao'];
            $data_vencimento = $_POST['vencimento'];
            $dia_vencimento = $_POST['dia_vencimento'];
            $valor = $_POST['valor_despesa'];
            $pago = $_POST['pago'];

            $data_temp = "$ano_fim-$mes_fim-$dia_vencimento";
            
            $trata_erro = false;


            if ($tipo == 'unica') {

                if ($trata_erro == false) {
                    if ($categoria == null) {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            $('#informe_categoria_despesa').slideDown('slow');
                            $("#categoria_despesa").css({"border": "solid 1px red"});

                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }



                if ($trata_erro == false) {
                    if ($data_vencimento == null) {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            $('#erro_dt_vencimento').slideDown('slow');
                            $("#vencimento").css({"border": "solid 1px red"});

                            $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";

                        </script>
                        <?php
                        $trata_erro = true;
                    }

                    /* Se a data de vencimento não tiver vazia, verifica se é uma data válida */
                    if ($trata_erro == false) {
                        $data_vencimento = explode("/", "$data_vencimento");
                        $dia = $data_vencimento[0];
                        $mes = $data_vencimento[1];
                        $ano = $data_vencimento[2];

                        //Padrão americano mes/dia/ano > 12/30/1995
                        if (checkdate($mes, $dia, $ano)) {
                            $data_vencimento = $dia;
                            $data_vencimento .= "/" . $mes;
                            $data_vencimento .= "/" . $ano;
                        }

                        //Senão for válida...
                        else {
                            ?>
                            <script>
                                $("#popup_nova_despesa").dialog({
                                    width: 450,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                    }
                                });
                                $('#data_vencimento_invalida').slideDown('slow');
                                $("#vencimento").css({"border": "solid 1px red"});

                                $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                                document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                                document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";
                            </script>
                            <?php
                            $trata_erro = true;
                        }
                    }
                }



                if ($trata_erro == false) {
                    if ($valor == null || $valor == 'R$ 0,00') {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            $('#erro_informe_valor_despesa').slideDown('slow');
                            $("#valor_despesa").css({"border": "solid 1px red"});

                            $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.vencimento.value = "<?php echo $data_vencimento ?>";

                        </script>
                        <?php
                        $trata_erro = true;
                    }


                    if ($trata_erro == false) {
                        
                        /* Se tudo tiver certo insere
                          Convertendo a variável valor para 100.00,
                         * removendo R$
                         */

                        $valor = str_replace("R", "", $valor);
                        $valor = str_replace("$", "", $valor);
                        $valor = str_replace(".", "", $valor);
                        $valor = str_replace(",", ".", $valor);

                        /*                         * ********Todas as Gambiarras da data********** */
                        $data_vencimento = explode("/", "$data_vencimento");
                        $dia = $data_vencimento[0];
                        $mes = $data_vencimento[1];
                        $ano = $data_vencimento[2];

                        $data_vencimento = $ano;
                        $data_vencimento .= "-$mes";
                        $data_vencimento .= -$dia;

                        $chave = uniqid(rand(), true);
                        $chave.= time();

                        insere_movimentacao_unica("Despesa", $chave, $id_usuario, $categoria, $descricao, $data_vencimento, $valor, $pago);
                        echo '<meta http-equiv="refresh" content="0">';
                    }
                }
            }// fim receita unica



            if ($tipo == "fixa") {

                $trata_erro = false;

                if ($mes_fim == null || $ano_fim == null) {
                    ?>
                    <script>
                        $("#popup_nova_despesa").dialog({
                            width: 450,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });
                        document.getElementById("despesa_fixa").checked = true;
                        $("#dt_vencimento").hide("normal");
                        $("#despesa_ate").slideDown("normal");
                        $("#dia_fixo_despesa").slideDown("normal");
                        $("#checkBox_pago").hide("normal");

                        $('#erro_informe_datafim_despesa').slideDown('slow');
                        $("#mes_fim_despesa").css({"border": "solid 1px red"});
                        $("#ano_fim_despesa").css({"border": "solid 1px red"});

                        $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                        document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                        document.nova_despesa.dia_vencimento.value = "<?php echo $dia_vencimento ?>";
                        document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";

                    </script>
                    <?php
                    $trata_erro = true;
                }


                if ($trata_erro == false) {
                    if ($data_fim < date('Y-m')) {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            document.getElementById("despesa_fixa").checked = true;
                            $("#dt_vencimento").hide("normal");
                            $("#despesa_ate").slideDown("normal");
                            $("#dia_fixo_despesa").slideDown("normal");
                            $("#checkBox_pago").hide("normal");

                            $('#erro_data_fim_menor_que_data_atual').slideDown('slow');
                            $("#mes_fim_despesa").css({"border": "solid 1px red"});
                            $("#ano_fim_despesa").css({"border": "solid 1px red"});
                            

                            $("#mes_fim_despesa option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_despesa option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.dia_vencimento.value = "<?php echo $dia_vencimento ?>";
                            document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }
                
                
                
                if ($trata_erro == false) {
                    if ($data_temp < date('Y-m-d')) {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            document.getElementById("despesa_fixa").checked = true;
                            $("#dt_vencimento").hide("normal");
                            $("#despesa_ate").slideDown("normal");
                            $("#dia_fixo_despesa").slideDown("normal");
                            $("#checkBox_pago").hide("normal");

                            $('#erro_data_fim_menor_que_data_atual2').slideDown('slow');
                            $("#dia_vencimento").css({"border": "solid 1px red"});

                            $("#mes_fim_despesa option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_despesa option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.dia_vencimento.value = "<?php echo $dia_vencimento ?>";
                            document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }



                if ($trata_erro == false) {
                    if ($categoria == null) {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });

                            document.getElementById("despesa_fixa").checked = true;
                            $("#dt_vencimento").hide("normal");
                            $("#despesa_ate").slideDown("normal");
                            $("#dia_fixo_despesa").slideDown("normal");
                            $("#checkBox_pago").hide("normal");

                            $('#informe_categoria_despesa').slideDown('slow');
                            $("#categoria_despesa").css({"border": "solid 1px red"});

                            $("#mes_fim_despesa option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_despesa option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.dia_vencimento.value = "<?php echo $dia_vencimento ?>";
                            document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }


                if ($trata_erro == false) {
                    if ($dia_vencimento == null) {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });

                            document.getElementById("despesa_fixa").checked = true;
                            $("#dt_vencimento").hide("normal");
                            $("#despesa_ate").slideDown("normal");
                            $("#dia_fixo_despesa").slideDown("normal");
                            $("#checkBox_pago").hide("normal");

                            $('#erro_informe_dia_vencimento').slideDown('slow');
                            $("#dia_vencimento").css({"border": "solid 1px red"});

                            $("#mes_fim_despesa option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_despesa option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.valor_despesa.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }

                if ($trata_erro == false) {
                    if ($valor == null || $valor == 'R$ 0,00') {
                        ?>
                        <script>
                            $("#popup_nova_despesa").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });

                            document.getElementById("despesa_fixa").checked = true;
                            $("#dt_vencimento").hide("normal");
                            $("#despesa_ate").slideDown("normal");
                            $("#dia_fixo_despesa").slideDown("normal");
                            $("#checkBox_pago").hide("normal");

                            $('#erro_informe_valor_despesa').slideDown('slow');
                            $("#valor_despesa").css({"border": "solid 1px red"});

                            $("#mes_fim_despesa option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_despesa option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_despesa option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_despesa.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_despesa.dia_vencimento.value = "<?php echo $dia_vencimento ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                    if ($trata_erro == false) {
                        /* Se tudo tiver certo insere
                          Convertendo a variável valor para 100.00,
                         * removendo "R" e "$" 
                         */
                        $valor = str_replace("R", "", $valor);
                        $valor = str_replace("$", "", $valor);
                        $valor = str_replace(".", "", $valor);
                        $valor = str_replace(",", ".", $valor);

                        $chave = uniqid(rand(), true);
                        $chave.= time();
                        
                        
                        insere_movimentacao_fixa("Despesa", $chave, $id_usuario, $categoria, $descricao, $data_fim, $valor, $pago, $dia_vencimento);
                        echo '<meta http-equiv="refresh" content="0">';
                        
                    }
                }
            }
        }
        ?>



        <!------------------------------------------POPUP NOVA RECEITA----------------------------------->
        <div id="popup_nova_receita" title="Nova Receita">
            <div id="bloco_receita">
                <!--Erros-->
                <div id="erro_dt_recebimento" class="notificacao no erro_lancamento">Informe a data do recebimento.</div>
                <div id="erro_informe_valor_receita" class="notificacao no erro_lancamento">Informe o valor da receita.</div>
                <div id="erro_informe_datafim_receita" class="notificacao no erro_lancamento">Informe a data final.</div>
                <div id="erro_informe_dia_receb" class="notificacao no erro_lancamento">Informe o dia do recebimento.</div>
                <div id="data_recebimento_invalida" class="notificacao no erro_lancamento">Data inválida.</div>
                <div id="informe_categoria_receita" class="notificacao no erro_lancamento">Informe uma categoria.</div>
                <div id="erro_data_fim_menor_que_data_atual_receita" class="notificacao no erro_lancamento">A data final deve ser maior que a data atual.</div>
                <div id="erro_data_fim_menor_que_data_atual_receita2" class="notificacao no erro_lancamento">Se o mês final for o mês atual, o dia do vencimento deve ser maior do que o dia atual.</div>
                <!--/Erros-->

                <form method="post" enctype="multipart/form-data" name="nova_receita">
                    <ul>
                        <li class="tipo"><input type="radio" checked name="tipo" value="unica" id="receita_unica"> <label for="receita_unica">Única</label></li>
                        <li class="tipo"><input type="radio" name="tipo" value="fixa" id="receita_fixa"> <label for="receita_fixa">Fixa</label></li>
                    </ul>




                    <!--                    <div id="receita_ate">
                                            <label for="receita_fixa_ate">Até quando?</label>
                                            <br>
                                            <input type="month" name="receita_fixa_ate" id="receita_fixa_ate" class="texto" min="<?php //echo date('Y-m')  ?>">
                                        </div>-->

                    <div id="receita_ate">
                        <label>Até quando?</label>
                        <br>
                        <select id="mes_fim_receita" name="mes_fim_receita" class="texto">
                            <option selected="" value="" disabled="">Mês</option>
                            <option value="01">Janeiro</option>
                            <option value="02">Fevereiro</option>
                            <option value="03">Março</option>
                            <option value="04">Abril</option>
                            <option value="05">Maio</option>
                            <option value="06">Junho</option>
                            <option value="07">Julho</option>
                            <option value="08">Agosto</option>
                            <option value="09">Setembro</option>
                            <option value="10">Outubro</option>
                            <option value="11">Novembro</option>
                            <option value="12">Dezembro</option>
                        </select>
                        &nbsp
                        <select id="ano_fim_receita" name="ano_fim_receita" class="texto">
                            <option selected="" value="" disabled="">Ano</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                        </select>
                    </div>


                    <div class="espaco">
                        <label for="categoria_receita" class="espaco">Categoria:</label>
                        <br>
                        <select name="categoria_receita" class="texto txt_small" id="categoria_receita" required oninvalid="setCustomValidity('Selecione uma categoria.')" onchange="try {
                                    setCustomValidity('');
                                } catch (e) {
                                }">
                            <option selected disabled value="">Selecione:</option>
                            
                            <?php
                            $sql = mysql_query("SELECT * FROM categorias_receitas");

                            while ($retorno = mysql_fetch_array($sql)) {
                                $categoria = $retorno['categoria'];
                                ?>
                                <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                <?php
                            }

                            $sql_categoria_usuario = mysql_query("SELECT * FROM categorias_receitas_usuario WHERE id_usuario = '$id_usuario'");

                            $tem_categoria = mysql_num_rows($sql_categoria_usuario);
                            if ($tem_categoria >= 1) {
                                echo '<option disabled>Personalizadas:</option>';
                            }


                            while ($retorno = mysql_fetch_array($sql_categoria_usuario)) {
                                $categoria = $retorno['categoria'];
                                ?>
                                <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <a id="add_receita">Adicionar</a>
                    </div>

                    <div class="espaco">
                        <label for="txt_descricao">Descrição:</label>
                        <br>
                        <input type="text" name="txt_descricao" id="txt_descricao" placeholder="Breve descrição (opcional)" class="texto txt_large" maxlength="20">
                    </div>

                    <div id="dt_recebimento" class="espaco">
                        <label for="recebimento">Data de recebimento:</label>
                        <br>
                        <input type="text" class="texto txt_small data" name="recebimento" id="recebimento" min="" placeholder="dd/mm/aaaa">
                    </div>

                    <div id="dia_fixo_receita" class="espaco">
                        <label for="dia_recebimento">Dia do recebimento:</label>
                        <br>
                        <input type="number" id="dia_recebimento" name="dia_recebimento" class="texto txt_small" min="1" max="31" placeholder="Dia">
                    </div>

                    <div class="espaco">
                        <label for="valor_receita">Valor:</label>
                        <br>
                        <input type="text" name="valor_receita" id="valor_receita" placeholder="Valor" class="texto txt_small" required>
                        <span id="checkBox_recebido">
                            <label for="recebido">Já recebido?</label>
                            <input type="checkbox" name="recebido" id="recebido">
                        </span>
                    </div>

                    <div class="espaco"> <!--Gambiarra pra espaço entre os botões-->
                    </div>

                    <input type="submit" value="Salvar" name="btn_salvar_receita" id="btn_salvar_receita" class="">
                    <input type="reset" value="Limpar" name="btn_cancelar" id="btn_cancelar" class="">
                </form> 
            </div>
        </div>

        <!-- ---------------------------------------BLOCO RESPONSÁVEL POR NOVA RECEITA ----------------------------- -->
        <?php
        if (isset($_POST['btn_salvar_receita'])) {
            $id_usuario = $usuario['id'];
            $tipo = $_POST['tipo'];

            $mes_fim = $_POST['mes_fim_receita'];
            $ano_fim = $_POST['ano_fim_receita'];
            $data_fim = "$ano_fim-$mes_fim";

            $categoria = $_POST['categoria_receita'];
            $descricao = $_POST['txt_descricao'];
            $data_recebimento = $_POST['recebimento'];
            $dia_recebimento = $_POST['dia_recebimento'];
            $valor = $_POST['valor_receita'];
            $recebido = $_POST['recebido'];
            
            $data_temp = "$ano_fim-$mes_fim-$dia_recebimento";

            /*
              echo "Id: $id <br>";
              echo "Tipo: $tipo <br>";
              echo "Categoria: $categoria <br>";
              echo "Descricao: $descricao <br>";
              echo "Data recebimento: $data_recebimento <br>";
              echo "Dia recebimento: $dia_recebimento <br>";
             */
            if ($tipo == 'unica') {

                if ($categoria == null) {
                    ?>
                    <script>
                        $("#popup_nova_receita").dialog({
                            width: 450,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });
                        $('#informe_categoria_receita').slideDown('slow');
                        $("#categoria_receita").css({"border": "solid 1px red"});
                    </script>
                    <?php
                    $trata_erro = true;
                }

                if ($trata_erro == false) {
                    if ($data_recebimento == null) {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            $('#erro_dt_recebimento').slideDown('slow');
                            $("#recebimento").css({"border": "solid 1px red"});

                            $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.valor_receita.value = "<?php echo $valor ?>";

                        </script>
                        <?php
                        $trata_erro = true;
                    }

                    /* Se a data de vencimento não tiver vazia, verifica se é uma data válida */
                    if ($trata_erro == false) {
                        $data_recebimento = explode("/", "$data_recebimento");
                        $dia = $data_recebimento[0];
                        $mes = $data_recebimento[1];
                        $ano = $data_recebimento[2];

                        //Padrão americano mes/dia/ano > 12/30/1995
                        if (checkdate($mes, $dia, $ano)) {
                            $data_recebimento = $dia;
                            $data_recebimento .= "/" . $mes;
                            $data_recebimento .= "/" . $ano;
                        }

                        //Senão for válida...
                        else {
                            ?>
                            <script>
                                $("#popup_nova_receita").dialog({
                                    width: 450,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                    }
                                });
                                $('#data_recebimento_invalida').slideDown('slow');
                                $("#vencimento").css({"border": "solid 1px red"});

                                $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                                document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                                document.nova_receita.valor_receita.value = "<?php echo $valor ?>";
                            </script>
                            <?php
                            $trata_erro = true;
                        }
                    }
                }



                if ($trata_erro == false) {
                    if ($valor == null || $valor == 'R$ 0,00') {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            $('#erro_informe_valor_receita').slideDown('slow');
                            $("#valor_receita").css({"border": "solid 1px red"});

                            $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.recebimento.value = "<?php echo $data_recebimento ?>";

                        </script>
                        <?php
                        $trata_erro = true;
                    }


                    if ($trata_erro == false) {
                        /* Se tudo tiver certo insere
                          Convertendo a variável valor para 100.00,
                         * removendo R$
                         */

                        $valor = str_replace("R", "", $valor);
                        $valor = str_replace("$", "", $valor);
                        $valor = str_replace(".", "", $valor);
                        $valor = str_replace(",", ".", $valor);

                        /*                         * ********Todas as Gambiarras da data********** */
                        $data_recebimento = explode("/", "$data_recebimento");
                        $dia = $data_recebimento[0];
                        $mes = $data_recebimento[1];
                        $ano = $data_recebimento[2];

                        $data_recebimento = $ano;
                        $data_recebimento .= "-$mes";
                        $data_recebimento .= -$dia;

                        $chave = uniqid(rand(), true);
                        $chave.= time();

                        $inseriuReceitaUnica = insere_movimentacao_unica("Receita", $chave, $id_usuario, $categoria, $descricao, $data_recebimento, $valor, $recebido);
                        echo '<meta http-equiv="refresh" content="0">';
                        
                    }
                }
            }// fim receita unica

            if ($tipo == "fixa") {
                $trata_erro = false;

                if ($mes_fim == null || $ano_fim == null) {
                    ?>
                    <script>
                        $("#popup_nova_receita").dialog({
                            width: 450,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });
                        document.getElementById("receita_fixa").checked = true;
                        $("#dt_recebimento").hide("normal");
                        $("#receita_ate").slideDown("normal");
                        $("#dia_fixo_receita").slideDown("normal");
                        $("#checkBox_recebido").hide("fast");

                        $('#erro_informe_datafim_receita').slideDown('slow');
                        $("#mes_fim_receita").css({"border": "solid 1px red"});
                        $("#ano_fim_receita").css({"border": "solid 1px red"});

                        $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                        document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                        document.nova_receita.dia_recebimento.value = "<?php echo $dia_recebimento ?>";
                        document.nova_receita.valor_receita.value = "<?php echo $valor ?>";

                    </script>
                    <?php
                    $trata_erro = true;
                }


                if ($trata_erro == false) {
                    if ($data_fim < date('Y-m')) {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            document.getElementById("receita_fixa").checked = true;
                            $("#dt_recebimento").hide("normal");
                            $("#receita_ate").slideDown("normal");
                            $("#dia_fixo_receita").slideDown("normal");
                            $("#checkBox_recebido").hide("fast");

                            $('#erro_data_fim_menor_que_data_atual_receita').slideDown('slow');
                            $("#mes_fim_receita").css({"border": "solid 1px red"});
                            $("#ano_fim_receita").css({"border": "solid 1px red"});

                            $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.dia_recebimento.value = "<?php echo $dia_recebimento ?>";
                            document.nova_receita.valor_receita.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }
                
                
                
                if ($trata_erro == false) {
                    if ($data_temp < date('Y-m-d')) {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });
                            document.getElementById("receita_fixa").checked = true;
                            $("#dt_recebimento").hide("normal");
                            $("#receita_ate").slideDown("normal");
                            $("#dia_fixo_receita").slideDown("normal");
                            $("#checkBox_recebido").hide("fast");

                            $('#erro_data_fim_menor_que_data_atual_receita2').slideDown('slow');
                            $("#dia_recebimento").css({"border": "solid 1px red"});
                            
                            $("#mes_fim_receita option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_receita option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.dia_recebimento.value = "<?php echo $dia_recebimento ?>";
                            document.nova_receita.valor_receita.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }



                if ($trata_erro == false) {
                    if ($categoria == null) {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });

                            document.getElementById("receita_fixa").checked = true;
                            $("#dt_recebimento").hide("normal");
                            $("#receita_ate").slideDown("normal");
                            $("#dia_fixo_receita").slideDown("normal");
                            $("#checkBox_recebido").hide("fast");

                            $('#informe_categoria_receita').slideDown('slow');
                            $("#categoria_receita").css({"border": "solid 1px red"});

                            $("#mes_fim_receita option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_receita option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.dia_recebimento.value = "<?php echo $dia_recebimento ?>";
                            document.nova_receita.valor_receita.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }


                if ($trata_erro == false) {
                    if ($dia_recebimento == null) {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });

                            document.getElementById("receita_fixa").checked = true;
                            $("#dt_recebimento").hide("normal");
                            $("#receita_ate").slideDown("normal");
                            $("#dia_fixo_receita").slideDown("normal");
                            $("#checkBox_recebido").hide("fast");

                            $('#erro_informe_dia_receb').slideDown('slow');
                            $("#dia_recebimento").css({"border": "solid 1px red"});

                            $("#mes_fim_receita option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_receita option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.valor_receita.value = "<?php echo $valor ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                }

                if ($trata_erro == false) {
                    if ($valor == null || $valor == 'R$ 0,00') {
                        ?>
                        <script>
                            $("#popup_nova_receita").dialog({
                                width: 450,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                                }
                            });

                            document.getElementById("receita_fixa").checked = true;
                            $("#dt_recebimento").hide("normal");
                            $("#receita_ate").slideDown("normal");
                            $("#dia_fixo_receita").slideDown("normal");
                            $("#checkBox_recebido").hide("fast");

                            $('#erro_informe_valor_receita').slideDown('slow');
                            $("#valor_receita").css({"border": "solid 1px red"});

                            $("#mes_fim_receita option[value='<?php echo $mes_fim ?>']").attr("selected", true);
                            $("#ano_fim_receita option[value='<?php echo $ano_fim ?>']").attr("selected", true);
                            $("#categoria_receita option[value='<?php echo $categoria ?>']").attr("selected", true);
                            document.nova_receita.txt_descricao.value = "<?php echo $descricao ?>";
                            document.nova_receita.dia_recebimento.value = "<?php echo $dia_recebimento ?>";
                        </script>
                        <?php
                        $trata_erro = true;
                    }
                    if ($trata_erro == false) {
                        /* Se tudo tiver certo insere
                          Convertendo a variável valor para 100.00,
                         * removendo "R" e "$" 
                         */
                        $valor = str_replace("R", "", $valor);
                        $valor = str_replace("$", "", $valor);
                        $valor = str_replace(".", "", $valor);
                        $valor = str_replace(",", ".", $valor);

                        $chave = uniqid(rand(), true);
                        $chave.= time();

                        insere_movimentacao_fixa("Receita", $chave, $id_usuario, $categoria, $descricao, $data_fim, $valor, $recebido, $dia_recebimento);
                        echo '<meta http-equiv="refresh" content="0">';
                    }
                }
            }
        }
        ?>


            <!--------------------------------Popup nova categoria despesa---------------------------------------------->
        <div id="popup_nova_categoria_despesa" title="Nova Categoria - Despesa">
            <form method="post" name="form_nova_despesa">
                <label for="txt_categoria_despesa">Categoria:</label>
                <input type="text" name="txt_categoria_despesa" id="txt_categoria_despesa" placeholder="Digite a categoria" class="texto txt_large" maxlength="15">

                <div id="txt_categoria_despesa_vazio" class="notificacao_categoria" >Digite uma categoria.</div>
                <div id="erro_despesa_ja_cadastrada" class="notificacao_categoria" >Categoria já cadastrada.</div>

                <input type="submit" name="btn_salvar_categoria_despesa" id="btn_salvar_categoria_despesa" value="Salvar">
            </form>
        </div>


        <?php
        //***************Ação do botão salvar categoria de despesa*******************
        if (isset($_POST['btn_salvar_categoria_despesa'])) {
            $categoria = $_POST['txt_categoria_despesa'];

            //Verifica se o usuário não digitou nada
            if ($categoria == null) {
                ?>
                <script>

                    $("#popup_nova_despesa").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                        }
                    });

                    $('#txt_categoria_despesa_vazio').show();
                    $("#popup_nova_categoria_despesa").dialog({
                        height: 160,
                        width: 290,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                        }
                    });
                </script>
                <?php
            }
            //Senão tiver em branco, verifica se esse nome de categoria já esta cadastrado nas 2 tabelas categorias_receitas e categorias_receitas_usuario
            else {

                $ja_cadastrada = verifica_categoria_despesa($id_usuario, $categoria);

                //Se já tiver cadastrada, da erro
                if ($ja_cadastrada) {
                    ?>
                    <script>

                        $("#popup_nova_despesa").dialog({
                            width: 450,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });


                        $('#erro_despesa_ja_cadastrada').show();

                        $("#popup_nova_categoria_despesa").dialog({
                            height: 140,    
                            width: 315,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });
                    </script>
                    <?php
                }
                //Senão volta pra tela já atualizada chamando o pop de despesa com a categoria atualizada
                else {
                    header('Location: base.php?p=movimentacoes&acao=new_Despesa&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                }
            }
        }
        ?>




        <!--------------------------------Popup nova categoria receita---------------------------------------------->
        <div id="popup_nova_categoria_receita" title="Nova Categoria - Receita">
            <form method="post" name="form_nova_receita">
                <label for="txt_categoria_receita">Categoria:</label>
                <input type="text" name="txt_categoria_receita" id="txt_categoria_receita" placeholder="Digite a categoria" class="texto txt_large" maxlength="15">

                <div id="txt_categoria_receita_vazio" class="notificacao_categoria" >Digite uma categoria.</div>
                <div id="erro_receita_ja_cadastrada" class="notificacao_categoria" >Categoria já cadastrada.</div>

                <input type="submit" name="btn_salvar_categoria_receita" id="btn_salvar_categoria_receita" value="Salvar">
            </form>
        </div>

        <?php
        //***************Ação do botão salvar categoria de receita*******************
        if (isset($_POST['btn_salvar_categoria_receita'])) {
            $categoria = $_POST['txt_categoria_receita'];

            //Verifica se o usuário não digitou nada
            if ($categoria == null) {
                ?>
                <script>

                    $("#popup_nova_receita").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                        }
                    });

                    $('#txt_categoria_receita_vazio').show();
                    $("#popup_nova_categoria_receita").dialog({
                        height: 165,
                        width: 290,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                        }
                    });
                </script>
                <?php
            }
            //Senão tiver em branco, verifica se esse nome de categoria já esta cadastrado nas 2 tabelas categorias_receitas e categorias_receitas_usuario
            else {

                $ja_cadastrada = verifica_categoria_receita($id_usuario, $categoria);

                //Se já tiver cadastrada, da erro
                if ($ja_cadastrada) {
                    ?>
                    <script>

                        $("#popup_nova_receita").dialog({
                            width: 450,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });


                        $('#erro_receita_ja_cadastrada').show();

                        $("#popup_nova_categoria_receita").dialog({
                            height: 145,
                            width: 315,
                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                            resizable: false,
                            close: function() {
                                //location.href = "http://localhost/GestaTudo/base.php?p=receita";
                            }
                        });
                    </script>
                    <?php
                }
                //Senão volta pra tela já atualizada
                else {
                    header('Location: base.php?p=movimentacoes&acao=new_Receita&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                }
            }
        }
        ?>

























        <?php
        /*         * ***************SE O BOTÃO btn_buscar_relatorio NÃO FOR CLICADO********** */
        if (!isset($_POST['btn_buscar_relatorio'])) {
            $data_de = $_GET['dt_de'];
            $data_ate = $_GET['dt_ate'];
            $trata_erro_datas = false;

            //Se as datas tiverem brancas
            if (($data_de == null) || ($data_ate == null)) {
                $ultimo_dia = retorna_dia_final_mes(date('m'));
                $data_de = date('Y-m-01');
                $data_ate = date('Y-m-') . $ultimo_dia;
                $trata_erro_datas = true;
            }

            //Senão tiverem brancas, verifica se são válidas
            else {

                //Data DE:
                $data_de = explode("-", "$data_de");
                $dia_de = $data_de[2];
                $mes_de = $data_de[1];
                $ano_de = $data_de[0];

                //Data ATE:
                $data_ate = explode("-", "$data_ate");
                $dia_ate = $data_ate[2];
                $mes_ate = $data_ate[1];
                $ano_ate = $data_ate[0];

                if (!(checkdate($mes_de, $dia_de, $ano_de))) {
                    $data_de = date('Y-m-01');
                    $ultimo_dia = retorna_dia_final_mes(date('m'));
                    $data_ate = date('Y-m-') . $ultimo_dia;
                    $trata_erro_datas = true;

                    header('Location: base.php?p=movimentacoes&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                }

                if (!(checkdate($mes_ate, $dia_ate, $ano_ate))) {
                    $data_de = date('Y-m-01');
                    $ultimo_dia = retorna_dia_final_mes(date('m'));
                    $data_ate = date('Y-m-') . $ultimo_dia;
                    $trata_erro_datas = true;

                    header('Location: base.php?p=movimentacoes&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                }
            }

            if ($trata_erro_datas == false) {
                $data_de = $ano_de;
                $data_de .= "-$mes_de";
                $data_de .= "-$dia_de";

                $data_ate = $ano_ate;
                $data_ate .= "-$mes_ate";
                $data_ate .= "-$dia_ate";

                if ($data_de > $data_ate) {
                    $ultimo_dia = retorna_dia_final_mes(date('m'));
                    $data_de = date('Y-m-01');
                    $data_ate = date('Y-m-') . $ultimo_dia;
                    header('Location: base.php?p=movimentacoes&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                }
            }
        }
        ?>                     




        <div id="topo_movimentacoes" class="cor_de_fundo">

            <div id="botoes_lancamentos">
                <button id="chama_despesa">Despesa</button>
                <button id="chama_receita">Receita</button>
            </div>

            <form method="post" name="form_datas">
                <div id="datas">
                    <input type="text" class="data texto" name="receitas_de" id="receitas_de" placeholder="De: " value="<?php 
                                                                                                                            $txtDe = $_GET['dt_de'];
                                                                                                                            $txtDe = explode("-", "$txtDe");
                                                                                                                            $diaDe = $txtDe[2];
                                                                                                                            $mesDe = $txtDe[1];
                                                                                                                            $anoDe = $txtDe[0];
                                                                                                                            echo "$diaDe/$mesDe/$anoDe"; 
                                                                                                                        ?>">
                    -
                    <input type="text" class="data texto" name="receitas_ate" id="receitas_ate" placeholder="Até: " value="<?php
                                                                                                                                $txtAte = $_GET['dt_ate'];
                                                                                                                                $txtAte = explode("-", "$txtAte");
                                                                                                                                $diaAte = $txtAte[2];
                                                                                                                                $mesAte = $txtAte[1];
                                                                                                                                $anoAte = $txtAte[0];
                                                                                                                                echo "$diaAte/$mesAte/$anoAte";
                                                                                                                            ?>"
                    >
                    <input type="submit" value="Buscar" name="btn_buscar_relatorio" id="btn_buscar_relatorio">
                </div>

                <div class="notificacao no" id="datas_null">Informe as duas datas.</div>
                <div class="notificacao no" id="data_invalida">Informe uma data válida.</div>
                <div class="notificacao no" id="periodo_invalido">Período de busca inválido. <br> A data inicial deve ser menor do que a data final.</div>
                <div class="notificacao ok" id="atualizado_sucesso">Movimentação atualizada com sucesso.</div>

            </form>
        </div>

        <section id="sessao_relatorio">
            <?php
            if (isset($_POST['btn_buscar_relatorio'])) {
                $data_de = $_POST['receitas_de'];
                $data_ate = $_POST['receitas_ate'];

                $erro = false;

//                echo $data_de . '<br>';
//                echo $data_ate . '<br>';
                //Se as datas tiverem brancas
                if (($data_de == "") || ($data_ate == "")) {
                    $erro = true;
                    ?>
                    <script>
                        $('#datas_null').slideDown('fast');
                        $("#receitas_de").css({"border": "solid 1px red"});
                        $("#receitas_ate").css({"border": "solid 1px red"});
                        document.form_datas.receitas_de.value = "<?php echo $data_de ?>";
                        document.form_datas.receitas_ate.value = "<?php echo $data_ate ?>";
                    </script>
                <?php
                }

                //Senão tiverem brancas, verifica se são válidas
                else {

                    //Data DE:
                    $data_de = explode("/", "$data_de");
                    $dia_de = $data_de[0];
                    $mes_de = $data_de[1];
                    $ano_de = $data_de[2];

                    if (!(checkdate($mes_de, $dia_de, $ano_de))) {
                        $erro = true;
                        ?>
                                    <script>
                                        $('#data_invalida').slideDown('fast');
                                        $("#receitas_de").css({"border": "solid 1px red"});
                        <?php
                        $data_de = "$dia_de/";
                        $data_de .= "$mes_de/";
                        $data_de .= $ano_de;
                        ?>
                                        document.form_datas.receitas_de.value = "<?php echo $data_de ?>";
                                        document.form_datas.receitas_ate.value = "<?php echo $data_ate ?>";
                                    </script>
                        <?php
                    }

                    //Data ATE:
                    $data_ate = explode("/", "$data_ate");
                    $dia_ate = $data_ate[0];
                    $mes_ate = $data_ate[1];
                    $ano_ate = $data_ate[2];

                    if (!(checkdate($mes_ate, $dia_ate, $ano_ate))) {
                        $erro = true;
                        ?>
                                    <script>
                                        $('#data_invalida').slideDown('fast');
                                        $("#receitas_ate").css({"border": "solid 1px red"});
                        <?php
                        $data_de = "$dia_de/";
                        $data_de .= "$mes_de/";
                        $data_de .= $ano_de;

                        $data_ate = "$dia_ate/";
                        $data_ate .= "$mes_ate/";
                        $data_ate .= $ano_ate;
                        ?>
                                        document.form_datas.receitas_de.value = "<?php echo $data_de ?>";
                                        document.form_datas.receitas_ate.value = "<?php echo $data_ate ?>";
                                    </script>
                        <?php
                    }

                    if ($erro == false) {

                        $data_de = $ano_de;
                        $data_de .= "-$mes_de";
                        $data_de .= "-$dia_de";

                        $data_ate = $ano_ate;
                        $data_ate .= "-$mes_ate";
                        $data_ate .= "-$dia_ate";

                        if ($data_de > $data_ate) {
                            ?>
                            <script>
                                $('#periodo_invalido').slideDown('fast');
                                $("#receitas_de").css({"border": "solid 1px red"});
                                $("#receitas_ate").css({"border": "solid 1px red"});
                            <?php
                            $data_de = "$dia_de/";
                            $data_de .= "$mes_de/";
                            $data_de .= $ano_de;

                            $data_ate = "$dia_ate/";
                            $data_ate .= "$mes_ate/";
                            $data_ate .= $ano_ate;
                            ?>
                                document.form_datas.receitas_de.value = "<?php echo $data_de ?>";
                                document.form_datas.receitas_ate.value = "<?php echo $data_ate ?>";
                            </script>
                            <?php
                        } else {
                            header('Location: base.php?p=movimentacoes&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                        }
                    }
                }
            }


            /* * ********************Mostrando periodo*************************88 */
            echo "<div id='periodo' class='cor_de_fundo'>Periodo: " . date("d/m/Y", strtotime($data_de)) . " à " . date("d/m/Y", strtotime($data_ate)) . "</div>";
            ?>
                
            <!--FILTROOOOOOOOS-->
            <div id="filtros" class="cor_de_fundo">
                <label id="filtrar_por">Filtrar por:</label>
                <form method="post" name="filtros">
                    <label id="label_despesas">Despesas</label>

                    <div id="filtros_despesas">
                        <label id="label_nao_pagas">Não pagas</label>
                        <br>
                        <label id="label_pagas">Pagas</label>
                        <br>
                        <label id="label_todas_despesas">Todas</label>
                    </div>

                    <br>

                    <label id="label_receitas">Receitas</label>
                    <div id="filtros_receitas">
                        <label id="label_nao_recebidas">Não recebidas</label>
                        <br>
                        <label id="label_recebidas">Recebidas</label>
                        <br>
                        <label id="label_todas_receitas">Todas</label>
                    </div>

                    <br>

                    <label for="ft_tudo" id="label_todas">Tudo</label>
                </form>
            </div>            



            
            <!------------------------------------------EDITAR MOV ÚNICA------------------------------------------------>
            <div id="popup_editar_movimentacao_unica" title="Editar movimentação">
                
    
                <!--OBTER MOVIMENTACAO A SER EDITADA-->
                <?php
                    $id_mov = $_GET['idmov'];
                    $key_mov = $_GET['keymov'];
                    
                    $movimentacao = retorna_dados_movimentacao($id_usuario, $id_mov, $key_mov);
                    
                    if($movimentacao[mov_tipo] == "Despesa"){
                        $despesa_checked = "checked";
                        $receita_checked = null;
                        $tipo_movimentacao = "Despesa";
                    }
                    else{
                        $despesa_checked = null;
                        $receita_checked = "checked";
                    }
                ?>
                
                
                <form method="post" id="dados_mov_atual" name="dados_mov_atual">
                    <img src="img/editar2.png" alt="Editar movimentação" id="img_editar_mov">
                    <div id="tipo_atual">
                        <input <?php echo $despesa_checked ?> type="radio" name="edita_tipo_mov_unica" value="Despesa" id="despesa_unica_editar"> <label for="despesa_unica_editar">Despesa</label>
                        <input <?php echo $receita_checked ?> type="radio" name="edita_tipo_mov_unica" value="Receita" id="receita_unica_editar"> <label for="receita_unica_editar">Receita</label>
                    </div>
                    <br>
                    
                    <div id="dados_atuais">
                        <div>
                            <label for="categoria_atual">Categoria:</label>
                            <br>
                            <select name="categoria_atual" class="texto txt_small" id="categoria_atual">
                                    <option selected value="<?php echo $movimentacao[categoria] ?>"><?php echo $movimentacao[categoria] ?></option>
                                    <?php
                                    echo '<option disabled>Receitas:</option>';
                                    $sql = mysql_query("SELECT * FROM categorias_receitas");

                                    while ($retorno = mysql_fetch_array($sql)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }

                                    echo '<option disabled>Despesas:</option>';
                                    $sql = mysql_query("SELECT * FROM categorias_despesas");

                                    while ($retorno = mysql_fetch_array($sql)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }


                                    $sql_categoria_usuario = mysql_query("SELECT * FROM categorias_receitas_usuario WHERE id_usuario = '$id_usuario'");
                                    $sql_categoria_usuario2 = mysql_query("SELECT * FROM categorias_despesas_usuario WHERE id_usuario = '$id_usuario'");

                                    $tem_categoria_receitas = mysql_num_rows($sql_categoria_usuario);
                                    $tem_categoria_despesas = mysql_num_rows($sql_categoria_usuario2);

                                    if ( ($tem_categoria_receitas >= 1) || $tem_categoria_despesas >= 1  ) {
                                        echo '<option disabled>Personalizadas:</option>';
                                    }
                                    
                                    
                                    if ($tem_categoria_receitas >= 1){
                                        echo '<option disabled>Receitas:</option>';
                                    }

                                    while ($retorno = mysql_fetch_array($sql_categoria_usuario)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }
                                    
                                    if ($tem_categoria_despesas >= 1){
                                        echo '<option disabled>Despesas:</option>';
                                    }
                                    
                                    while ($retorno = mysql_fetch_array($sql_categoria_usuario2)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }  
                                    ?>
                            </select>
                        </div>
                    
                        <div>
                            <label for="descricao_atual">Descrição:</label>
                            <br>
                            <input type="text" id="descricao_atual" maxlength="20" name="descricao_atual" class="texto txt_large" placeholder="Descrição" value="<?php echo $movimentacao[descricao]?>">
                        </div>

                        <div>
                            <label for="data_atual">Data:</label>
                            <br>
                            <input type="text" name="data_atual" id="data_atual" class="data texto txt_small" placeholder="dd/mm/aaaa" value="<?php echo date("d/m/Y", strtotime($movimentacao[data]))?>">
                        </div>

                        <div>
                            <label for="valor_atual">Valor:</label>
                            <br>
                            <input type="text" id="valor_atual" name="valor_atual" class="texto txt_small" placeholder="Informe o valor" value="<?php echo "R$ ".number_format($movimentacao[valor], 2, ',', '.')?>">
                        </div>

                        <br>
                        <input type="submit" value="Salvar" name="btn_editar_mov_unica" id="btn_editar_mov_unica">
                        <a href="http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>"><input type="button" value="Cancelar" name="btn_cancelar_edicao_mov_unica" id="btn_cancelar_edicao_mov_unica"></a>

                        <div class="notificacao ok sucesso_edita" id="editada_sucesso_unica">Movimentação alterada <br> com sucesso.</div>
                        <div class="notificacao no erro_edita" id="data_vazia_edita_unica">Por favor, informe uma data.</div>
                        <div class="notificacao no erro_edita" id="edicao_data_invalida_edita_unica">Informe uma data válida.</div>
                        <div class="notificacao no erro_edita" id="edicao_valor_null_edita_unica">Por favor, informe o valor.</div>
                    </div>
                </form>
                
                
                <?php
                    if(isset($_POST['btn_editar_mov_unica'])){
                        $trata_erro_editar = false;
                        
                        $tipo = $_POST['edita_tipo_mov_unica'];
                        $categoria = $_POST['categoria_atual'];
                        $descricao = $_POST['descricao_atual'];
                        $data = $_POST['data_atual'];
                        $valor = $_POST['valor_atual'];
                        
                        
                        if($data == null){
                            ?>
                            <script>
                                $('#data_vazia_edita_unica').slideDown('normal');
                                $("#data_atual").css({"border": "solid 1px red"});
                            </script>
                            <?php
                            $trata_erro_editar = true;
                        }
                        
                        
                        if ($trata_erro_editar == FALSE){
                            $data = explode("/", "$data");
                            $dia = $data[0];
                            $mes = $data[1];
                            $ano = $data[2];
                        
                            //Padrão americano mes/dia/ano > 12/30/1995
                            //Senão for uma data válida
                            if ( !(checkdate($mes, $dia, $ano)) ) {
                                ?>
                                <script>
                                    $('#edicao_data_invalida_edita_unica').show('normal');
                                    $("#data_atual").css({"border": "solid 1px red"});
                                </script>
                                <?php
                                $trata_erro_editar = true;
                            }
                        }
                        
                        if ($trata_erro_editar == FALSE){
                            if( ($valor == null) || ($valor == "R$ 0,00") ){
                                ?>
                                <script>
                                    $('#edicao_valor_null_edita_unica').show('normal');
                                    $("#valor_atual").css({"border": "solid 1px red"});
                                </script>
                                <?php
                                $trata_erro_editar = true;
                            }
                        }
                        
                        
                        //SE TIVER TUDO CERTO, ALTERA...
                        if ($trata_erro_editar == FALSE){
                            $data = $ano;
                            $data .= "-$mes";
                            $data .= "-$dia";
                            
                            $valor = str_replace("R", "", $valor);
                            $valor = str_replace("$", "", $valor);
                            $valor = str_replace(".", "", $valor);
                            $valor = str_replace(",", ".", $valor);
                            
                            //echo "id_mov = $id_mov <br> key_mov = $key_mov <br> tipo = $tipo <br> categoria = $categoria <br> descrição = $descricao <br> data = $data <br> valor = $valor";
                            
                            $alterou = edita_movimentacao_unica($id_usuario, $id_mov, $key_mov, $tipo, $categoria, $descricao, $data, $valor);
                            
                            if($alterou){
                                ?>
                                <script>
                                    $('#editada_sucesso_unica').show('normal');
                                </script>
                                <?php
                                echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                            }
                            
                            
                        }
                        
                        
                    }
                ?>
                
                
            </div>
            
            
            
            
            
            <!------------------------------------------EDITAR MOV FIXA------------------------------------------------>
            <div id="popup_editar_movimentacao_fixa" title="Editar movimentação fixa">
                
    
                <!--OBTER MOVIMENTACAO A SER EDITADA-->
                <?php
                    $id_mov = $_GET['idmov'];
                    $key_mov = $_GET['keymov'];
                    $movimentacao = retorna_dados_movimentacao($id_usuario, $id_mov, $key_mov);
                    if($movimentacao[mov_tipo] == "Despesa"){
                        $despesa_checked = "checked";
                        $receita_checked = null;
                    }
                    else{
                        $despesa_checked = null;
                        $receita_checked = "checked";
                    }
                ?>
                
                
                <form method="post" id="dados_mov_atual_fixa" name="dados_mov_atual_fixa">
                    <img src="img/editar2.png" alt="Editar movimentação" id="img_editar_mov">
                    
                    <div id="dados_atuais_fixa">
                        <input <?php echo $despesa_checked ?> type="radio" name="edita_tipo_mov_fixa" value="Despesa" id="despesa_fixa_editar"> <label for="despesa_fixa_editar">Despesa</label>
                        <input <?php echo $receita_checked ?> type="radio" name="edita_tipo_mov_fixa" value="Receita" id="receita_fixa_editar"> <label for="receita_fixa_editar">Receita</label>
                        
                        <br>
                        <br>
                        
                        <div>
                            <label for="categoria_atual_edita_fixa">Categoria:</label>
                            <br>
                            <select name="categoria_atual_edita_fixa" class="texto txt_small" id="categoria_atual_edita_fixa">
                                    <option selected value="<?php echo $movimentacao[categoria] ?>"><?php echo $movimentacao[categoria] ?></option>
                                    <?php
                                    echo '<option disabled>Receitas:</option>';
                                    $sql = mysql_query("SELECT * FROM categorias_receitas");

                                    while ($retorno = mysql_fetch_array($sql)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }

                                    echo '<option disabled>Despesas:</option>';
                                    $sql = mysql_query("SELECT * FROM categorias_despesas");

                                    while ($retorno = mysql_fetch_array($sql)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }


                                    $sql_categoria_usuario = mysql_query("SELECT * FROM categorias_receitas_usuario WHERE id_usuario = '$id_usuario'");
                                    $sql_categoria_usuario2 = mysql_query("SELECT * FROM categorias_despesas_usuario WHERE id_usuario = '$id_usuario'");

                                    $tem_categoria_receitas = mysql_num_rows($sql_categoria_usuario);
                                    $tem_categoria_despesas = mysql_num_rows($sql_categoria_usuario2);

                                    if ( ($tem_categoria_receitas >= 1) || $tem_categoria_despesas >= 1  ) {
                                        echo '<option disabled>Personalizadas:</option>';
                                    }
                                    
                                    
                                    if ($tem_categoria_receitas >= 1){
                                        echo '<option disabled>Receitas:</option>';
                                    }

                                    while ($retorno = mysql_fetch_array($sql_categoria_usuario)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }
                                    
                                    if ($tem_categoria_despesas >= 1){
                                        echo '<option disabled>Despesas:</option>';
                                    }
                                    
                                    while ($retorno = mysql_fetch_array($sql_categoria_usuario2)) {
                                        $categoria = $retorno['categoria'];
                                        ?>
                                        <option value="<?php echo $categoria ?>"><?php echo $categoria ?></option>
                                        <?php
                                    }  
                                    ?>
                            </select>
                        </div>
                    
                        <div>
                            <label for="descricao_atual_fixa">Descrição:</label>
                            <br>
                            <input type="text" id="descricao_atual_fixa" name="descricao_atual_fixa" maxlength="20" class="texto txt_large" placeholder="Descrição" value="<?php echo $movimentacao[descricao]?>">
                        </div>

                        <div>
                            <label for="data_atual_fixa">Data:</label>
                            <br>
                            <input type="text" name="data_atual_fixa" id="data_atual_fixa" class="data texto txt_small" placeholder="dd/mm/aaaa" value="<?php echo date("d/m/Y", strtotime($movimentacao[data]))?>">
                        </div>

                        <div>
                            <label for="valor_atual_fixa">Valor:</label>
                            <br>
                            <input type="text" id="valor_atual_fixa" name="valor_atual_fixa" class="texto txt_small" placeholder="Informe o valor" value="<?php echo "R$ ".number_format($movimentacao[valor], 2, ',', '.')?>">
                        </div>
                    </div>
                        
                        
                    <div id="aviso_mov_fixa_editar">
                            <p id="">Essa é uma movimentação fixa até 
                            <?php 
                                $datafinal_movimentacao_fixa_editar = retorna_datafim_mov($id_usuario, $key_mov);
                                echo date("d/m/Y", strtotime($datafinal_movimentacao_fixa_editar));
                            ?>
                            .</p>

                            <input type="radio" name="op_editar_mov" id="edita_essa" value="edita_essa"> <label for="edita_essa">Desejo editar somente essa movimentação.</label> <br>
                            <input type="radio" name="op_editar_mov" id="edita_proximas" value="edita_proximas"> <label for="edita_proximas">Desejo editar todas as próximas movimentações.</label> 
                    </div>
                        
                    
                        <input type="submit" value="Salvar" name="btn_editar_mov_fixa" id="btn_editar_mov_fixa">
                        <a href="http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>"><input type="button" value="Cancelar" name="btn_cancelar_edicao_mov_unica" id="btn_cancelar_edicao_mov_unica"></a>
                        
                        <div class="notificacao ok erro_edita" id="editada_sucesso_fixa">Movimentação alterada com sucesso.</div>
                        <div class="notificacao no erro_edita" id="data_vazia_fixa">Por favor, informe uma data.</div>
                        <div class="notificacao no erro_edita" id="edicao_data_invalida_fixa">Informe uma data válida.</div>
                        <div class="notificacao no erro_edita" id="edicao_valor_null_fixa">Por favor, informe o valor.</div>
                        <div class="notificacao no erro_edita" id="nao_selecionou_op_editar_fixa">Por favor, selecione uma opção.</div>
                        <div class="notificacao no erro_edita" id="mexeuNaData">Não é permitido alterar o mês dessa movimentação.</div>
                    
                </form>
                
                
                <?php
                    if(isset($_POST['btn_editar_mov_fixa'])){
                        $trata_erro_editar = false;
                        
                        $tipo = $_POST['edita_tipo_mov_fixa'];
                        $categoria = $_POST['categoria_atual_edita_fixa'];
                        $descricao = $_POST['descricao_atual_fixa'];
                        $data = $_POST['data_atual_fixa'];
                        $valor = $_POST['valor_atual_fixa'];
                        
                        $edita = $_POST['op_editar_mov'];
                        
                        
                        if($data == null){
                            ?>
                            <script>
                                $('#data_vazia_fixa').slideDown('normal');
                                $("#data_atual_fixa").css({"border": "solid 1px red"});
                            </script>
                            <?php
                            $trata_erro_editar = true;
                        }
                        
                        
                        if ($trata_erro_editar == FALSE){
                            $data = explode("/", "$data");
                            $dia = $data[0];
                            $mes = $data[1];
                            $ano = $data[2];
                        
                            //Padrão americano mes/dia/ano > 12/30/1995
                            //Senão for uma data válida
                            if ( !(checkdate($mes, $dia, $ano)) ) {
                                ?>
                                <script>
                                    $('#edicao_data_invalida_fixa').show('normal');
                                    $("#data_atual_fixa").css({"border": "solid 1px red"});
                                </script>
                                <?php
                                $trata_erro_editar = true;
                            }
                        }
                        
                        if ($trata_erro_editar == FALSE){
                            if( ($valor == null) || ($valor == "R$ 0,00") ){
                                ?>
                                <script>
                                    $('#edicao_valor_null_fixa').show('normal');
                                    $("#valor_atual_fixa").css({"border": "solid 1px red"});
                                </script>
                                <?php
                                $trata_erro_editar = true;
                            }
                        }
                        
                        
                        //SE TIVER TUDO CERTO, ALTERA...
                        if ($trata_erro_editar == FALSE){
                            
                            //Se o cara não selecionou nenhuma opção
                            if($edita == NULL){
                                
                                $data = $dia;
                                $data .= "/$mes";
                                $data .= "/$ano";
                                
                                /*GAMBIARRRRRRRAAAAAAAAA*/
                                if ($tipo == "Despesa"){
                                    $tipo_variavel = "despesa_edita_fixa";
                                }
                                else{
                                    $tipo_variavel = "receita_edita_fixa";
                                }
                                
                            ?>
                                <script>
                                    $('#nao_selecionou_op_editar_fixa').show('normal');
                                    $('#aviso_mov_fixa_editar').css({"border":"solid 1px red"});
                                    
                                    //Atribuindo as opções selecionadas, caso dê erro.
                                    document.getElementById("<?php echo $tipo_variavel ?>").checked = true;
                                    $("#categoria_atual_edita_fixa option[value='<?php echo $categoria ?>']").attr("selected", true);
                                    document.dados_mov_atual_fixa.descricao_atual_fixa.value = "<?php echo $descricao ?>";
                                    document.dados_mov_atual_fixa.data_atual_fixa.value = "<?php echo $data ?>";
                                    document.dados_mov_atual_fixa.valor_atual_fixa.value = "<?php echo $valor ?>";
                                    
                                </script>
                            <?php
                            }
                            elseif ($edita == "edita_essa"){
                                
                                $data = $ano;
                                $data .= "-$mes";
                                $data .= "-$dia";

                                $valor = str_replace("R", "", $valor);
                                $valor = str_replace("$", "", $valor);
                                $valor = str_replace(".", "", $valor);
                                $valor = str_replace(",", ".", $valor);
                                
                               $alterou = edita_movimentacao_unica($id_usuario, $id_mov, $key_mov, $tipo, $categoria, $descricao, $data, $valor);
                            
                                if($alterou){
                                    ?>
                                    <script>
                                        $('#editada_sucesso_fixa').show('normal');
                                    </script>
                                    <?php
                                    echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                                } 
                            }
                            //Senão é porque o cara quer que edite tudo daqui pra frente
                            else{
                                
                                /*GAMBIARRRRRRRAAAAAAAAA*/
                                if ($tipo == "Despesa"){
                                    $tipo_variavel = "despesa_edita_fixa";
                                }
                                else{
                                    $tipo_variavel = "receita_edita_fixa";
                                }
                                
                                
                                //se o burro do usuário mudar o mês da receita fixa, da erro. Porque ele tem que alterar a data desse mes
                                $trata_erro_mexeu_mesOuAno_data = false;
                                if( ($mes != date('m', strtotime($movimentacao[data]))) ||  ($ano != date('Y', strtotime($movimentacao[data])))){
                                    
                                    $data = "$dia/";
                                    $data .= "$mes/";
                                    $data .= $ano;
                                    ?>
                                    <script>
                                        $('#mexeuNaData').show('normal');
                                        $('#data_atual_fixa').css({"border":"solid 1px red"});

                                        //Atribuindo as opções selecionadas, caso dê erro.
                                        document.getElementById("<?php echo $tipo_variavel ?>").checked = true;
                                        $("#categoria_atual_edita_fixa option[value='<?php echo $categoria ?>']").attr("selected", true);
                                        document.dados_mov_atual_fixa.descricao_atual_fixa.value = "<?php echo $descricao ?>";
                                        document.dados_mov_atual_fixa.data_atual_fixa.value = "<?php echo $data ?>";
                                        document.dados_mov_atual_fixa.valor_atual_fixa.value = "<?php echo $valor ?>";
                                    
                                    </script>
                                    
                                    <?php
                                    $trata_erro_mexeu_mesOuAno_data = true;
                                }
                                
                                
                                
                                if($trata_erro_mexeu_mesOuAno_data == false){
                                    
                                    $data = $ano;
                                    $data .= "-$mes";
                                    $data .= "-$dia";

                                    $valor = str_replace("R", "", $valor);
                                    $valor = str_replace("$", "", $valor);
                                    $valor = str_replace(".", "", $valor);
                                    $valor = str_replace(",", ".", $valor);
                                    
                                    $alterou = edita_proximas_movimentacoes($id_usuario, $id_mov, $key_mov, $tipo, $categoria, $descricao, $data, $valor);
                                    if($alterou){
                                        ?>
                                        <script>
                                            $('#editada_sucesso_fixa').show('normal');
                                        </script>
                                        <?php
                                        echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                                    } 
                                }
                            }
                            
                            
                            
                            
                        }
                        
                        
                    }
                ?>
                
                
            </div>
            
            
            
            
            
            
            

            <!------------------------------------------EXCLUIR MOV ÚNICA------------------------------------------------>
            <div id="popup_excluir_movimentacao_unica" title="Excluir movimentação">
                
                <img src="img/trash9.png">
                <span>Ter certeza que deseja excluir essa movimentação?</span>

                <form method="post">
                    <input type="submit" value="Sim" name="btn_excluir_mov_unica" id="btn_excluir_mov_unica">
                    <a href="http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>"><input type="button" value="Não" name="btn_cancelar_mov_unica" id="btn_cancelar_mov_unica"></a>
                    
                    <div class="notificacao ok erro_exclui" id="excluiu_sucesso_unica">Movimentação excluída com sucesso.</div>
                </form>
                
                <?php
                /*SE O BOTÃO SIM FOR CLICADO*/
                if(isset($_POST['btn_excluir_mov_unica'])){
                    $id_mov = $_GET['idmov'];
                    $key_mov = $_GET['keymov'];
                    
                    $data_de = $_GET['dt_de'];
                    $data_ate = $_GET['dt_ate'];
                    
                    $atrelado = verifica_atrelamento_usuario_mov($id_usuario, $id_mov);
                    if ($atrelado) {
                        $excluiu = exclui_movimentacao_unica($id_usuario, $id_mov);
                        if($excluiu){
                            ?>
                            <script>
                                $("#popup_excluir_movimentacao_unica").dialog({
        //                          height: 170,
                                    width: 445,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de="<?php echo $data_de ?>"&dt_ate="<?php echo $data_ate ?>";
                                    }
                                });
                                $('#excluiu_sucesso_unica').slideDown('normal');
                            </script>
                            <?php   
                            echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                        }
                    }
                    else{
                        header('Location: base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'');
                    }
                }
                
                /*SE O BOTÃO NÃO FOR CLICADO*/
                if(isset($_POST['btn_cancelar_mov_unica'])){
                    $data_de = $_GET['dt_de'];
                    $data_ate = $_GET['dt_ate'];
                    header('Location: base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'');
//                    echo '<meta http-equiv="refresh" content="2;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                }
                
                ?>
            </div>

            
            
            
            
            <!------------------------------------------EXCLUIR MOV FIXA------------------------------------------------>
            <div id="popup_excluir_movimentacao_fixa" title="Excluir movimentação">
                
                <img src="img/trash9.png">
                <span>Ter certeza que deseja excluir essa movimentação?</span>
                <?php                    
                    $key_mov = $_GET['keymov'];
                ?>
                
                <form method="post">
                    <div id="aviso_mov_fixa">
                        <p id="">Essa é uma movimentação fixa até 
                        <?php 
                            $datafinal_movimentacao = retorna_datafim_mov($id_usuario, $key_mov);
                            echo date("d/m/Y", strtotime($datafinal_movimentacao));
                        ?>
                        .</p>

                        <input type="radio" name="op_excluir_mov" id="exclui_essa" value="exclui_essa"> <label for="exclui_essa">Desejo excluir somente essa movimentação.</label> <br>
                        <input type="radio" name="op_excluir_mov" id="exclui_proximas" value="exclui_proximas"> <label for="exclui_proximas">Desejo excluir todas as próximas movimentações.</label> <br>
                        <input type="radio" name="op_excluir_mov" id="exclui_todas" value="exclui_todas"> <label for="exclui_todas"><b>Atenção!</b> Desejo excluir todas, inclusive anteriores.</label>
                    </div>  

                    <input type="submit" value="Sim" name="btn_excluir_mov_fixa" id="btn_excluir_mov_fixa">
                        <a href="http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>" > <input type="button" value="Não" name="btn_cancelar_mov_fixa" id="btn_cancelar_mov_fixa"></a>

                        <div class="notificacao ok erro_exclui" id="excluiu_sucesso_fixa">Movimentação excluída com sucesso.</div>
                        <div class="notificacao no erro_exclui" id="nao_selecionou_op_fixa">Por favor, escolha uma opção.</div>
                </form>
                    
                <?php
                /*SE O BOTÃO SIM FOR CLICADO*/
                if(isset($_POST['btn_excluir_mov_fixa'])){
                    $opcao_escolhida = $_POST['op_excluir_mov'];
                    
                    /*Senão selecionar nenhuma opção, da o erro*/
                    if($opcao_escolhida == null){
                    ?>
                        <script>
                            $("#popup_excluir_movimentacao_fixa").dialog({
    //                          height: 170,
                                width: 445,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function() {
                                    //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de="<?php echo $data_de ?>"&dt_ate="<?php echo $data_ate ?>";
                                }
                            });
                            $('#nao_selecionou_op_fixa').slideDown('normal');
                            $('#aviso_mov_fixa').css({"border":"solid 1px red"});
                        </script>
                    <?php
                    }
                    else{
                        $id_mov = $_GET['idmov'];
                        $key_mov = $_GET['keymov'];

                        $data_de = $_GET['dt_de'];
                        $data_ate = $_GET['dt_ate'];

                        $atrelado = verifica_atrelamento_usuario_mov($id_usuario, $id_mov);
                        if ($atrelado) {
                            /*SE A OPÇÃO ESCOLHIDA FOR EXCLUI SÓ ESSA*/
                            if($opcao_escolhida == "exclui_essa"){
                                $exclui_essa = exclui_movimentacao_unica($id_usuario, $id_mov);
                                if($exclui_essa == TRUE){
                                ?>
                                    <script>
                                    $("#popup_excluir_movimentacao_fixa").dialog({
            //                          height: 170,
                                        width: 445,
                                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                        resizable: false,
                                        close: function() {
                                            //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de="<?php echo $data_de ?>"&dt_ate="<?php echo $data_ate ?>";
                                        }
                                    });
                                    $('#excluiu_sucesso_fixa').slideDown('normal');
                                    </script>
                            <?php   
                                    echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                                }
                            }
                            /*SE A OPÇÃO ESCOLHIDA FOR EXCLUIR AS PRÓXIMAS*/
                            elseif($opcao_escolhida == "exclui_proximas"){
                                $exclui_proximas = exclui_proximas_movimentacoes_fixas($id_usuario, $id_mov, $key_mov);
                                if($exclui_proximas){
                                ?>
                                    <script>
                                    $("#popup_excluir_movimentacao_fixa").dialog({
            //                          height: 170,
                                        width: 445,
                                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                        resizable: false,
                                        close: function() {
                                            //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de="<?php echo $data_de ?>"&dt_ate="<?php echo $data_ate ?>";
                                        }
                                    });
                                    $('#excluiu_sucesso_fixa').slideDown('normal');
                                    </script>
                                <?php 
                                echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                                }                                
                            }
                            elseif ($opcao_escolhida == "exclui_todas") {
                                $exclui_todas = exclui_todas_movimentacoes_fixas_porChave($id_usuario, $key_mov);
                                if($exclui_todas){
                                ?>
                                    <script>
                                    $("#popup_excluir_movimentacao_fixa").dialog({
            //                          height: 170,
                                        width: 445,
                                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                        resizable: false,
                                        close: function() {
                                            //location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de="<?php echo $data_de ?>"&dt_ate="<?php echo $data_ate ?>";
                                        }
                                    });
                                    $('#excluiu_sucesso_fixa').slideDown('normal');
                                    </script>
                                <?php 
                                echo '<meta http-equiv="refresh" content="1;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                                } 
                            }
                        }
                        else{
                            header('Location: base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'');
                        }
                    }
                }
                
                /*SE O BOTÃO NÃO FOR CLICADO*/
                if(isset($_POST['btn_cancelar_mov_fixa'])){
                    $data_de = $_GET['dt_de'];
                    $data_ate = $_GET['dt_ate'];
                    header('Location: base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'');
//                    echo '<meta http-equiv="refresh" content="2;url=http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de='.$data_de.'&dt_ate='.$data_ate.'">';
                }
                
                ?>   
            </div>



                <?php
                /* * ********************************AÇÃO DOS 3 BOTÕES - MARCAR PAGO... EDITAR .... E EXCLUIR*********************************************************** */

                /*ALTERANDO PARA PAGO_RECEBIDO*/
                if ($_GET['acao'] == 'atualizar') {
                    $id_mov = $_GET['idmov'];
                    $key_mov = $_GET['keymov'];

                    $atrelado = verifica_atrelamento_usuario_mov($id_usuario, $id_mov);
                    if ($atrelado) {
                        altera_pago_recebido($id_usuario, $id_mov)
                        ?>
                        <script>
    //                            alert("Movimentação alterada com sucesso!");
                        </script>
                        <?php
                        $data_de = $_GET['dt_de'];
                        $data_ate = $_GET['dt_ate'];
                        header('Location: base.php?p=movimentacoes&dt_de=' . $data_de . '&dt_ate=' . $data_ate . '');
                    }
                }


                
                
                if ($_GET['acao'] == 'editar_mov') {
                    $id_mov = $_GET['idmov'];
                    $key_mov = $_GET['keymov'];
                    
                     $atrelado = verifica_atrelamento_usuario_mov($id_usuario, $id_mov);
                    if ($atrelado) {
                        $tipo = verifica_tipo_movimentacao($id_mov);
                        if ($tipo == "Única") {
                            ?>
                            <script>
                                $("#popup_editar_movimentacao_unica").dialog({
//                                  height: 170,
                                    width: 310,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>";
                                    }
                                });
                            </script>
                        <?php
                        }
                        elseif($tipo == "Fixa"){
                            ?>
                            <script>
                                $("#popup_editar_movimentacao_fixa").dialog({
//                                  height: 170,
                                    width: 395,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>";
                                    }
                                });
                            </script>
                        <?php
                        }
                    }
                }




                if ($_GET['acao'] == 'excluir_mov') {
                    $id_mov = $_GET['idmov'];
                    $key_mov = $_GET['keymov'];

                    $atrelado = verifica_atrelamento_usuario_mov($id_usuario, $id_mov);
                    if ($atrelado) {
                        $tipo = verifica_tipo_movimentacao($id_mov);
                        if ($tipo == "Única") {
                            ?>
                            <script>
                                $("#popup_excluir_movimentacao_unica").dialog({
//                                  height: 170,
                                    width: 445,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>";
                                    }
                                });
                            </script>
                        <?php
                        }
                        elseif($tipo == "Fixa"){
                            ?>
                            <script>
                                $("#popup_excluir_movimentacao_fixa").dialog({
//                                  height: 170,
                                    width: 445,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function() {
                                        location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $data_de ?>&dt_ate=<?php echo $data_ate ?>";
                                    }
                                });
                            </script>
                        <?php
                        }
                    }
                }


                /* * ********************************FIM DA AÇÃO DOS 3 BOTÕES - MARCAR PAGO... EDITAR .... E EXCLUIR*********************************************************** */



                
                
                /*Mostrando o relatório*/
                retorna_movimentacoes($id_usuario, $data_de, $data_ate);
                ?> 
                            
                            
        </section>
                    
                    
        <?php
            if ($_GET['acao'] == 'new_Despesa'){
                ?>
                <script>
                    $("#popup_nova_despesa").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $_GET['dt_de'] ?>&dt_ate=<?php echo $_GET['dt_ate'] ?> ";
                            $("#popup_nova_categoria_despesa").dialog('close');
                        }
                    });
                </script>
                <?php
            }
            
            if ($_GET['acao'] == 'new_Receita'){
                ?>
                <script>
                    $("#popup_nova_receita").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function() {
                            location.href = "http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=<?php echo $_GET['dt_de'] ?>&dt_ate=<?php echo $_GET['dt_ate'] ?> ";
                            $("#popup_nova_categoria_receita").dialog('close');
                        }
                    });
                </script>
                <?php
            }
        ?>

    </body>
</html>