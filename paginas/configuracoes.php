<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/configuracoes.css" rel="stylesheet" type="text/css">
        
        <script>
            $(function($) {
                
                $("#btnCancTodasMov").click(function() {
                    $("#popup_deseja_apagar_tudo").dialog('close');
                });


                $("#chamaPopup_deseja_apagar_tudo").click(function() {
                    $("#popup_deseja_apagar_tudo").dialog({
                        width: 395,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false
                    });
                });
                
                
                
                
                
                $("#btnCancelarInativarConta").click(function() {
                    $("#popup_inativarconta").dialog('close');
                });


                $("#chamaPopup_inativarConta").click(function() {
                    $("#popup_inativarconta").dialog({
                        width: 487,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false
                    });
                });
                
                $("#btnCancelarExcluirConta").click(function() {
                    $("#popup_excluirconta").dialog('close');
                });


                $("#chamaPopup_excluirConta").click(function() {
                    $("#popup_excluirconta").dialog({
                        width: 450,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false
                    });
                });
                
            });
        </script>        
    </head>

    <body>
        <div id="central" class="">
            
            <div id="top" class="cor_de_fundo">
                <h3><img src="img/config2.png"> Configurações</h3>
            </div>
            
            
            <div id="lado_esquerdo" class="cor_de_fundo">
                <div id="div_perfil" class="divs">
                    <h3>PERFIL</h3>
                    <a href="base.php?p=perfil" id="dados_pessoais"><img src="img/user2.png"> Dados pessoais</a>
                    <span id="chamaPopup_inativarConta"><img src="img/inativeuser.png" id="inativeuser"> Desejo inativar minha conta</span>
                    <br>
                    <br>
                    <br>
                    <a href="base.php?p=senha" id="link_senha"><img src="img/password.png"> Senha</a>
                    <span id="chamaPopup_excluirConta"><img src="img/deleteuser.png" id="deleteuser"> Desejo excluir minha conta</span>
                </div>

                
                
                
                <!--Popup inativar conta-->
                <div id="popup_inativarconta" title="Inativar conta">
                    <h4>Tem certeza de que deseja inativar sua conta?</h4>
                    <h5>Inativar sua conta desabilitará seu perfil do GestaTudo temporariamente, porém, não iremos excluir nenhuma movimentação, metas ou categorias personalizadas.</h5>
                    
                    <form method="post" style="text-align: right">
                        <input type="submit" value="Confirmar" name="btnInativarConta" id="btnInativarConta">
                        <input type="button" value="Cancelar" name="btnCancelarInativarConta" id="btnCancelarInativarConta">
                    </form>
                    
                    <br>
                    <div class="notificacao ok" id="deixeFeedback">Por favor <?php echo $usuario[nome] ?>, deixe-nos um feedback. <br>Obrigado!</div>
                    
                    <?php
                        if(isset($_POST['btnInativarConta'])){
                            
                            inativaUsuario($id_usuario);
                            ?>
                            <script>
                                $("#popup_inativarconta").dialog({
                                    width: 487,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false
                                });
                                $('#btnInativarConta').attr('disabled', 'disabled');
                                $('#btnCancelarInativarConta').attr('disabled', 'disabled');
                                $('#deixeFeedback').slideToggle('fast');
                            </script>
                            <?php
                            unset($_SESSION['usuario_logado']);
                            echo '<meta http-equiv="refresh" content="3;url=http://localhost/GestaTudo/fale-conosco.php">';
                        }
                    ?>
                    
                </div>
                
                
                
                <!--Popup excluir conta-->
                <div id="popup_excluirconta" title="Excluir conta">
                    <h4>Tem certeza de que deseja excluir sua conta?</h4>
                    <h5><b>Atenção!</b> Excluir sua conta removerá seu perfil completamente do GestaTudo, excluiremos suas movimentações, metas e categorias personalizadas.</h5>
                    
                    <form method="post" style="text-align: right">
                        <input type="submit" value="Confirmar" name="btnExcluirConta" id="btnExcluirConta">
                        <input type="button" value="Cancelar" name="btnCancelarExcluirConta" id="btnCancelarExcluirConta">
                    </form>
                    
                    <br>
                    <div class="notificacao ok" id="deixeFeedbackExcluir"><?php echo $usuario[nome] ?>, gostaríamos que nos deixasse um feedback com o motivo de sua saída.<br>Obrigado!</div>
                    
                    <?php
                        if(isset($_POST['btnExcluirConta'])){
                            excluirUsuario($id_usuario);
                            ?>
                            <script>
                                $("#popup_excluirconta").dialog({
                                    width: 450,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false
                                });
                                $('#btnExcluirConta').attr('disabled', 'disabled');
                                $('#btnCancelarExcluirConta').attr('disabled', 'disabled');
                                $('#deixeFeedbackExcluir').slideToggle('fast');
                            </script>
                            <?php
                            echo '<meta http-equiv="refresh" content="5;url=http://localhost/GestaTudo/fale-conosco.php">';
                        }
                    ?>
                    
                </div>
                
                
                
                
                <div id="div_categorias" class="divs">
                    <h3>CATEGORIAS</h3>
                    <a href="base.php?p=categorias"><img src="img/categorias.png">Minhas categorias</a>
                    
                </div>
                
            </div>
            
            <div id="lado_direito" class="cor_de_fundo">
                <div id="div_notificacoes" class="divs">
                    <h3>NOTIFICAÇÕES</h3>
                    
                    <div id="img_mail">
                        <img src="img/mail.png">
                    </div>
                    
                    <div id="div_permiteNots">
                        <form method="post">
                            <input value="<?php echo $usuario[permite_notificacoes] ?>" type="checkbox" id="permiteNotificaoes" name="permiteNotificaoes" onclick="location.href='base.php?p=configuracoes&permite=true'"> <label for="permiteNotificaoes" id="labelPermiteNot" onclick="location.href='base.php?p=configuracoes&permite=true'">Deseja receber notificações por e-mail?</label>
                        </form>
                    </div>
                    <?php
                        if($usuario[permite_notificacoes] == 1){
                            ?>
                            <script>
                                //marcar
                                $('#permiteNotificaoes').prop('checked', true);
                            </script>
                            <?php
                            }
                            else{
                            ?>
                                <script>
                                    //desmarcar
                                    $('#permiteNotificaoes').removeAttr('checked');
                                </script>
                            <?php
                        }
                        
                        if($_GET[permite] == 'true'){
                            $editou = editaPermissaoNotificacoes($id_usuario);
                            if($editou){
                                header('Location: base.php?p=configuracoes');
                            }
                        }
                    ?>
                </div>

                <div id="div_movimentacoes" class="divs">
                    <h3>MOVIMENTAÇÕES</h3>
                    <span id="aviso_excluir">Clicando abaixo você irá excluir todas as movimentações já lançadas. Pense bem antes de fazer isso...</span>
                    <div id="link_excluir_movs">
                        <p id="chamaPopup_deseja_apagar_tudo"><img src="img/trash9.png">Excluir todas as minhas movimentações</p>
                    </div>
                    
                    <div id="popup_deseja_apagar_tudo" title="Atenção!">
                        <h4>Tem certeza que deseja excluir todas as suas movimentações?</h4>
                        <div id="form_deleta_todas_mov">
                            <form method="post">
                                <input type="submit" value="Sim" name="btnDelTodasMov" id="btnDelTodasMov">
                                <input type="button" value="Cancelar" name="btnCancTodasMov" id="btnCancTodasMov">
                            </form>
                        </div>
                        
                        <div class="notificacao ok" id="notificaDelTodasMovs">Movimentações excluídas com sucesso!</div>
                    </div>

                    <?php
                        if(isset($_POST['btnDelTodasMov'])){
                            $id_usuario = $usuario[id];
                            excluiTodasMovimentacoes($id_usuario);
                            ?>
                            <script>
                                $("#popup_deseja_apagar_tudo").dialog({
                                    width: 395,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false
                                });
                                
                                $("#notificaDelTodasMovs").slideToggle("normal");
                            </script>
                            <?php
                            echo '<meta http-equiv="refresh" content="1">';
                        }
                    ?>
                    
                    
                </div>
            </div>
            
        </div>
    </body>
</html>


