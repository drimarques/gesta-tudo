<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}

$meta = usuarioMeta($id_usuario);


//Se o cara quiser concluir a meta
if($_GET['conclui_meta'] == 'true' && $_GET['id_meta'] != ""){
    $id = $_GET['id_meta'];
    concluiMeta($id, $id_usuario);
}

?>

<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/metas.css" rel="stylesheet" type="text/css">
        <script src="highCharts/highcharts.js"></script>
        <script src="highCharts/modules/exporting.js"></script>
        <!--Mascara R$ 000,00 no valor-->
        <script src="jQuery/jquery.maskMoney.js"></script>
        <script>
            $(function() {
                $("#valor_meta").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
                $("#edita_valor_meta").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});
                

                $("#btnChamaMeta").click(function(){
                    $("#popup_meta").dialog({
                        width: 250,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function () {
                            location.href = "http://localhost/GestaTudo/base.php?p=metas";
                        }
                    });
                });
                
                
                
                
                
                //------------------Exluir meta--------------------
                $("#btnCalcelarExclusaoMeta").click(function() {
                    $("#popup_deseja_excluir_meta").dialog('close');
                });
               
                $("#excluir_meta").click(function() {
                    $("#popup_deseja_excluir_meta").dialog({
                        width: 320,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false
                    });
                });
                
                $("#btnCalcelarExclusaoMeta2").click(function() {
                    $("#popup_deseja_excluir_meta2").dialog('close');
                });
                //------------------/Exluir meta--------------------
                
                
                
                
                
                
                
                
                //Editar meta
                $("#btnCalcelarEditarMeta").click(function() {
                    $("#popup_editar_meta").dialog('close');
                });
               
                $("#editar_meta").click(function() {
                    $("#popup_editar_meta").dialog({
                        width: 220,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false
                    });
                });
                
               
            });
        </script>
        
    </head>
    <body>
        
        <div id="centro" class="cor_de_fundo">
            
            <div id="metas_concluidas">
                <h3 id="title_metas_concluidas">METAS CONCLUÍDAS</h3>
                <?php retornaMetasConluidas($id_usuario); ?>
                
            </div>
            
            <div id="btnChamaMeta"><img src="img/meta.png" title="Cadastrar nova meta"><br>Inserir meta</div>
        </div>
        
        
        
        
        
        
        <div id="popup_meta" title="Cadastrar meta">
            <form method="post" name="form_meta" id="form_meta">
                <label for="nome_meta">Nome da meta:</label>
                <br>
                <input type="text" id="nome_meta" name="nome_meta" class="texto" placeholder="Digite aqui" maxlength="35">
                
                <br>
                <br>
                
                <label for="mes_fim_meta">Até quando deseja concluir sua meta?</label>
                <br>
                <select id="mes_fim_meta" name="mes_fim_meta" class="texto">
                    <option selected="" value="" disabled="">Mês</option>
                    <option value="01">Janeiro</option>
                    <option value="02">Fevereiro</option>
                    <option value="03">Março</option>
                    <option value="04">Abril</option>
                    <option value="05">Maio</option>
                    <option value="06">Junho</option>
                    <option value="07">Julho</option>
                    <option value="08">Agosto</option>
                    <option value="09">Setembro</option>
                    <option value="10">Outubro</option>
                    <option value="11">Novembro</option>
                    <option value="12">Dezembro</option>
                </select>
                &nbsp
                <select id="ano_fim_meta" name="ano_fim_meta" class="texto">
                    <option selected="" value="" disabled="">Ano</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                </select>

                <br>
                <br>
                
                <label for="valor_meta">Valor pretendido:</label>
                <br>
                <input type="text" id="valor_meta" name="valor_meta" class="texto" placeholder="Valor">
                
                <br>
                <br>
                <input type="submit" name="btnCadastraMeta" id="btnCadastraMeta" value="Salvar">
            </form>
            
            <!--Erros-->
            <div class="notificacao no" id="nomeMetaNull">Digite o nome da meta</div>
            <div class="notificacao no" id="dataFinalMetaNull">Selecione até quando deseja concluir sua meta</div>
            <div class="notificacao no" id="dataFinalMetaMenorDataAtual">A data da meta não pode ser menor que a data atual</div>
            <div class="notificacao no" id="valorMetaNull">Informe o valor da meta</div>
            
            <?php 
            
                if(isset($_POST['btnCadastraMeta'])){
                    $nomeMeta = $_POST['nome_meta'];
                    $mesFinalMeta = $_POST['mes_fim_meta'];
                    $anoFinalMeta = $_POST['ano_fim_meta'];
                    $valorMeta = $_POST['valor_meta'];
                    $erroMetas = false;
                    
                    $dataAtual = date('Y-m');
                    $dataFinalMeta = "$anoFinalMeta-$mesFinalMeta";
                    //echo "$nomeMeta - $mesFinalMeta - $anoFinalMeta - $valorMeta";
                    
                    //Tratando nome da meta
                    if($nomeMeta == ""){
                        $erroMetas = true;
                        ?>
                        <script>
                            $("#popup_meta").dialog({
                                width: 250,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function () {
                                    location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                }
                            });
                            
                            $("#nomeMetaNull").slideToggle("normal");
                            $("#nome_meta").css({"border":"solid 1px red"});
                            
                            document.form_meta.nome_meta.value = "<?php echo $nomeMeta ?>";
                            $("#mes_fim_meta option[value='<?php echo $mesFinalMeta ?>']").attr("selected", true);
                            $("#ano_fim_meta option[value='<?php echo $anoFinalMeta ?>']").attr("selected", true);
                            document.form_meta.valor_meta.value = "<?php echo $valorMeta ?>";
                        </script>
                        <?php
                    }
                    
                    //Tratando erro de mes e ano
                    if($erroMetas == false){
                        if($mesFinalMeta == "" || $anoFinalMeta == ""){
                            $erroMetas = true;
                        ?>
                        <script>
                            $("#popup_meta").dialog({
                                width: 250,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function () {
                                    location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                }
                            });
                            
                            $("#dataFinalMetaNull").slideToggle("normal");
                            $("#mes_fim_meta").css({"border":"solid 1px red"});
                            $("#ano_fim_meta").css({"border":"solid 1px red"});
                            
                            document.form_meta.nome_meta.value = "<?php echo $nomeMeta ?>";
                            $("#mes_fim_meta option[value='<?php echo $mesFinalMeta ?>']").attr("selected", true);
                            $("#ano_fim_meta option[value='<?php echo $anoFinalMeta ?>']").attr("selected", true);
                            document.form_meta.valor_meta.value = "<?php echo $valorMeta ?>";
                        </script>
                        <?php
                        }
                        
                        
                        //Se a data final for menor que a dataAtual
                        elseif ($dataFinalMeta <= $dataAtual) {
                            $erroMetas = true;
                            ?>
                            <script>
                                $("#popup_meta").dialog({
                                    width: 250,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function () {
                                        location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                    }
                                });

                                $("#dataFinalMetaMenorDataAtual").slideToggle("normal");
                                $("#mes_fim_meta").css({"border":"solid 1px red"});
                                $("#ano_fim_meta").css({"border":"solid 1px red"});

                                document.form_meta.nome_meta.value = "<?php echo $nomeMeta ?>";
                                $("#mes_fim_meta option[value='<?php echo $mesFinalMeta ?>']").attr("selected", true);
                                $("#ano_fim_meta option[value='<?php echo $anoFinalMeta ?>']").attr("selected", true);
                                document.form_meta.valor_meta.value = "<?php echo $valorMeta ?>";
                            </script>
                        <?php
                        }
                    }
                    
                    //Tratando erro de valor
                    if($erroMetas == false){
                        if ($valorMeta == null || $valorMeta == 'R$ 0,00') {
                           $erroMetas = true;
                            ?>
                            <script>
                                $("#popup_meta").dialog({
                                    width: 250,
                                    modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                    resizable: false,
                                    close: function () {
                                        location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                    }
                                });

                                $("#valorMetaNull").slideToggle("normal");
                                $("#valor_meta").css({"border":"solid 1px red"});

                                document.form_meta.nome_meta.value = "<?php echo $nomeMeta ?>";
                                $("#mes_fim_meta option[value='<?php echo $mesFinalMeta ?>']").attr("selected", true);
                                $("#ano_fim_meta option[value='<?php echo $anoFinalMeta ?>']").attr("selected", true);
                                document.form_meta.valor_meta.value = "<?php echo $valorMeta ?>";
                            </script>
                            <?php 
                        }
                    }
                    
                    
                    //Se não tiver erro nenhum, cadastra
                    if($erroMetas == false){
                        $valorMeta = str_replace("R", "", $valorMeta);
                        $valorMeta = str_replace("$", "", $valorMeta);
                        $valorMeta = str_replace(".", "", $valorMeta);
                        $valorMeta = str_replace(",", ".", $valorMeta);
                        cadastraMeta($id_usuario, $nomeMeta, $dataAtual, $dataFinalMeta, $valorMeta);
                        echo '<meta http-equiv="refresh" content="0">';
                    }
                    
                }
            ?>            
        </div>

        <?php
            //Senão tem meta
            if($meta == false){
               ?>
                <script>
                    $("#btnChamaMeta").css({"display":"initial"});
                    $("#btnChamaMeta").css({"margin-top":"110px"});
                    $("#centro").css({"min-height":"400px"});
                </script>
                
                <div id="popup_deseja_excluir_meta2" title="Atenção!">
                    <h4>Deseja realmente excluir sua meta?</h4>
                    <form method="post" id="form_excluir_meta2">
                        <input type="submit" name="btnExluirMeta2" id="btnExluirMeta2" value="Sim">
                        <input type="button" name="btnCalcelarExclusaoMeta2" id="btnCalcelarExclusaoMeta2" value="Não">
                    </form>
                    <?php
                        if($_GET['acao_meta'] == 'excluir' && $_GET['id_meta'] !=  ""){
                        ?>
                        <script>
                            $("#popup_deseja_excluir_meta2").dialog({
                                width: 320,
                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                resizable: false,
                                close: function () {
                                    location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                }
                            });
                        </script>
                        <?php
                        }
                        
                        if(isset($_POST['btnExluirMeta2'])){
                            $id_metaExcluir = $_GET['id_meta'];
                            excluirMetaUsuario($id_metaExcluir, $id_usuario);
                        }
                    ?>
                </div>
                <?php
                    //Se tem metas concluídas, deixa o titulo e a tabela
                    $temMetaConcluida = retornaSeMetaConcluida($id_usuario);
                    if($temMetaConcluida == false){
                        ?>
                        <script>
                            $("#title_metas_concluidas").css({"display":"none"});
                        </script>
                        <?php
                    } 
                    else{
                        ?>
                        <script>
                            $("#btnChamaMeta").css({"margin-top":"50px"});
                            $("#centro").css({"min-height":"600px"});
                        </script>
                        <?php
                    }
            }
            //Se tem meta, some com o botão de lançar meta
            else{
                ?>
                <script>
                    $("#btnChamaMeta").css({"display":"none"});
                    $("#centro").css({"display":"none"});
                </script>
                <?php
                
                //Chama função que atualiza o saldo inicial da meta
                    $saldoInicialMeta = atualizaSaldoInicialMeta($id_usuario);
                    $diaFinalMeta = date("d", strtotime($meta[data_fim]));
                    $mesFinalMeta = date("m", strtotime($meta[data_fim]));
                    $nomeMesFinalMeta = retorna_nome_mes($mesFinalMeta);
                    $anoFinalMeta = date("Y", strtotime($meta[data_fim]));
                
                ?>
                
                <div id="topo_metas" class="cor_de_fundo">
                    <h3>Meta: <?php echo $meta[nome_meta] ?>.</h3>
                    <h3>Valor: R$ <?php echo number_format($meta[valor], 2, ',', '.') ?> até <?php echo "$diaFinalMeta de $nomeMesFinalMeta de $anoFinalMeta"?>.</h3>
                    
                    <p id="excluir_meta"><img src="img/trash9.png">Excluir meta</p>
                    <p id="editar_meta"><img src="img/editar2.png">Editar meta</p>
                    
                    <!--Popup exluir meta-->
                    <div id="popup_deseja_excluir_meta" title="Atenção!">
                        <h4>Deseja realmente excluir sua meta?</h4>
                        <form method="post" id="form_excluir_meta">
                            <input type="submit" name="btnExluirMeta" id="btnExluirMeta" value="Sim">
                            <input type="button" name="btnCalcelarExclusaoMeta" id="btnCalcelarExclusaoMeta" value="Não">
                        </form>
                        
                        <?php
                            if(isset($_POST['btnExluirMeta'])){
                                excluirMetaUsuario($meta[id_meta], $id_usuario);
                            }
                        ?>
                    </div>
                    
                    
                    <!--Popup editar meta-->
                    <div id="popup_editar_meta" title="Editar meta">    
                        <form method="post" id="form_editar_meta">
                            <table>
                                <tbody>
                                    <tr>
                                        <td colspan="2"><label for="edita_nome_meta">Nome:</label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><input value="<?php echo $meta[nome_meta] ?>" type="text" id="edita_nome_meta" name="edita_nome_meta" class="texto" maxlength="35"></td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2"><label for="edita_mes_fim_meta">Data de conclusão</label></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <select id="edita_mes_fim_meta" name="edita_mes_fim_meta" class="texto">
                                            <option selected value="<?php echo date('m', strtotime($meta[data_fim])) ?>"><?php echo retorna_nome_mes(date('m', strtotime($meta[data_fim]))) ?></option>
                                            <option value="01">Janeiro</option>
                                            <option value="02">Fevereiro</option>
                                            <option value="03">Março</option>
                                            <option value="04">Abril</option>
                                            <option value="05">Maio</option>
                                            <option value="06">Junho</option>
                                            <option value="07">Julho</option>
                                            <option value="08">Agosto</option>
                                            <option value="09">Setembro</option>
                                            <option value="10">Outubro</option>
                                            <option value="11">Novembro</option>
                                            <option value="12">Dezembro</option>
                                            </select>
                                        </td>
                                        
                                        <td>
                                            <select id="edita_ano_fim_meta" name="edita_ano_fim_meta" class="texto">
                                            <option selected value="<?php echo date('Y', strtotime($meta[data_fim])) ?>"><?php echo retorna_nome_mes(date('Y', strtotime($meta[data_fim]))) ?></option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2"><label for="edita_valor_meta" id="label_edita_valor_meta">Valor:</label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><input type="text" id="edita_valor_meta" name="edita_valor_meta" class="texto" value="<?php echo number_format($meta[valor], 2, ',', '.') ?>"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="submit" name="btnSalvarEditarMeta" id="btnSalvarEditarMeta" value="Salvar"></td>
                                        <td><input type="button" name="btnCalcelarEditarMeta" id="btnCalcelarEditarMeta" value="Cancelar"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        
                        <!--Erros-->
                        <div class="notificacao no" id="editaNomeMetaNull">Por favor, informe o nome da meta.</div>
                        <div class="notificacao no" id="editaValorMetaNull">Por favor, informe o valor da meta.</div>
                        <div class="notificacao no" id="editaDataMetaNull">Por favor, informe a data final da meta.</div>
                        <div class="notificacao no" id="editaDataMetaMenor">A data final deve ser maior que a data atual.</div>
                        
                        <?php
                            if(isset($_POST['btnSalvarEditarMeta'])){
                                $nomeEditaMeta = $_POST['edita_nome_meta'];
                                $mesEditaMeta = $_POST['edita_mes_fim_meta'];
                                $anoEditaMeta = $_POST['edita_ano_fim_meta'];
                                $valorEditaMeta = $_POST['edita_valor_meta'];
                                $erroEditaMeta = false;
                                
                                $dataFimEditaMeta = "$anoEditaMeta-$mesEditaMeta";
                                ///echo $dataFimEditaMeta;
                                
                                //Tratando nome da meta
                                if($nomeEditaMeta == ""){
                                    $erroEditaMeta = true;
                                    ?>
                                    <script>
                                        $("#popup_editar_meta").dialog({
                                            width: 220,
                                            modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                            resizable: false,
                                            close: function () {
                                                location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                            }
                                        });

                                        $("#editaNomeMetaNull").slideToggle("normal");
                                        $("#edita_nome_meta").css({"border":"solid 1px red"});
                                        
                                        document.form_editar_meta.edita_nome_meta.value = "<?php echo $nomeEditaMeta ?>";
                                        $("#edita_mes_fim_meta option[value='<?php echo date('m', strtotime($meta[data_fim])) ?>']").attr("selected", true);
                                        $("#edita_ano_fim_meta option[value='<?php echo date('Y', strtotime($meta[data_fim])) ?>']").attr("selected", true);
                                        document.form_editar_meta.edita_valor_meta.value = "<?php echo $valorEditaMeta ?>";
                                    </script>
                                    <?php
                                }
                                
                                
                                
                                //Tratando data final da meta
                                if($dataFimEditaMeta <= date('Y-m')){
                                    $erroEditaMeta = true;
                                        ?>
                                        <script>
                                            $("#popup_editar_meta").dialog({
                                                width: 220,
                                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                                resizable: false,
                                                close: function () {
                                                    location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                                }
                                            });

                                            $("#editaDataMetaMenor").slideToggle("normal");
                                            $("#edita_mes_fim_meta").css({"border":"solid 1px red"});
                                            $("#edita_ano_fim_meta").css({"border":"solid 1px red"});
                                            //$("#edita_mes_fim_meta option[value='<?php echo date('m', strtotime($meta[data_fim])) ?>']").attr("selected", true);
                                            //$("#edita_ano_fim_meta option[value='<?php echo date('Y', strtotime($meta[data_fim])) ?>']").attr("selected", true);
                                            document.form_editar_meta.edita_nome_meta.value = "<?php echo $nomeEditaMeta ?>";
                                            document.form_editar_meta.edita_valor_meta.value = "<?php echo $valorEditaMeta ?>";
                                        </script>
                                        <?php
                                }
                                
                                
                                
                                //Tratando erro de valor
                                if($erroEditaMeta == false){
                                    if ($valorEditaMeta == null || $valorEditaMeta == 'R$ 0,00') {
                                       $erroEditaMeta = true;
                                        ?>
                                        <script>
                                            $("#popup_editar_meta").dialog({
                                                width: 220,
                                                modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                                                resizable: false,
                                                close: function () {
                                                    location.href = "http://localhost/GestaTudo/base.php?p=metas";
                                                }
                                            });

                                            $("#editaValorMetaNull").slideToggle("normal");
                                            $("#edita_valor_meta").css({"border":"solid 1px red"});
                                            //$("#edita_mes_fim_meta option[value='<?php echo date('m', strtotime($meta[data_fim])) ?>']").attr("selected", true);
                                            //$("#edita_ano_fim_meta option[value='<?php echo date('Y', strtotime($meta[data_fim])) ?>']").attr("selected", true);
                                            document.form_editar_meta.edita_nome_meta.value = "<?php echo $nomeEditaMeta ?>";
                                            document.form_editar_meta.edita_valor_meta.value = "<?php echo $valorEditaMeta ?>";
                                        </script>
                                        <?php 
                                    }
                                }


                                //Se não tiver erro nenhum, edita...
                                if($erroEditaMeta == false){
                                    $valorEditaMeta = str_replace("R", "", $valorEditaMeta);
                                    $valorEditaMeta = str_replace("$", "", $valorEditaMeta);
                                    $valorEditaMeta = str_replace(".", "", $valorEditaMeta);
                                    $valorEditaMeta = str_replace(",", ".", $valorEditaMeta);
                                    
                                    editaMeta($meta[id_meta], $id_usuario, $nomeEditaMeta, $dataFimEditaMeta, $valorEditaMeta);
                                    echo '<meta http-equiv="refresh" content="0">';
                                }
                                
                                
                                
                                
                                
                            }
                        ?>
                    </div>
                    
                    
                    
                    <?php 
                        $numeroMesesRestantes = retornaQtdMesesRestante($meta[id_meta]);
                        $restante = retornaRestanteParaMeta($meta[id_meta], $id_usuario, $saldoInicialMeta);
                        $saldoAtualDaMeta = retornaSaldoAtualParaMeta($meta[id_meta], $id_usuario, $saldoInicialMeta);
                        if($restante < 0){
                            $restante = 0;
                        }
                        
                        //Fazendo condição pra ver se o cara concluiu a meta;
                        if($numeroMesesRestantes == 0){
                            if($restante > 0){
                                echo "<div id='fim_meta'>$usuario[nome], você não atingiu sua meta no tempo em que esperava. Faltaram R$ ".number_format($restante, 2, ',', '.')."  :(</div>";
                            }
                            //Senão ele conseguiu concluir
                            else{
                                echo "<div id='concluiu_meta'><b>Parabéns</b> $usuario[nome]! Você concluiu sua meta no tempo esperado com R$ ".number_format($saldoAtualDaMeta, 2, ',', '.')." :D<br> Clique <a id='aqui_meta' href='base.php?p=metas&conclui_meta=true&id_meta=$meta[id_meta]'><u>aqui</u></a> para finalizar essa meta.</div>";
                            }
                            
                        }
                        elseif($numeroMesesRestantes == 1){
                            //Se não acabou o prazo da meta
                                $economiza = $restante / $numeroMesesRestantes;
                                
                                if($economiza == '0'){
                                    echo "<div id='esta_no_caminho'>$usuario[nome], por enquanto você está com saldo acima de sua meta... <br>Continue assim para concluí-la com no mínimo <b>R$ ".number_format($meta[valor], 2, ',', '.')."</b>.</div>";
                                }
                                else{
                                    echo "<div id='previsao_meta'>$usuario[nome], você deve economizar <b>R$ ".number_format($economiza, 2, ',', '.')."</b> por mês para atingir sua meta. Resta apenas <b>$numeroMesesRestantes mês</b>...</div>";
                                }
                        }
                        else{
                            //Se não acabou o prazo da meta
                                $economiza = $restante / $numeroMesesRestantes;
                                
                                if($economiza == '0'){
                                    echo "<div id='esta_no_caminho'>$usuario[nome], por enquanto você está com saldo acima de sua meta... <br>Continue assim para concluí-la com no mínimo <b>R$ ".number_format($meta[valor], 2, ',', '.')."</b>.</div>";
                                }
                                else{
                                    echo "<div id='previsao_meta'>$usuario[nome], você deve economizar <b>R$ ".number_format($economiza, 2, ',', '.')."</b> por mês para atingir sua meta. Restam <b>$numeroMesesRestantes meses</b>...</div>";
                                }
                        }
                        
                        
                    ?>
                    
                    
                </div>
                
                
                
                
                
                <div id="resumo_meta" class="cor_de_fundo">

                    <h3>EVOLUÇÃO</h3>

                    <?php retornaTabelaEvolucaoMesesMeta($meta[id_meta], $id_usuario, $saldoInicialMeta); ?>
                </div>

                <div id="lado_direito" class="cor_de_fundo">
                    <div id="grafico_porcentagem" style="min-width: 240px; height: 330px; max-width: 600px; margin: 0 auto">

                    </div>
                </div>
                
                <!--<div class="clear"></div>-->
                
                <?php
                
                
            }
        ?>
        
        <script>
            //Gráfico de pizza
            $(function() {
                $("#valor_meta").maskMoney({showSymbol: true, symbol: "R$ ", decimal: ",", thousands: "."});

                $("#btnChamaMeta").click(function(){
                    $("#popup_meta").dialog({
                        width: 250,
                        modal: true, /*MUITO LOOOOOOOOOOOOCO*/
                        resizable: false,
                        close: function () {
                            location.href = "http://localhost/GestaTudo/base.php?p=metas";
                        }
                    });
                });  



                //Gráfico
                // Radialize the colors
                Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
                    return {
                        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
                        stops: [
                            [0, color],
                            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                        ]
                    };
                });

                // Build the chart
                $('#grafico_porcentagem').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: 'Percentual de conclusão da meta'
                    },
                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                },
                                connectorColor: 'silver'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '%',
                        data:[ 
                                {
                                    name: 'Concluído',
                                    y: <?php echo retornaSaldoAtualParaMeta($meta[id_meta], $id_usuario, $saldoInicialMeta) ?> ,
                                    //sliced: true,
                                    //selected: true,
                                    color: 'rgb(120,200,255)'
                                },
                                {
                                    name: 'Restante',
                                    y: <?php echo retornaRestanteParaMeta($meta[id_meta], $id_usuario, $saldoInicialMeta) ?>,
                                    color: 'rgba(255,99,71, 0.8)'
                                }

                        ]
                    }]
                });
            });
        </script>
    </body>
</html>

