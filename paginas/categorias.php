<?php
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}
?>

<!DOCTYPE HTML>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/categorias.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        
        
        <div id="centro_categorias" class="cor_de_fundo">
            <div id="left" class="cor_de_fundo">
                <h3>CATEGORIAS DE RECEITAS</h3>
                <form method="post" id="formNewCategoryReceita">
                    <label for="newCategoryReceita">Nova categoria:</label><br>
                    <input type="text" id="newCategoryReceita" name="newCategoryReceita" class="texto" maxlength="15">
                    <input type="submit" value="Adicionar" id="btnNewCategoryReceita" name="btnNewCategoryReceita"> 
                </form>
                
                <div id="erroCatReceitaJaExiste" class="notificacao no">Você já cadastrou essa categoria</div>
                <div id="erroCatReceitaPadrao" class="notificacao no">Essa é uma categoria padrão já existente</div>
                
                <?php 
                    retornaCategoriasReceitas($id_usuario);
                ?>
            </div>

            <div id="right" class="cor_de_fundo">
                <h3>CATEGORIAS DE DESPESAS</h3>
                <form method="post" id="formNewCategoryDespesa">
                    <label for="newCategoryDespesa">Nova categoria:</label><br>
                    <input type="text" id="newCategoryDespesa" name="newCategoryDespesa" class="texto" maxlength="15">
                    <input type="submit" value="Adicionar" id="btnNewCategoryDespesa" name="btnNewCategoryDespesa"> 
                </form>
                
                <div id="erroCatDespesaJaExiste" class="notificacao no">Você já cadastrou essa categoria</div>
                <div id="erroCatDespesaPadrao" class="notificacao no">Essa é uma categoria padrão já existente</div>
                
                <?php 
                    retornaCategoriasDespesas($id_usuario);
                ?>
            </div>
        </div>
        
        <?php
            if(isset($_POST['btnNewCategoryReceita'])){
                $novaCategoriaReceita = $_POST['newCategoryReceita'];

                $trataErroPadrao = verificaCategoriasReceitas($novaCategoriaReceita);
                if($trataErroPadrao == true){
                   ?>
                    <script>
                        $("#erroCatReceitaPadrao").slideToggle("normal");
                    </script>
                    <?php 
                }
                
                $trataErroPers = verificaCategoriasReceitasPersonalizadas($id_usuario, $novaCategoriaReceita);
                if ($trataErroPers == true){
                    ?>
                    <script>
                        $("#erroCatReceitaJaExiste").slideToggle("normal");
                    </script>
                    <?php
                }
                
                if(($trataErroPers == false) && ($trataErroPadrao == false)){
                    insereCategoriaReceita($id_usuario, $novaCategoriaReceita);
                    header("Location: base.php?p=categorias");
                }
            }
        ?>
                    
                    
                    
       <?php
            if(isset($_POST['btnNewCategoryDespesa'])){
                $novaCategoriaDespesa = $_POST['newCategoryDespesa'];

                $trataErroPadrao = verificaCategoriasDespesas($novaCategoriaDespesa);
                if($trataErroPadrao == true){
                   ?>
                    <script>
                        $("#erroCatDespesaPadrao").slideToggle("normal");
                    </script>
                    <?php 
                }
                
                $trataErroPers = verificaCategoriasDespesasPersonalizadas($id_usuario, $novaCategoriaDespesa);
                if ($trataErroPers == true){
                    ?>
                    <script>
                        $("#erroCatDespesaJaExiste").slideToggle("normal");
                    </script>
                    <?php
                }
                
                if(($trataErroPers == false) && ($trataErroPadrao == false)){
                    insereCategoriaDespesa($id_usuario, $novaCategoriaDespesa);
                    header("Location: base.php?p=categorias");
                }
            }
        ?>
        
        
        
        
        
        
        
    </body>
</html>

