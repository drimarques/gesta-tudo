<!DOCTYPE HTML>
<?php

session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}

?>

<html lang="pt-br">
    <head>
        <title>GestaTudo - <?php echo $nome_pagina ?></title> <!--ucwords é pra colocar a primeira letra do nome da pagina em maiuculo-->
        <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
        <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, pagina nao encontrada">
        <meta name="author" content="Adriano Marques">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/pagina-nao-encontrada.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="box">
            <img src="img/warning.png">
            <h3>Ops! Página não encontrada.</h3>
            <h4>Possivéis causas:</h4>
            <ul>
                <li>O conteúdo não está mais no ar.</li>
                <li>A página mudou de lugar.</li>
                <li>O Servidor está fora do ar.</li>
                <li>Você digitou o endereço errado.</li>
            </ul>
        </div>
    </body>
</html>
