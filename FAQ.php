<?php
$arquivo = $_GET["arquivo"];
if (isset($arquivo) && file_exists($arquivo)) {
    switch (strtolower(substr(strrchr(basename($arquivo), "."), 1))) {
        case "pdf": $tipo = "application/pdf";
            break;
        case "exe": $tipo = "application/octet-stream";
            break;
        case "zip": $tipo = "application/zip";
            break;
        case "doc": $tipo = "application/msword";
            break;
        case "xls": $tipo = "application/vnd.ms-excel";
            break;
        case "ppt": $tipo = "application/vnd.ms-powerpoint";
            break;
        case "gif": $tipo = "image/gif";
            break;
        case "png": $tipo = "image/png";
            break;
        case "jpg": $tipo = "image/jpg";
            break;
        case "mp3": $tipo = "audio/mpeg";
            break;
        case "php": // deixar vazio por seurança
        case "htm": // deixar vazio por seurança
        case "html": // deixar vazio por seurança
    }
    header("Content-Type: " . $tipo); // informa o tipo do arquivo ao navegador
    header("Content-Length: " . filesize($arquivo)); // informa o tamanho do arquivo ao navegador
    header("Content-Disposition: attachment; filename=" . basename($arquivo)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
    readfile($arquivo); // lê o arquivo
    exit; // aborta pós-ações 
}
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>GestaTudo - FAQ</title>
        <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
        <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, FAQ, perguntas, pergunta">
        <meta name="author" content="Adriano Marques">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/FAQ.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="img/logomini.png">
        <script src="jQuery/jquery-2.1.1.js"></script>
    </head>
    <body>
        <div class="centralizer">
            <header><!--cabeçalho-->
                <a href="base.php?p=home" id="logo">                        
                    <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                </a>
            </header>

            <section>
                <h1 id="titulo">Perguntas frequentes</h1>


                <ul>
                    <li class="perguntas" id="pergunta1">
                        Como faço qualquer merda?
                    </li>

                    <div class="respostas" id="resposta1">
                        assim...
                    </div>


                    <li class="perguntas" id="pergunta2">Pergunta 2</li>

                    <div class="respostas" id="resposta2">
                        Resposta 2
                    </div>


                    <li class="perguntas" id="pergunta3">Pergunta 3</li>

                    <div class="respostas" id="resposta3">
                        Resposta 3
                    </div>


                    <li class="perguntas" id="pergunta4">Pergunta 4</li>

                    <div class="respostas" id="resposta4">
                        Resposta 4
                    </div>
                    
                    
                    
                    <h3 id="duvida_aqui">Sua dúvida não está aqui?</h3>
                    <h5 id="sub_titulo"><a href="fale-conosco.php" id="fale_conosco">Fale conosco</a> ou faça <a href="FAQ.php?arquivo=downloads/Sumario.pdf" id="download">download</a> do manual de usuário completo!</h5>
                    
                </ul>

                

            </section>

        </div>
        <footer><!--Rodapé-->
<!--            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=" . date('m') . "&ano=" . date('Y') . "' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>-->
            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
            <br>
            <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
        </footer>
        <script src="js/FAQ.js"></script>
    </body>
</html>
