<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../jQuery/jquery-2.1.1.js"></script>
        <link href="../jQuery/jquery-ui-date-picker/jquery-ui.css" rel="stylesheet" type="text/css">
        <script src="../jQuery/jquery-ui-date-picker/jquery-ui.js"></script> 

        <script>
            $(function($) {

                $(".data").datepicker({
                    dateFormat: 'dd/mm/yy',
                    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    nextText: 'Próximo',
                    prevText: 'Anterior'
                });

            });
        </script>

    </head>
    <body>

        <form method="post">
            <input type="text" name="data" class="data">
            <input type="submit" name="botao" id="botao">
        </form>


        <?php
        if (isset($_POST['botao'])) {
            $data = $_POST['data'];

            $data = explode("/", "$data"); // fatia a string $dat em pedados, usando / como referência
            $dia = $data[0];
            $mes = $data[1];
            $ano = $data[2];

            // verifica se a data é válida!
            // 1 = true (válida)
            // 0 = false (inválida)
            
            if (checkdate($mes, $dia, $ano)) {
                if($ano > 1996){
                    echo "Insira uma data anterior à 1996";
                }
                else{
                    echo "Data válida";
                }
            } 
            else {
                echo "data inválida!";
            }
        }
        ?>
    </body>
</html>

