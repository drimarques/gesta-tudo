<?php ?>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>


<script>
    $(function () {
        $('#container').highcharts({
            chart: {
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 13,
                    beta: 13,
                    viewDistance: 25,
                    depth: 40
                },
                marginTop: 80,
                marginRight: 40
            },
            title: {
                text: 'Total fruit consumption, grouped by gender'
            },
            xAxis: {
                categories: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Bananas']
            },
            yAxis: {
                allowDecimals: false,
                min: 0,
                title: {
                    text: 'Number of fruits'
                }
            },
            tooltip: {
                headerFormat: '<b>{point.key}</b><br>',
                pointFormat: '<span style="color:{series.color}">\u25CF</span> {series.name}: R$ {point.y:f}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    depth: 40
                }
            },
            series: [{
                    name: 'Receitas',
                    data: [2000.99, 1800, 1000, 0, 0],
                    stack: 'Receitas'
                }, {
                    name: 'Despesas',
                    data: [0, 9.00, 0, 0, 0],
                    stack: 'Despesas'
                }]
        });
    });
</script>


<style>
    #container {
        height: 400px; 
        min-width: 310px; 
        max-width: 800px;
        margin: 0 auto;
    }  
</style>


<div id="container" style="height: 400px"></div>
