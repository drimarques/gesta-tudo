<!DOCTYPE HTML>
<?php
date_default_timezone_set('Brazil/East');
date_default_timezone_set('America/Sao_Paulo');
session_start();
if (!isset($_SESSION['usuario_logado'])) {
    header("Location: index.php");
    exit;
}

$p = $_GET['p'];

if (empty($p)) {
    $pagina = 'home';
} else {
    $pagina = $p;
}

$nome_pagina;
if ($pagina == "home") {
    $a1 = "selecionado";
    $imghome = "img/home2.png";
    $nome_pagina = "Home";
} elseif (($pagina == "receita") || ($pagina == "despesa")) {
    $a2 = "selecionado";
    $imghome = "img/home2.png";
    $nome_pagina = "Novo lançamento";
} elseif ($pagina == "relatorios") {
    $a3 = "selecionado";
    $imghome = "img/home2.png";
    $nome_pagina = "Relatórios";
} elseif ($pagina == "banco") {
    $a4 = "selecionado";
    $imghome = "img/home2.png";
    $nome_pagina = "Acesse seu banco";
} elseif (($pagina == "perfil") || ($pagina == "senha")) {
    $a5 = "selecionado";
    $imghome = "img/home2.png";
    $nome_pagina = "Configurações de perfil";
}
?>


<html lang="pt-br">
    <head>
        <title>GestaTudo - <?php echo $nome_pagina ?></title> <!--ucwords é pra colocar a primeira letra do nome da pagina em maiuculo-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/estilo.base2.css" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900italic,900,300italic,300,100italic,100' rel='stylesheet' type='text/css'>
        <script src="jQuery/jquery-2.1.1.js"></script>
        <script src="jQuery/jCycle/jquery.cycle2.min.js"></script>
        <script src="jQuery/productiondropdown.js"></script>
        <script>
            $(document).ready(function() {
                $("#cam").mouseover(function() {
                    $("#notifica_foto").fadeIn("normal");
                });

                $("#cam").mouseout(function() {
                    $("#notifica_foto").fadeOut("fast");
                });
            });
        </script>
    </head>
    <body>
        <div class="centralizer">
            <header><!--Cabeçalho-->
            <?php
                require_once('config/conexao.php');
                require_once('config/funcoes.php');
                $email = $_SESSION['usuario_logado'];
                $busca_usu_logado = mysql_query("SELECT * FROM usuarios WHERE email = '$email'");
                $usuario = mysql_fetch_array($busca_usu_logado);
                $nome = $usuario['nome'];

                //script responsável por carregar a foto do uauário
                if ($usuario['foto'] == NULL) {
                    $foto_usuario = 'user.png';
                } else {
                    $foto_usuario = $usuario['foto'];
                }
            ?>

                <a href="base.php?p=home"><div id="logo"><!--Logo-->
                        Logo
                    </div></a>

                <div id="foto_usuario">
                    <img src="img_perfil_usuarios/<?php echo $foto_usuario ?>">
                </div>
                <a href="base.php?p=perfil"><img src="img/cam2.png" id="cam">
                    <div id="notifica_foto"><p>Atualizar foto do perfil</p></div>
                </a>
            </header>

            <nav><!--Menu-->
                <a href="base.php?p=home"><img id="home" src="<?php echo $imghome ?>" class="<?php echo $a1 ?>"></a>        

                <ul id="tabs">
                    <li class="<?php echo $a2 ?>">Lançamentos ▼
                        <ul class="dropdown">
                            <a href="<?php echo '?p=receita' ?>"><li><img src="img/new.png">Nova receita</li></a>
                            <a href="<?php echo '?p=despesa' ?>"><li><img src="img/new2.png">Nova despesa</li></a>
                        </ul>
                    </li>
                    
                    <a href="<?php echo '?p=relatorios' ?>"><li class="<?php echo $a3 ?>">Relatórios</li></a>
                    <a href="<?php echo '?p=banco' ?>"><li class="<?php echo $a4 ?>">Acesse seu banco</li></a>

                    <li id="nome-usuario" class="<?php echo $a5 ?>"><p><?php echo $nome ?> ▼</p>

                        <ul class="dropdown">
                            <a href="<?php echo '?p=perfil' ?>"><li><img src="img/config.png">Configurações</li></a>
                            <a href="logout.php"><li><img src="img/logout.png">Logout</li></a>
                        </ul>
                    </li>
                </ul>
            </nav>

            <!--Fazendo uma gambiarra no CSS-->
            <div class="clear"></div>
            <section>
                <?php
                #se o arquivo existir, a gente inclui ele aqui
                if (file_exists('paginas/' . $pagina . '.php')) {
                    include("paginas/$pagina.php");
                } else {
                    echo "Página não encontrada.";
                }
                ?>
            </section>
        </div>
        <footer><!--Rodapé-->
                <?php echo 'Copyright &copy' . date('Y ') . '<a href="base.php?p=home" id="gestatudo">GestaTudo</a> - Todos direitos reservados'; ?>
            <p><a href="#">Quem somos</a> - <a href="#">Fale conosco</a></p>
        </footer>
    </body>
</html>