﻿<?php
session_start();
unset($_SESSION['usuario_logado']);
?>

<!DOCTYPE HTML>
<html lang="pt-br">
     <head>
          <title>GestaTudo</title>
          <meta name="description" content="GestaTudo - Sistema de controle financeiro Web">
          <meta name="keywords" content="gestatudo, gesta tudo, besaba, sistema, sistemas, controle, financeiro, pessoal, web, login, cadastro, cadastros">
          <meta name="author" content="Adriano Marques">
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link href="css/estilo.index.css" rel="stylesheet" type="text/css">
          <link rel="shortcut icon" href="img/logomini.png">
          <!--        <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900italic,900,300italic,300,100italic,100' rel='stylesheet' type='text/css'>-->
          <script src="jQuery/jquery-2.1.1.js"></script>
          <script src="jQuery/jCycle/jquery.cycle2.min.js"></script>
          <script src="jQuery/jquery.maskedinput.min.js"></script> 


          <!--                      Responsável pelo datepicker-->
          <link href="jQuery/jquery-ui-custom/jquery-ui.css" rel="stylesheet" type="text/css">
          <script src="jQuery/jquery-ui-custom/jquery-ui.js"></script> 

          <script>
               $(function ($) {
                    $('#tablet').cycle({
                         fx: 'fade',
                         speed: 1000
                    });

                    $("#nascimento").mask("99/99/9999");
                    $(".data").datepicker({
                         dateFormat: 'dd/mm/yy',
                         dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                         dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
                         dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
                         monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                         monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                         nextText: 'Próximo',
                         prevText: 'Anterior',
                         yearRange: '1900: 1998',
                         changeMonth: true,
                         changeYear: true
                    });



                    //Tratando focus nos campos
                    $("#txt_cad_senha").focus(function () {
                         $('#notifica_senha').slideDown('normal');
                    });

                    $("#txt_cad_senha").blur(function () {
                         $('#notifica_senha').slideUp('normal');
                    });

               });




          </script>
     </head>
     <body>
          <div class="centralizer"> 
               <header>
                    <?php
                    require_once('config/conexao.php');
                    require_once('config/funcoes.php');
                    ?>

<!--                    <a href="base.php?p=home&mes=<?php echo date('m') ?>&ano=<?php echo date('Y') ?>" id="logo">-->
                    <a href="base.php?p=home" id="logo">                        
                         <img src="img/logogestatudo.png" alt="Logo GestaTudo">
                    </a>



                    <div id="login"><!--Login-->
                         <form method="post" name="login">
                              <table id="tabela-login" cellspacing="1">
                                   <thead>
                                        <tr>
                                             <th><label for="txt_login">E-mail:</label></th>
                                             <th><label for="txt_senha">Senha:</label></th>
                                        </tr>
                                   </thead>

                                   <tfoot>
                                        <tr>
                                             <td><a href="esqueceu-a-senha.php" id="esqueci_senha">Esqueceu sua senha?</a></td>
                                             <td><input type="submit" name="btn_entrar" value="Entrar" id="btn_entrar" class="transparente"></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="erro_senha"><?php echo "Senha incorreta." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="erro_usu_n_cadastrado"><?php echo "E-mail ou a senha incorretos." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="emailOUsenha_null"><?php echo "Por favor, preencha todos os campos." ?></div></td>
                                        </tr>
                                   </tfoot>

                                   <tbody>
                                        <tr>
                                             <td><input type="email" name="txt_login" placeholder="Digite seu e-mail" maxlength="255" id="txt_login" class="texto transparente" required oninvalid="setCustomValidity('Por favor, preencha com seu e-mail.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                             <td><input type="password" name="txt_senha" placeholder="Digite sua senha" maxlength="255" id="txt_senha" required class="texto transparente" oninvalid="setCustomValidity('Por favor, preencha com sua senha.')" onchange="try {
                                                           setCustomValidity('');
                                                      } catch (e) {
                                                      }"></td>
                                        </tr>
                                   </tbody>  
                              </table>
                         </form>


                         <!--AÇÃO DO BOTÃO ENTRAR btn_entrar -->
                         <?php
                         if (isset($_POST['btn_entrar'])) {
                              $email = $_POST['txt_login'];
                              $senha = $_POST['txt_senha'];
                              $trata_erro = FALSE;

                              if ($email == "") {
                                   ?>
                                   <script>
                                        $('#emailOUsenha_null').slideDown('slow');
                                        $("#txt_login").css({"border": "solid 1px red"});

                                        document.login.txt_login.value = "<?php echo $email ?>";
                                        document.login.txt_senha.value = "<?php echo $senha ?>";
                                   </script>
                                   <?php
                                   $trata_erro = true;
                              }


                              if ($senha == "") {
                                   ?>
                                   <script>
                                        $('#emailOUsenha_null').slideDown('slow');
                                        $("#txt_senha").css({"border": "solid 1px red"});

                                        document.login.txt_login.value = "<?php echo $email ?>";
                                        document.login.txt_senha.value = "<?php echo $senha ?>";
                                        document.login.txt_senha.focus();
                                   </script>
                                   <?php
                                   $trata_erro = true;
                              }


                              if ($trata_erro == FALSE) {
                                   $cadastrado = verifica_email_cadastrado($email);
                                   if ($cadastrado == FALSE) {
                                        ?>
                                        <script>
                                             $('#erro_usu_n_cadastrado').slideDown('normal');
                                        </script>
                                        <?php
                                        $trata_erro = TRUE;
                                   }
                              }


                              if ($trata_erro == FALSE) {
                                   $email_senha_correto = verifica_emailesenha($email, $senha);

                                   if ($email_senha_correto == TRUE) {

                                        /* Verificamos se o e-mail foi confirmado, se o usuário está ativo 0 ou */
                                        $usuario = retorna_usuario($email);

                                        if ($usuario['ativo'] == 0) {
                                             $_SESSION['usuario_logado'] = $email;
                                             header('Location: acesse-seu-email.php');
                                             $trata_erro = TRUE;
                                        }

                                        /* Se o e-mail e a senha tiverem corretos e o usuário tiver ativo, verificamos se o usuário está com a senha padrão,
                                          /se estiver ele será direcionado para tela redefine-senha.php */

                                        if ($trata_erro == false) {

                                             $senha_aleatoria = verifica_senha_aleatoria($usuario['id']);
                                             if ($senha_aleatoria == TRUE) {
                                                  $_SESSION['usuario_logado'] = $email;
                                                  header('Location: redefine-senha.php');
                                             } else {
                                                  $_SESSION['usuario_logado'] = $email;
//                                        header('Location: base.php?p=home&mes='.date("m").'&ano='.date("Y").'');
                                                  header('Location: base.php?p=home');
                                             }
                                        }
                                   } else {
                                        ?>
                                        <script>
                                             $('#erro_senha').slideDown('normal');
                                             $("#txt_senha").css({"border": "solid 1px red"});
                                             document.login.txt_login.value = "<?php echo $email ?>";
                                             document.getElementById('txt_senha').focus();
                                        </script>
               <?php
          }
     }
}
?>

                    </div>

                    <!--Cadastro com sucesso-->
                    <div class="notificacao ok" id="cadastro_ok"><?php echo "Cadastro realizado com sucesso!<br>Acesse seu e-mail para ativar sua conta." ?></div>

                    <!--E-mail ativo com sucesso-->
                    <div class="notificacao ok" id="email_ativo"><?php echo "Conta ativada com sucesso!" ?></div>

               </header>
               <section>
                    <div id="cadastro"> <!--Cadastro-->
                         <form method="post" name="cadastro">
                              <table cellspacing="1"> 
                                   <thead>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="email_ja_cadastrado"><?php echo "E-mail já cadastrado.<br> Esqueceu sua senha? <a href=\"esqueceu-a-senha.php\" id=\"clique_aqui\">Clique aqui.</a>" ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="erro_email"><?php echo "Os e-mails informados devem ser iguais." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="erro_senhas_erradas"><?php echo "As senhas informadas devem ser iguais." ?></div></td>
                                        </tr>
        <!--                                <tr>
                                            <td colspan="2"><div class="notificacao ok" id="cadastro_ok"><?php echo "Cadastro realizado com sucesso!<br>Acesse seu e-mail para ativar sua conta." ?></div></td>
                                        </tr>-->
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="data_invalida"><?php echo "Insira uma data válida." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="menor_idade"><?php echo "Insira uma data anterior há 1996." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="nome_null"><?php echo "Por favor, informe seu nome." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="nome_menor_3"><?php echo "Seu nome deve ter no mínimo 3 caracteres." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="sobrenome_null"><?php echo "Por favor, informe seu sobrenome." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="sobrenome_menor_3"><?php echo "Seu sobrenome deve ter no mínimo 3 caracteres." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="carateres_especiais"><?php echo "Este campo não aceita números, espaços ou <br>caracteres especiais." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="email_null"><?php echo "Por favor, informe seu e-mail." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="email_invalido"><?php echo "Formato de e-mail inválido." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="senha_naoTemNumero"><?php echo "Sua senha deve conter ao menos 1 número." ?></div></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><div class="notificacao no" id="senha_menor_8"><?php echo "Sua senha deve ter no mínimo 8 caracteres." ?></div></td>
                                        </tr>

                                   </thead>
                                   <tbody>
                                        <tr>
                                             <td><label for="txt_nome">Nome:</label></td>
                                             <td><label for="txt_sobrenome" id="senha">Sobrenome:</label></td>
                                        </tr>
                                        <tr>
                                             <td><input type="text" title="No mínimo 3 e no máximo 20 caracteres." placeholder="Digite seu nome" class="texto txt_pequeno" id="txt_nome" name="txt_nome" maxlength="20" required required oninvalid="setCustomValidity('Por favor, preencha com o seu sobrenome.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                             <td><input type="text" title="No mínimo 3 e no máximo 20 caracteres." placeholder="Digite seu sobrenome" class="texto txt_pequeno" id="txt_sobrenome" name="txt_sobrenome" maxlength="20" required oninvalid="setCustomValidity('Por favor, preencha com o seu sobrenome.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                        </tr> 

                                        <tr>
                                             <td colspan="2"><label for="txt_cad_email">E-mail:</label></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><input type="email" placeholder="Digite seu e-mail" class="texto txt_grande" maxlength="255" name="txt_cad_email" id="txt_cad_email" required oninvalid="setCustomValidity('Por favor, preencha com seu e-mail.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                        </tr>

                                        <tr>
                                             <td colspan="2"><label for="txt_conf_email">Confirmar e-mail:</label></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><input type="email" placeholder="Confirme seu e-mail" class="texto txt_grande" maxlength="255" name="txt_conf_email" id="txt_conf_email" required oninvalid="setCustomValidity('Por favor, confirme seu e-mail.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                        </tr>

                                        <tr>
                                             <td colspan="2"><label for="txt_cad_senha">Senha:</label></td>
                                        </tr>
                                        <tr>
                                             <td colspan="2"><input type="password" title="Sua senha deve conter no mínimo 8 caracteres e pelo menos um número." maxlength="255" placeholder="Digite sua senha" class="texto txt_grande" name="txt_cad_senha" id="txt_cad_senha" required oninvalid="setCustomValidity('Por favor, preencha sua senha')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                        </tr>

                                        <tr>
                                             <td colspan="2"><span id='notifica_senha' class="notifica_preechimento">Sua senha deve ter no mínimo 8 caracteres e pelo menos 1 número.</span></td>
                                        </tr>

                                        <tr>
                                             <td colspan="2"><label for="txt_conf_senha">Confirmar senha:</label></td>
                                        </tr>

                                        <tr>
                                             <td colspan="2"><input type="password" title="Sua senha deve conter no mínimo 8 caracteres e pelo menos um número." maxlength="255" placeholder="Novamente a senha" class="texto txt_grande" name="txt_conf_senha" id="txt_conf_senha" required oninvalid="setCustomValidity('Por favor, confirme sua senha.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                        </tr>

                                        <tr>
                                             <td colspan="2"><label for="nascimento">Data de nascimento:</label></td>
                                        </tr>

                                        <tr>
                                             <td><input type="text" max="1996-12-31" id="nascimento" placeholder="Data de nascimento" name="nascimento" class="texto data" required oninvalid="setCustomValidity('Por favor, informe sua data de nascimento.')" onchange="try {
                                                   setCustomValidity('');
                                              } catch (e) {
                                              }"></td>
                                        </tr> 
                                        <tr>
                                             <td><input type="submit" name="btn_criarConta" value="Criar conta" class="transparente" id="btn_criarConta"> </td>
                                        </tr>                      
                                   </tbody>
                              </table>
                         </form>



                    </div>

                    <!--AÇÃO DO BOTÃO "CRIAR CONTA"--> 
<?php
if (isset($_POST['btn_criarConta'])) {

     $nome = $_POST['txt_nome'];
     $sobrenome = $_POST['txt_sobrenome'];
     $email = $_POST['txt_cad_email'];
     $confirma_email = $_POST['txt_conf_email'];
     $senha = $_POST['txt_cad_senha'];
     $confirma_senha = $_POST['txt_conf_senha'];
     $data_nascimento = $_POST['nascimento'];

     $erro = false;

     //Tratando campo nome
     if ($nome == "") {
          ?>
                              <script>
                                   $('#nome_null').slideDown('slow');
                                   $("#txt_nome").css({"border": "solid 1px red"});

                                   document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                   document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                   document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                   document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                   document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                   document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                   document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                              </script>
          <?php
          $erro = true;
     } elseif (strlen($nome) < 3) {
          ?>
                              <script>
                                   $('#nome_menor_3').slideDown('slow');
                                   $("#txt_nome").css({"border": "solid 1px red"});

                                   document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                   document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                   document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                   document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                   document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                   document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                   document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                              </script>
          <?php
          $erro = true;
     } else {
          $caracteres_especiais = verifica_caracteres($nome);
          if ($caracteres_especiais) {
               ?>
                                   <script>
                                        $('#carateres_especiais').slideDown('slow');
                                        $("#txt_nome").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }



     //Tratando campo sobrenome
     if ($erro == false) {
          if ($sobrenome == "") {
               ?>
                                   <script>
                                        $('#sobrenome_null').slideDown('slow');
                                        $("#txt_sobrenome").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          } elseif (strlen($sobrenome) < 3) {
               ?>
                                   <script>
                                        $('#sobrenome_menor_3').slideDown('slow');
                                        $("#txt_sobrenome").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          } else {
               $caracteres_especiais = verifica_caracteres($sobrenome);
               if ($caracteres_especiais) {
                    ?>
                                        <script>
                                             $('#carateres_especiais').slideDown('slow');
                                             $("#txt_sobrenome").css({"border": "solid 1px red"});

                                             document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                             document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                             document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                             document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                             document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                             document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                             document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                        </script>
                    <?php
                    $erro = true;
               }
          }
     }



     //Tratando campo e-mail
     if ($erro == false) {
          if ($email == "") {
               ?>
                                   <script>
                                        $('#email_null').slideDown('slow');
                                        $("#txt_cad_email").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
          // Também verifica se não existe nenhum erro anterior
          elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
               ?>
                                   <script>
                                        $('#email_invalido').slideDown('slow');
                                        $("#txt_cad_email").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }

     //Tratando campo confirmar e-mail
     if ($erro == false) {
          if ($confirma_email == "") {
               ?>
                                   <script>
                                        $('#email_null').slideDown('slow');
                                        $("#txt_conf_email").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
          // Também verifica se não existe nenhum erro anterior
          elseif (!filter_var($confirma_email, FILTER_VALIDATE_EMAIL)) {
               ?>
                                   <script>
                                        $('#email_invalido').slideDown('slow');
                                        $("#txt_conf_email").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }




     //Tratando campo SENHA
     if ($erro == false) {

          if ($senha == "") {
               ?>
                                   <script>
                                        $('#senha_null').slideDown('slow');
                                        $("#txt_cad_senha").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          } elseif (strlen($senha) < 8) {
               ?>
                                   <script>
                                        $('#senha_menor_8').slideDown('slow');
                                        $("#txt_cad_senha").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          } else {
               $tem_numero = verificaExisteNumeros($senha);
               if ($tem_numero == FALSE) {
                    ?>
                                        <script>
                                             $('#senha_naoTemNumero').slideDown('slow');
                                             $("#txt_cad_senha").css({"border": "solid 1px red"});

                                             document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                             document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                             document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                             document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                             document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                             document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                             document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                        </script>
                    <?php
                    $erro = true;
               }
          }
     }



     if ($erro == false) {
          $erro_email = emails_iguais($email, $confirma_email); //Chamada da função emails_iguais                        
          if ($erro_email) {
               ?>
                                   <script>
                                        $('#erro_email').slideDown('slow');
                                        $("#txt_cad_email").css({"border": "solid 1px red"});
                                        $("#txt_conf_email").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }

     if ($erro == false) {
          $senhas_diferentes = senhas_iguais($senha, $confirma_senha);
          if ($senhas_diferentes) {
               ?>
                                   <script>
                                        $('#erro_senhas_erradas').slideDown('slow');
                                        $("#txt_cad_senha").css({"border": "solid 1px red"});
                                        $("#txt_conf_senha").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }

     if ($erro == false) {
          $data_nascimento = explode("/", "$data_nascimento");
          $dia = $data_nascimento[0];
          $mes = $data_nascimento[1];
          $ano = $data_nascimento[2];

          //Padrão americano mes/dia/ano > 12/30/1995
          if (checkdate($mes, $dia, $ano)) {
               if ($ano > 1998) {
                    ?>
                                        <script>
                                             $('#menor_idade').slideDown('slow');
                                             $("#nascimento").css({"border": "solid 1px red"});
                                             document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                             document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                             document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                             document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                             document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                             document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                        </script>
                    <?php
                    $erro = true;
               }
          } else {
               ?>
                                   <script>
                                        $('#data_invalida').slideDown('slow');
                                        $("#nascimento").css({"border": "solid 1px red"});
                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }


     if ($erro == false) {
          $ja_cadastrado = verifica_email_cadastrado($email);
          if ($ja_cadastrado) {
               ?>
                                   <script>
                                        $('#email_ja_cadastrado').slideDown('slow');
                                        $("#txt_cad_email").css({"border": "solid 1px red"});
                                        $("#txt_conf_email").css({"border": "solid 1px red"});

                                        document.cadastro.txt_nome.value = "<?php echo $nome ?>";
                                        document.cadastro.txt_sobrenome.value = "<?php echo $sobrenome ?>";
                                        document.cadastro.txt_cad_email.value = "<?php echo $email ?>";
                                        document.cadastro.txt_conf_email.value = "<?php echo $confirma_email ?>";
                                        document.cadastro.nascimento.value = "<?php echo $data_nascimento ?>";
                                        document.cadastro.txt_cad_senha.value = "<?php echo $senha ?>";
                                        document.cadastro.txt_conf_senha.value = "<?php echo $confirma_senha ?>";
                                   </script>
               <?php
               $erro = true;
          }
     }
     //Senão ocorrer nenhum erro de cadastro, o usuário é inserido no banco, e o e-mail de boas vidas é disparado
     if ($erro == false) {
          $data_nascimento = $ano;
          $data_nascimento .= "-$mes";
          $data_nascimento .= -$dia;

          //Gerando a chave pro e-mail
          $chave = uniqid(rand(), true);
          $chave.= time();

          $sucesso = cadastra_usuario($nome, $sobrenome, $email, $senha, $data_nascimento, $chave);
          if ($sucesso == 1) {
               $retorno = retorna_usuario($email);
               $id_usuario = $retorno['id'];

               email_boasVindas($id_usuario, $nome, $sobrenome, $email, $chave);
               ?>
                                   <script>
                                        $(document).ready(function () {
                                             $('#cadastro_ok').slideDown('slow');
                                        });
                                   </script>
                                   <?php
                              }
                         }
                    }
                    ?>
                    <div id="tablet"><!--Tablet-->
                         <img src="img/img_tablets_fade/img1.png" class="img_dt_tablet">
                         <img src="img/img_tablets_fade/img2.png" class="img_dt_tablet">
                         <img src="img/img_tablets_fade/img3.png" class="img_dt_tablet">
                         <img src="img/img_tablets_fade/img4.png" class="img_dt_tablet">
                         <img src="img/img_tablets_fade/img5.png" class="img_dt_tablet">
                    </div>
               </section>
          </div>

          <div class="clear"></div>

          <footer><!--Rodapé-->
  <!--            <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home&mes=" . date('m') . "&ano=" . date('Y') . "' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>-->
               <span><?php echo "Copyright &copy" . date('Y ') . "<a href='base.php?p=home' id='gestatudo'>GestaTudo</a> - Todos direitos reservados"; ?></span>
               <br>
               <a href="quem-somos.php">Quem somos</a> - <a href="fale-conosco.php">Fale conosco</a>
          </footer>

<?php
if ($_GET['ativo'] == 'true') {
     ?>
               <script>
                    $('#email_ativo').slideDown('fast');
               </script>
     <?php
}
?>

     </body>
</html>