<?php

function retornaCategoriasReceitas($id_usuario){
    $ha_resultados = false;
    $mensagem = "<h2 class='sem_resultados_categorias'>Nenhuma categoria de receita personalizada cadastrada.</h2>";
    
    //Select das despesas atrasadas
    $sql = "SELECT * FROM categorias_receitas_usuario WHERE id_usuario = '$id_usuario'";
    $busca = mysql_query($sql);
    $tem = mysql_num_rows($busca);
    
    if($tem){
        $ha_resultados = true;
        $tabelaCatReceitas = "<table>
                                <thead>
                                    <tr>
                                        <th colspan='2'>Categorias</th>                                        
                                    </tr>
                                </thead>";
        
        while ($retorno = mysql_fetch_array($busca)) {          
            $tabelaCatReceitas.="<tbody>
                                    <tr>
                                        <td>$retorno[categoria]</td>
                                        <td><a href='config/exclui-categoria.php?id=$retorno[id_cat_receita_usuario]&tipo=Rec'><img src='img/trash9.png'></a></td>
                                    </tr>
                                </tbody>"; 
        }
        
        $tabelaCatReceitas .= "</table>";
    }
    
    
    if($ha_resultados == true){
        echo $tabelaCatReceitas;
    }
    else{
        echo $mensagem;
    }
    
}



function retornaCategoriasDespesas($id_usuario){
    $ha_resultados = false;
    $mensagem = "<h2 class='sem_resultados_categorias'>Nenhuma categoria de despesa personalizada cadastrada.</h2>";
    
    //Select das despesas atrasadas
    $sql = "SELECT * FROM categorias_despesas_usuario WHERE id_usuario = '$id_usuario'";
    $busca = mysql_query($sql);
    $tem = mysql_num_rows($busca);
    
    if($tem){
        $ha_resultados = true;
        $tabelaCatDespesas = "<table>
                                <thead>
                                    <tr>
                                        <th colspan='2'>Categorias</th>                                        
                                    </tr>
                                </thead>";
        
        while ($retorno = mysql_fetch_array($busca)) {          
            $tabelaCatDespesas.="<tbody>
                                    <tr>
                                        <td>$retorno[categoria]</td>
                                        <td><a href='config/exclui-categoria.php?id=$retorno[id_cat_despesa_usuario]&tipo=Des'><img src='img/trash9.png'></a></td>
                                    </tr>
                                </tbody>"; 
        }
        
        $tabelaCatDespesas .= "</table>";
    }
    
    
    if($ha_resultados == true){
        echo $tabelaCatDespesas;
    }
    else{
        echo $mensagem;
    }
    
}