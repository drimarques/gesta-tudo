<?php

//Função que cadastra a meta
function cadastraMeta($id_usuario, $nomeMeta, $dataAtual, $dataFinalMeta, $valorMeta){
    $dataInicio = date('Y-m-01');
    $mesFinal = date('m', strtotime("$dataFinalMeta"));
    $dia = retorna_dia_final_mes($mesFinal);
    $dataFinalMeta = "$dataFinalMeta-$dia";
    
    
    
    $insert = "INSERT INTO metas (id_usuario, nome_meta, data_inicio, data_fim, valor, concluida)
                VALUES (
                '$id_usuario',
                '$nomeMeta',
                '$dataInicio',
                '$dataFinalMeta', 
                '$valorMeta',
                '0'
               )";
    
    
    mysql_query($insert);
    
    
    $sqlIdMeta = "SELECT id_meta FROM metas WHERE id_usuario = '$id_usuario' and concluida = '0'";
    $busca = mysql_query($sqlIdMeta);
    $retorno = mysql_fetch_array($busca);
    $id_meta = $retorno[id_meta];
    
    
    
    
    
    
    while($dataAtual < $dataFinalMeta){
        
        $dataInsert = "";
        
        $mes = date('m', strtotime("$dataAtual"));
        $dia = retorna_dia_final_mes($mes);
        $dataInsert = "$dataAtual-$dia";
        
        $insert = "INSERT INTO metas_detalhado (id_meta, data_fecha_mes)
                VALUES(
                '$id_meta',
                '$dataInsert'    
               )";
    
        mysql_query($insert);
               
        $dataAtual = date('Y-m', strtotime("+1 months", strtotime("$dataAtual")));
    }
}




//Função simples pra editar metas
function editaMeta($id_meta, $id_usuario, $nomeNovoMeta, $dataFinalNovaMeta, $valorNovoMeta){
    
    $dia = retorna_dia_final_mes(date('m', strtotime($dataFinalNovaMeta)));
    
    $dataFinalNovaMeta = "$dataFinalNovaMeta-$dia";
    
    $update = "UPDATE metas SET nome_meta = '$nomeNovoMeta', data_fim = '$dataFinalNovaMeta', valor = '$valorNovoMeta' WHERE id_meta = '$id_meta' AND id_usuario = '$id_usuario' ";
    mysql_query($update);
    
    $sqlMeta = "SELECT * FROM metas WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";
    $buscaMeta = mysql_query($sqlMeta);
    $retornoMeta = mysql_fetch_array($buscaMeta);
    
    $sqlMetaDetalhada = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' ORDER BY data_fecha_mes DESC LIMIT 1";
    $buscaMetaDetalhada = mysql_query($sqlMetaDetalhada);
    $retornoMetaDetalhada = mysql_fetch_array($buscaMetaDetalhada);
    
    
    ?>
    <script>
           //alert("<?php //echo $retornoMetaDetalhada[data_fecha_mes] ?>");
    </script>
    <?php
    
    //Se as data que termina a meta principal for diferente da data final da meta detalhada
    if($retornoMeta[data_fim] != $retornoMetaDetalhada[data_fecha_mes]){
       
        //Se o cara diminuiu o tempo da meta
        if($retornoMeta[data_fim] < $retornoMetaDetalhada[data_fecha_mes]){
            $delete = "DELETE FROM metas_detalhado WHERE id_meta = '$id_meta' AND data_fecha_mes > '$dataFinalNovaMeta'";
            mysql_query($delete);
        }
        elseif($retornoMeta[data_fim] > $retornoMetaDetalhada[data_fecha_mes]){
            
            $dataFinalAtualMeta = date('Y-m', strtotime($retornoMetaDetalhada[data_fecha_mes]));
            $dataFinalMeta = date('Y-m', strtotime($retornoMeta[data_fim]));
            
            while($dataFinalAtualMeta < $dataFinalMeta){
                $dataFinalAtualMeta = date('Y-m', strtotime("+1 months", strtotime("$dataFinalAtualMeta")));
                $mes = date('m', strtotime($dataFinalAtualMeta));
                $dia = retorna_dia_final_mes($mes);
                $dataInsert = "$dataFinalAtualMeta-$dia";
                
                $insert = "INSERT INTO metas_detalhado (id_meta, data_fecha_mes)
                            VALUES(
                                '$id_meta',
                                '$dataInsert'    
                            )";
    
                mysql_query($insert);
                
                
            }
            
            
        }
       
    }
    
}






//Função que atualiza o saldo inicial da meta
function atualizaSaldoInicialMeta($id_usuario){
    
    $sqlDataInicio = "SELECT * FROM metas WHERE id_usuario = '$id_usuario' and concluida = '0'";
    $busca = mysql_query($sqlDataInicio);
    $retorno = mysql_fetch_array($busca);
    $dataInicio = $retorno[data_inicio];
    $id_meta = $retorno[id_meta];
    
    $mesInicialMeta = date('m', strtotime("$dataInicio"));
    $anoInicialMeta = date('Y', strtotime("$dataInicio"));
    
    $saldoInicialMeta = retornaSaldoInicialMeta($id_usuario, $mesInicialMeta, $anoInicialMeta);
            
    $update = "UPDATE metas_detalhado SET saldo = '$saldoInicialMeta' WHERE id_meta = '$id_meta' AND MONTH(data_fecha_mes) = '$mesInicialMeta' AND YEAR(data_fecha_mes) <= '$anoInicialMeta'";
    mysql_query($update);
    
    return $saldoInicialMeta;
    
}


//Função que retorna o saldo inicial da meta
function retornaSaldoInicialMeta($id_usuario, $mesInicioMeta, $anoInicioMeta){
    $sql = "SELECT * FROM movimentacoes WHERE MONTH(data) <= '$mesInicioMeta' and YEAR(data) <= '$anoInicioMeta' and mov_tipo = 'Receita' and id_usuario = '$id_usuario' and pago_recebido = '1'";
    $busca = mysql_query($sql);
    
    $totalReceitasAntesMeta = 0;
    while ($retorno = mysql_fetch_array($busca)) {
        $totalReceitasAntesMeta += $retorno['valor'];
    }
    
    
    
    $sql2 = "SELECT * FROM movimentacoes WHERE MONTH(data) <= '$mesInicioMeta' and YEAR(data) <= '$anoInicioMeta' and mov_tipo = 'Despesa' and id_usuario = '$id_usuario' and pago_recebido = '1'";
    $busca2 = mysql_query($sql2);
    
    $totalDespesasAntesMeta = 0;
    while ($retorno2 = mysql_fetch_array($busca2)) {
        $totalDespesasAntesMeta += $retorno2['valor'];
    }
    
    
    
    $saldoInicialMeta = $totalReceitasAntesMeta - $totalDespesasAntesMeta;
    
    return $saldoInicialMeta;
}


//Função que verfica se tem meta pro usuário
function usuarioMeta($id_usuario){
    $sql = "SELECT * FROM metas WHERE id_usuario = '$id_usuario' and concluida = '0'";
    $busca = mysql_query($sql);
    $temMeta = mysql_num_rows($busca);
    $retorno = mysql_fetch_array($busca);
    
    if($temMeta == '1'){
        return $retorno;
    }
    else{
        return false;
    }
}


//Função que verifica se tem alguma meta concluida
function retornaSeMetaConcluida($id_usuario){
    $sql = "SELECT * FROM metas WHERE id_usuario = '$id_usuario' and concluida = '1'";
    $busca = mysql_query($sql);
    $temMeta = mysql_num_rows($busca);
    $retorno = mysql_fetch_array($busca);
    
    if($temMeta >= '1'){
        return true;
    }
    else{
        return false;
    }
}


//Função principal da tela de meta, ela gera a tabela de evolução...
function retornaTabelaEvolucaoMesesMeta($id_meta, $id_usuario, $saldoInicialMeta){
    
    //Geramos até o dia 30 do mes atual
    $diaFinalMesAtual = retorna_dia_final_mes(date('m'));
    $dataAtual = date('Y-m-'.$diaFinalMesAtual);
    
    //Buscamos a data inicial da meta
    $sql = "SELECT * FROM metas WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";    
    $busca = mysql_query($sql);
    $retornoMetaPrincipal = mysql_fetch_array($busca);
    //Pegamos só o primeiro mes
//    $mesAnula = date('m', strtotime("$retornoMetaPrincipal[data_inicio]"));
    
    //Seleciona o primeiro mes pra anular
    $sqlAnulaMes = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' order by id_meta_detalhada ASC LIMIT 1";
    $buscaMesAnula = mysql_query($sqlAnulaMes);
    $retornoMesAnula = mysql_fetch_array($buscaMesAnula);
    $mesAnula = $retornoMesAnula[id_meta_detalhada];
    
    //Neste select desconsidemos o primeiro mes, pois no primeiro mes já temos o saldo inicial da meta
    $sql2 = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' and data_fecha_mes <= '$dataAtual' and id_meta_detalhada <> '$mesAnula' order by data_fecha_mes asc";
    $busca2 = mysql_query($sql2);
    $numeroDeLinhas = mysql_num_rows($busca2);
    
    //echo "Saldo inicial da meta: $saldoInicialMeta <br><br>";
//        <tfoot>
//            <tr>
//                <td colspan='$numeroDeLinhas'>Valor da meta: R$ " .number_format($retorno[valor], 2, ',', '.')."</td>
//            </tr>
//        </tfoot>
    
    if($saldoInicialMeta >= 0){
        $dinheiro = "dinheiro_azul";
    }
    else{
        $dinheiro = "dinheiro_vermelho";
    }
    
    $tabelaMeta = "<table>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Saldo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Início em ". $nomeMes = retorna_nome_mes(date('m', strtotime("$retornoMetaPrincipal[data_inicio]")))." de ".date('Y', strtotime("$retornoMetaPrincipal[data_inicio]"))."</td>
                            <td class='$dinheiro'>R$ " .number_format($saldoInicialMeta, 2, ',', '.')."</td>
                        </tr>
                    </tbody>
                    ";
    
    
    //Saldo que fechou o mes recebe o inicio do saldo da meta
    $saldoQueFechouMes = $saldoInicialMeta;
    
    
    while ($retorno = mysql_fetch_array($busca2)) {
        $mesAno = date('Y-m', strtotime("$retorno[data_fecha_mes]"));
        $dataInicio = "$mesAno-01";
        $diaFinal = retorna_dia_final_mes(date('m', strtotime("$retorno[data_fecha_mes]")));
        $dataFim = "$mesAno-$diaFinal";
        
        //echo "$dataInicio - $dataFim <br>";
        
        $totalReceitaMes = "";
        $totalDespesaMes = "";
        $saldoMesAtual = "";
        $dinheiro = "";
        
        $sql_receitasMes = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Receita' and data between '$dataInicio' AND '$dataFim' and pago_recebido ='1' order by data asc";
        $buscaReceitas = mysql_query($sql_receitasMes);
        while($retornoReceitas = mysql_fetch_array($buscaReceitas)){
            $totalReceitaMes += $retornoReceitas['valor']; 
        }
        
        $sql_despesasMes = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Despesa' and data between '$dataInicio' AND '$dataFim' and pago_recebido ='1' order by data asc";
        $buscaDespesas = mysql_query($sql_despesasMes);
        while($retornoDespesas = mysql_fetch_array($buscaDespesas)){
            $totalDespesaMes += $retornoDespesas['valor']; 
        }
        
        $saldoMesAtual = $totalReceitaMes - $totalDespesaMes;
        
        //echo "Quanto gerou de saldo naquele mês? $saldoMesAtual <br>";
        $saldoQueFechouMes += $saldoMesAtual;
        //echo "Quanto eu fechei de saldo real nesse mês? $saldoQueFechouMes <br> <br>";
        
        
        //Atualiza no banco mes a mes
        $update = "UPDATE metas_detalhado SET saldo = '$saldoQueFechouMes' WHERE id_meta_detalhada = '$retorno[id_meta_detalhada]'";
        mysql_query($update);
        
        if($saldoQueFechouMes >= 0){
            $dinheiro = "dinheiro_azul";
        }
        else{
            $dinheiro = "dinheiro_vermelho";
        }
        
        $nomeMes = retorna_nome_mes(date('m', strtotime("$retorno[data_fecha_mes]")));
        $tabelaMeta .= "<tr>
                            <td>".$nomeMes." de ". date('Y', strtotime($retorno[data_fecha_mes]))."</td>
                            <td class='$dinheiro'>R$ ".number_format($saldoQueFechouMes, 2, ',', '.')."</td>
                        </tr>";
        
    }
    
    
    
    $valorMeta = $retornoMetaPrincipal[valor];
    $restante = $valorMeta - $saldoQueFechouMes; 
    
    if($restante < 0){
        $restante = 0;
    }
    
    $tabelaMeta.="<tfoot>
                    <tr>
                        <td colspan='2'><div id='restante'>Restante: R$ ".number_format($restante, 2, ',', '.')."</div></td>
                    </tr>
                    </tfoot>";
    
    $tabelaMeta .= "</table>";
    echo $tabelaMeta;
     
}



//Função praticamente identica, só que ela retorna o saldo que está hoje pra meta pro gráfico
function retornaSaldoAtualParaMeta($id_meta, $id_usuario, $saldoInicialMeta){
        //Geramos até o dia 30 do mes atual
    $diaFinalMesAtual = retorna_dia_final_mes(date('m'));
    $dataAtual = date('Y-m-'.$diaFinalMesAtual);
    
    //Buscamos a data inicial da meta
    $sql = "SELECT * FROM metas WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";    
    $busca = mysql_query($sql);
    $retornoMetaPrincipal = mysql_fetch_array($busca);
    //Pegamos só o primeiro mes
//    $mesAnula = date('m', strtotime("$retornoMetaPrincipal[data_inicio]"));
    
    //Seleciona o primeiro mes pra anular
    $sqlAnulaMes = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' order by id_meta_detalhada ASC LIMIT 1";
    $buscaMesAnula = mysql_query($sqlAnulaMes);
    $retornoMesAnula = mysql_fetch_array($buscaMesAnula);
    $mesAnula = $retornoMesAnula[id_meta_detalhada];
    
    //Neste select desconsidemos o primeiro mes, pois no primeiro mes já temos o saldo inicial da meta
    $sql2 = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' and data_fecha_mes <= '$dataAtual' and id_meta_detalhada <> '$mesAnula' order by data_fecha_mes asc";
    $busca2 = mysql_query($sql2);
    $numeroDeLinhas = mysql_num_rows($busca2);
    
    //echo "Saldo inicial da meta: $saldoInicialMeta <br><br>";
//        <tfoot>
//            <tr>
//                <td colspan='$numeroDeLinhas'>Valor da meta: R$ " .number_format($retorno[valor], 2, ',', '.')."</td>
//            </tr>
//        </tfoot>
   
    //Saldo que fechou o mes recebe o inicio do saldo da meta
    $saldoQueFechouMes = $saldoInicialMeta;
    
    
    while ($retorno = mysql_fetch_array($busca2)) {
        $mesAno = date('Y-m', strtotime("$retorno[data_fecha_mes]"));
        $dataInicio = "$mesAno-01";
        $diaFinal = retorna_dia_final_mes(date('m', strtotime("$retorno[data_fecha_mes]")));
        $dataFim = "$mesAno-$diaFinal";
        
        //echo "$dataInicio - $dataFim <br>";
        
        $totalReceitaMes = "";
        $totalDespesaMes = "";
        $saldoMesAtual = "";
        $dinheiro = "";
        
        $sql_receitasMes = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Receita' and data between '$dataInicio' AND '$dataFim' and pago_recebido ='1' order by data asc";
        $buscaReceitas = mysql_query($sql_receitasMes);
        while($retornoReceitas = mysql_fetch_array($buscaReceitas)){
            $totalReceitaMes += $retornoReceitas['valor']; 
        }
        
        $sql_despesasMes = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Despesa' and data between '$dataInicio' AND '$dataFim' and pago_recebido ='1' order by data asc";
        $buscaDespesas = mysql_query($sql_despesasMes);
        while($retornoDespesas = mysql_fetch_array($buscaDespesas)){
            $totalDespesaMes += $retornoDespesas['valor']; 
        }
        
        $saldoMesAtual = $totalReceitaMes - $totalDespesaMes;
        
        //echo "Quanto gerou de saldo naquele mês? $saldoMesAtual <br>";
        $saldoQueFechouMes += $saldoMesAtual;
        //echo "Quanto eu fechei de saldo real nesse mês? $saldoQueFechouMes <br> <br>";
       
    }
    
    $valorMeta = $retornoMetaPrincipal[valor];
    $restante = $valorMeta - $saldoQueFechouMes; 
    
    return $saldoQueFechouMes;
    
}



//Função praticamente igual a função principal, só que ela retorna o restante pra meta
function retornaRestanteParaMeta($id_meta, $id_usuario, $saldoInicialMeta){
        //Geramos até o dia 30 do mes atual
    $diaFinalMesAtual = retorna_dia_final_mes(date('m'));
    $dataAtual = date('Y-m-'.$diaFinalMesAtual);
    
    //Buscamos a data inicial da meta
    $sql = "SELECT * FROM metas WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";    
    $busca = mysql_query($sql);
    $retornoMetaPrincipal = mysql_fetch_array($busca);
    //Pegamos só o primeiro mes
//    $mesAnula = date('m', strtotime("$retornoMetaPrincipal[data_inicio]"));
    
    //Seleciona o primeiro mes pra anular
    $sqlAnulaMes = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' order by id_meta_detalhada ASC LIMIT 1";
    $buscaMesAnula = mysql_query($sqlAnulaMes);
    $retornoMesAnula = mysql_fetch_array($buscaMesAnula);
    $mesAnula = $retornoMesAnula[id_meta_detalhada];
    
    //Neste select desconsidemos o primeiro mes, pois no primeiro mes já temos o saldo inicial da meta
    $sql2 = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' and data_fecha_mes <= '$dataAtual' and id_meta_detalhada <> '$mesAnula' order by data_fecha_mes asc";
    $busca2 = mysql_query($sql2);
    $numeroDeLinhas = mysql_num_rows($busca2);
    
    //echo "Saldo inicial da meta: $saldoInicialMeta <br><br>";
//        <tfoot>
//            <tr>
//                <td colspan='$numeroDeLinhas'>Valor da meta: R$ " .number_format($retorno[valor], 2, ',', '.')."</td>
//            </tr>
//        </tfoot>
   
    //Saldo que fechou o mes recebe o inicio do saldo da meta
    $saldoQueFechouMes = $saldoInicialMeta;
    
    
    while ($retorno = mysql_fetch_array($busca2)) {
        $mesAno = date('Y-m', strtotime("$retorno[data_fecha_mes]"));
        $dataInicio = "$mesAno-01";
        $diaFinal = retorna_dia_final_mes(date('m', strtotime("$retorno[data_fecha_mes]")));
        $dataFim = "$mesAno-$diaFinal";
        
        //echo "$dataInicio - $dataFim <br>";
        
        $totalReceitaMes = "";
        $totalDespesaMes = "";
        $saldoMesAtual = "";
        $dinheiro = "";
        
        $sql_receitasMes = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Receita' and data between '$dataInicio' AND '$dataFim' and pago_recebido ='1' order by data asc";
        $buscaReceitas = mysql_query($sql_receitasMes);
        while($retornoReceitas = mysql_fetch_array($buscaReceitas)){
            $totalReceitaMes += $retornoReceitas['valor']; 
        }
        
        $sql_despesasMes = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Despesa' and data between '$dataInicio' AND '$dataFim' and pago_recebido ='1' order by data asc";
        $buscaDespesas = mysql_query($sql_despesasMes);
        while($retornoDespesas = mysql_fetch_array($buscaDespesas)){
            $totalDespesaMes += $retornoDespesas['valor']; 
        }
        
        $saldoMesAtual = $totalReceitaMes - $totalDespesaMes;
        
        //echo "Quanto gerou de saldo naquele mês? $saldoMesAtual <br>";
        $saldoQueFechouMes += $saldoMesAtual;
        //echo "Quanto eu fechei de saldo real nesse mês? $saldoQueFechouMes <br> <br>";
       
    }
    
    $valorMeta = $retornoMetaPrincipal[valor];
    $restante = $valorMeta - $saldoQueFechouMes; 
    
    return $restante;
    
}



function retornaQtdMesesRestante($id_meta){
    $sql = "SELECT * FROM metas_detalhado WHERE id_meta = '$id_meta' and saldo IS NULL";
    $busca = mysql_query($sql);
    $retorno = mysql_fetch_array($busca);
    $numeroDeMesesRestantes = mysql_num_rows($busca);
    
    return $numeroDeMesesRestantes;
    
            
}


function excluirMetaUsuario($id_meta, $id_usuario){
    $delete1 = "DELETE FROM metas_detalhado WHERE id_meta = '$id_meta'";
    mysql_query($delete1);
    
    $delete2 = "DELETE FROM metas WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";
    mysql_query($delete2);
    
    header('Location: base.php?p=metas');
}



//Função que marca a meta como concluída
function concluiMeta($id_meta, $id_usuario){
    $sqlBuscaMeta = "SELECT * FROM metas WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";
    $buscaMeta = mysql_query($sqlBuscaMeta);
    $eDesseUsuario = mysql_num_rows($buscaMeta);
    
    if($eDesseUsuario == 1){
        $update = "UPDATE metas SET concluida = '1' WHERE id_meta = '$id_meta' and id_usuario = '$id_usuario'";
        mysql_query($update);
        
        header('Location: base.php?p=metas');
    }
}






//Metas concluidas do usuário
function retornaMetasConluidas($id_usuario){
    $sql = "SELECT * FROM metas WHERE id_usuario = '$id_usuario' AND concluida = '1'";
    $busca = mysql_query($sql);
    
    $temConcluida = mysql_num_rows($busca);
    
    $tabelaMetasConcluidas = "<table>
                              <thead>
                                <tr>
                                    <th>Meta</th>
                                    <th>Data inicial</th>
                                    <th>Data final</th>
                                    <th>Valor</th>
                                    <th></th>
                                </tr>
                              </thead>";
    
    if($temConcluida == '0'){
        
    }
    else{
    
        while($retorno = mysql_fetch_array($busca)){
            $tabelaMetasConcluidas .= "<tbody>
                                            <tr>
                                                <td>$retorno[nome_meta]</td>
                                                <td>".date('d/m/Y', strtotime($retorno[data_inicio]))."</td>
                                                <td>".date('d/m/Y', strtotime($retorno[data_fim]))."</td>    
                                                <td>R$ ".number_format($retorno[valor], 2, ',', '.')."</td>
                                                <td><a href='base.php?p=metas&acao_meta=excluir&id_meta=$retorno[id_meta]'><img src='img/trash9.png' title='Excluir meta'></a></td>
                                            </tr>
                                       </tbody>";
            
        }
        
        $tabelaMetasConcluidas .= "</table>";
        echo $tabelaMetasConcluidas;
        
    }
}



