<?php

function exibe_relatorio_7dias($id_usuario) {

    $data_atual = date('Y-m-d');
    $sete_dias_atras = date('Y-m-d', strtotime("-7 days", strtotime("$data_atual")));
    $mensagem = "<div class='sem_movimentacoes'>Não há movimentações no período.</div>";
    $texto_dinheiro = null;
    $linha_mov_tipo = null;
    $dinheiro = null;
    $dinheiro_saldo = "";
        
    
    //Calculando saldo atual
    $saldo_atual = retorna_saldo_atual($id_usuario);
    if ($saldo_atual >= 0){
        $dinheiro = "dinheiro_azul";
        $dinheiro_saldo = "R$ ";
                    
    }
    else{
         $dinheiro = "dinheiro_vermelho";
         $dinheiro_saldo = "R$ ";
    }
    
    //Calculando saldo anterior
    $saldo7Dias = retornaSaldoAnterior($id_usuario, $sete_dias_atras);
        
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND pago_recebido = 1 and data between '$sete_dias_atras' AND '$data_atual' ORDER BY data ASC";
    $busca = mysql_query($sql);

    $tabela = "<table class='cor_de_fundo'>
                    <thead>
                        <th>Data</th>
                        <th>Movimentação</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                    </thead>
                    <tbody>
                        <tr id='linha_saldo_anterior'>
                            <th>Saldo anterior:</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan='5' class='$dinheiro'>".$dinheiro_saldo.number_format($saldo7Dias, 2, ',', '.')."</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <th>Saldo atual:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class='$dinheiro'>".$dinheiro_saldo.number_format($saldo_atual, 2, ',', '.')."</th>
                    </tfoot>";

    
    $linhas = mysql_num_rows($busca);
    if ($linhas == 0) {
        echo $mensagem;
    } 
    else {
        while ($retorno = mysql_fetch_array($busca)) {
            if($retorno[mov_tipo] == "Receita"){
                $linha_mov_tipo = "linha_receita";
                $dinheiro = "dinheiro_azul";
                $texto_dinheiro = "R$ ";
            }
            //Senão é despesa
            else{
                $linha_mov_tipo = "linha_despesa";
                $dinheiro = "dinheiro_vermelho";
                $texto_dinheiro = "R$ -";
            }
            
            
            $tabela.="<tbody>
                        <tr class='$linha_mov_tipo'>
                            <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                            <td>$retorno[mov_tipo]</td>
                            <td>$retorno[tipo]</td>
                            <td>$retorno[categoria]</td>
                            <td>$retorno[descricao]</td>
                            <td class='$dinheiro'>".$texto_dinheiro.number_format($retorno[valor], 2, ',', '.') . "</td>
                        </tr>
                    </tbody>";
        }
        
        $tabela.="</table>";
        echo $tabela;   
    }
    
}





function exibe_relatorio_30dias($id_usuario) {

    $data_atual = date('Y-m-d');
    $trinta_dias_atras = date('Y-m-d', strtotime("-30 days", strtotime("$data_atual")));
    $mensagem = "<div class='sem_movimentacoes'>Não há movimentações no período.</div>";
    $texto_dinheiro = null;
    $linha_mov_tipo = null;
    $dinheiro = null;
    $dinheiro_saldo = "";
    
    
    
    
    //Calculando saldo atual
    $saldo_atual = retorna_saldo_atual($id_usuario);
    if ($saldo_atual >= 0){
        $dinheiro = "dinheiro_azul";
        $dinheiro_saldo = "R$ ";
                    
    }
    else{
         $dinheiro = "dinheiro_vermelho";
         $dinheiro_saldo = "R$ -";
    }
    
    
    
    //Calculando saldo anterior
    $saldo30Dias = retornaSaldoAnterior($id_usuario, $trinta_dias_atras);
    
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND pago_recebido = 1 and data between '$trinta_dias_atras' AND '$data_atual' ORDER BY data ASC";
    $busca = mysql_query($sql);

    $tabela = "<table class='cor_de_fundo'>
                    <thead>
                        <th>Data</th>
                        <th>Movimentação</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                    </thead>
                    <tbody>
                        <tr id='linha_saldo_anterior'>
                            <th>Saldo anterior:</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan='5' class='$dinheiro'>".$dinheiro_saldo.number_format($saldo30Dias, 2, ',', '.')."</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <th>Saldo atual:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class='$dinheiro'>".$dinheiro_saldo.number_format($saldo_atual, 2, ',', '.')."</th>
                    </tfoot>";

    
    $linhas = mysql_num_rows($busca);
    if ($linhas == 0) {
        echo $mensagem;
    } 
    else {
        while ($retorno = mysql_fetch_array($busca)) {
            if($retorno[mov_tipo] == "Receita"){
                $linha_mov_tipo = "linha_receita";
                $dinheiro = "dinheiro_azul";
                $texto_dinheiro = "R$ ";
            }
            //Senão é despesa
            else{
                $linha_mov_tipo = "linha_despesa";
                $dinheiro = "dinheiro_vermelho";
                $texto_dinheiro = "R$ -";
            }
            
            
            $tabela.="<tbody>
                        <tr class='$linha_mov_tipo'>
                            <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                            <td>$retorno[mov_tipo]</td>
                            <td>$retorno[tipo]</td>
                            <td>$retorno[categoria]</td>
                            <td>$retorno[descricao]</td>
                            <td class='$dinheiro'>".$texto_dinheiro.number_format($retorno[valor], 2, ',', '.') . "</td>
                        </tr>
                    </tbody>";
        }
        
        $tabela.="</table>";
        echo $tabela;   
    }
    
}



function exibe_relatorio_3meses($id_usuario) {

    $data_atual = date('Y-m-d');
    $noventa_dias_atras = date('Y-m-d', strtotime("-90 days", strtotime("$data_atual")));
    $mensagem = "<div class='sem_movimentacoes'>Não há movimentações no período.</div>";
    $texto_dinheiro = null;
    $linha_mov_tipo = null;
    $dinheiro = null;
    $dinheiro_saldo = "";
    
    
    
    
    //Calculando saldo atual
    $saldo_atual = retorna_saldo_atual($id_usuario);
    if ($saldo_atual >= 0){
        $dinheiro = "dinheiro_azul";
        $dinheiro_saldo = "R$ ";
                    
    }
    else{
         $dinheiro = "dinheiro_vermelho";
         $dinheiro_saldo = "R$ -";
    }
    
    //Calculando saldo anterior
    $saldo90Dias = retornaSaldoAnterior($id_usuario, $noventa_dias_atras);
    
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND pago_recebido = 1 and data between '$noventa_dias_atras' AND '$data_atual' ORDER BY data ASC";
    $busca = mysql_query($sql);

    $tabela = "<table class='cor_de_fundo'>
                    <thead>
                        <th>Data</th>
                        <th>Movimentação</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                    </thead>
                    <tbody>
                        <tr id='linha_saldo_anterior'>
                            <th>Saldo anterior:</th>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan='5' class='$dinheiro'>".$dinheiro_saldo.number_format($saldo90Dias, 2, ',', '.')."</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <th>Saldo atual:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class='$dinheiro'>".$dinheiro_saldo.number_format($saldo_atual, 2, ',', '.')."</th>
                    </tfoot>";

    
    $linhas = mysql_num_rows($busca);
    if ($linhas == 0) {
        echo $mensagem;
    } 
    else {
        while ($retorno = mysql_fetch_array($busca)) {
            if($retorno[mov_tipo] == "Receita"){
                $linha_mov_tipo = "linha_receita";
                $dinheiro = "dinheiro_azul";
                $texto_dinheiro = "R$ ";
            }
            //Senão é despesa
            else{
                $linha_mov_tipo = "linha_despesa";
                $dinheiro = "dinheiro_vermelho";
                $texto_dinheiro = "R$ -";
            }
            
            
            $tabela.="<tbody>
                        <tr class='$linha_mov_tipo'>
                            <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                            <td>$retorno[mov_tipo]</td>
                            <td>$retorno[tipo]</td>
                            <td>$retorno[categoria]</td>
                            <td>$retorno[descricao]</td>
                            <td class='$dinheiro'>".$texto_dinheiro.number_format($retorno[valor], 2, ',', '.') . "</td>
                        </tr>
                    </tbody>";
        }
        
        $tabela.="</table>";
        echo $tabela;   
    }  
}


function exibe_relatorio_geral($id_usuario) {

    $mensagem = "<div class='sem_movimentacoes'>Não há movimentações.</div>";
    $texto_dinheiro = null;
    $linha_mov_tipo = null;
    $dinheiro = null;
    $dinheiro_saldo = "";
    
    
    
    
    //Calculando saldo atual
    $saldo_atual = retorna_saldo_atual($id_usuario);
    if ($saldo_atual >= 0){
        $dinheiro = "dinheiro_azul";
        $dinheiro_saldo = "R$ ";
                    
    }
    else{
         $dinheiro = "dinheiro_vermelho";
         $dinheiro_saldo = "R$ ";
    }
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND pago_recebido = 1 ORDER BY data ASC";
    $busca = mysql_query($sql);

    $tabela = "<table class='cor_de_fundo'>
                    <thead>
                        <th>Data</th>
                        <th>Movimentação</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                    </thead>
                    <tfoot>
                        <th>Saldo atual:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class='$dinheiro'>".$dinheiro_saldo.number_format($saldo_atual, 2, ',', '.')."</th>
                    </tfoot>";

    
    $linhas = mysql_num_rows($busca);
    if ($linhas == 0) {
        echo $mensagem;
    } 
    else {
        while ($retorno = mysql_fetch_array($busca)) {
            if($retorno[mov_tipo] == "Receita"){
                $linha_mov_tipo = "linha_receita";
                $dinheiro = "dinheiro_azul";
                $texto_dinheiro = "R$ ";
            }
            //Senão é despesa
            else{
                $linha_mov_tipo = "linha_despesa";
                $dinheiro = "dinheiro_vermelho";
                $texto_dinheiro = "R$ -";
            }
            
            
            $tabela.="<tbody>
                        <tr class='$linha_mov_tipo'>
                            <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                            <td>$retorno[mov_tipo]</td>
                            <td>$retorno[tipo]</td>
                            <td>$retorno[categoria]</td>
                            <td>$retorno[descricao]</td>
                            <td class='$dinheiro'>".$texto_dinheiro.number_format($retorno[valor], 2, ',', '.') . "</td>
                        </tr>
                    </tbody>";
        }
        
        $tabela.="</table>";
        echo $tabela;   
    }  
}

function exibe_relatorio_por_periodo($id_usuario, $data_inicio, $data_fim) {

    $mensagem = "<div class='sem_movimentacoes'>Não há movimentações.</div>";
    $texto_dinheiro = null;
    $linha_mov_tipo = null;
    $dinheiro = null;
    $dinheiro_saldo = "";
    
    
    
    
    //Calculando saldo atual
    $saldo_atual = retorna_saldo_atual($id_usuario);
    if ($saldo_atual >= 0){
        $dinheiro = "dinheiro_azul";
        $dinheiro_saldo = "R$ ";
                    
    }
    else{
         $dinheiro = "dinheiro_vermelho";
         $dinheiro_saldo = "R$ ";
    }
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND data between '$data_inicio' and '$data_fim' AND pago_recebido = 1 ORDER BY data ASC";
    $busca = mysql_query($sql);

    $tabela = "<table class='cor_de_fundo'>
                    <thead>
                        <th>Data</th>
                        <th>Movimentação</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                    </thead>
                    <tfoot>
                        <th>Saldo atual:</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class='$dinheiro'>".$dinheiro_saldo.number_format($saldo_atual, 2, ',', '.')."</th>
                    </tfoot>";

    
    $linhas = mysql_num_rows($busca);
    if ($linhas == 0) {
        echo $mensagem;
    } 
    else {
        while ($retorno = mysql_fetch_array($busca)) {
            if($retorno[mov_tipo] == "Receita"){
                $linha_mov_tipo = "linha_receita";
                $dinheiro = "dinheiro_azul";
                $texto_dinheiro = "R$ ";
            }
            //Senão é despesa
            else{
                $linha_mov_tipo = "linha_despesa";
                $dinheiro = "dinheiro_vermelho";
                $texto_dinheiro = "R$ -";
            }
            
            
            $tabela.="<tbody>
                        <tr class='$linha_mov_tipo'>
                            <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                            <td>$retorno[mov_tipo]</td>
                            <td>$retorno[tipo]</td>
                            <td>$retorno[categoria]</td>
                            <td>$retorno[descricao]</td>
                            <td class='$dinheiro'>".$texto_dinheiro.number_format($retorno[valor], 2, ',', '.') . "</td>
                        </tr>
                    </tbody>";
        }
        
        $tabela.="</table>";
        echo $tabela;   
    }  
}



function retornaSaldoAnterior($id_usuario, $data){
    $sqlReceitas = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND mov_tipo = 'Receita' and pago_recebido = '1' AND data < '$data'";
    $buscaReceitas = mysql_query($sqlReceitas);
    
    $total_receitas_Anterior = 0;
    
    while ($retorno = mysql_fetch_array($buscaReceitas)) {
        $total_receitas_Anterior += $retorno['valor'];
    }
    
    
    $sqlDespesas= "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND mov_tipo = 'Despesa' and pago_recebido = '1' AND data < '$data'";
    $buscaDespesas = mysql_query($sqlDespesas);
    
    $total_despesas_Anterior = 0;
    while ($retorno = mysql_fetch_array($buscaDespesas)) {
        $total_despesas_Anterior += $retorno['valor'];
    }
    
    //echo "Receitas: $total_receitas_Anterior <br>";
    //echo "Despesas: $total_despesas_Anterior <br>";
    
    
    $saldoAntesdosDias = $total_receitas_Anterior - $total_despesas_Anterior;
    
    return $saldoAntesdosDias;
    
}
   
