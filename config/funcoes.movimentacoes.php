<?php

function insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido) {
    $insert = "INSERT INTO movimentacoes (mov_tipo, chave_mov, id_usuario, tipo, categoria, descricao, data, valor, pago_recebido) 
				VALUES (
                                '$mov_tipo',
                                '$chave_mov',
				'$id',
				'$tipo',
				'$categoria',
				'$descricao',
				'$data',
				'$valor',
                                '$recebido'
				)";

    $inseriu = mysql_query($insert);
    if ($inseriu) {
        return TRUE;
    } else {
        return FALSE;
    }
}






function verifica_dia_recebimento($mov_tipo, $chave_mov, $dia_recebimento, $data, $data_atual, $id, $tipo, $categoria, $descricao, $valor, $recebido){

//*****************************************************DIA 28 OU ANTERIOR*************************************************************
    
    if (($dia_recebimento >= 1) && ($dia_recebimento <= 28)) {
        $data = date('Y-m-d', strtotime("$data_atual-$dia_recebimento"));

        $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
    } 
    else {
        //************************************************DIA 29******************************************************************
        //Tratando mês de fevereiro, se o dia do recebimento for 29, todo mês de fevereiro entra dia 28
        if (($dia_recebimento == 29) and ( date("m", strtotime($data)) == 02)) {
            $data = date('Y-m-d', strtotime("-1 day", strtotime("$data_atual-$dia_recebimento")));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        //Se o mês não é fevereiro, mais é dia 28... insere normal
        elseif ($dia_recebimento == 29) {
            $data = date('Y-m-d', strtotime("$data_atual-$dia_recebimento"));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }

//************************************************DIA 30******************************************************************
        //Tratando mês de fevereiro, se o dia do recebimento for 30, todo mês de fevereiro entra dia 28
        if (($dia_recebimento == 30) and ( date("m", strtotime($data)) == 02)) {
            $data = date('Y-m-d', strtotime("-2 days", strtotime("$data_atual-$dia_recebimento")));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        //Se o dia for 30, mais não é fevereiro, faz normal...
        elseif ($dia_recebimento == 30) {
            $data = date('Y-m-d', strtotime("$data_atual-$dia_recebimento"));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }


//**********************************************DIA 31*******************************************************************
        //Tratando dia 31... o mais foda
        //Se o dia de receber é 31, e o mês é fevereiro, insere dia 28
        if (($dia_recebimento == 31) and ( date("m", strtotime($data)) == 02)) {
            $data = date('Y-m-d', strtotime("-3 days", strtotime("$data_atual-$dia_recebimento")));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        //Se o dia é 31, e os meses são Abril, Junho, Setembro ou Novembro.. insere dia 30.
        elseif ((($dia_recebimento == 31) and ( (date("m", strtotime($data))) == '04')) || (($dia_recebimento == 31) and ( (date("m", strtotime($data))) == '06')) || (($dia_recebimento == 31) and ( (date("m", strtotime($data))) == '09')) || (($dia_recebimento == 31) and ( (date("m", strtotime($data))) == '11'))) {

            $data = date('Y-m-d', strtotime("-1 days", strtotime("$data_atual-$dia_recebimento")));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
        //Se o dia é 31, mais é outro mês qualquer... insere normal
        elseif ($dia_recebimento == 31) {
            $data = date('Y-m-d', strtotime("$data_atual-$dia_recebimento"));

            $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, $tipo, $categoria, $descricao, $data, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }
        }
    }
}




//********************************************RECEITA ÚNICA**********************************************************************

function insere_movimentacao_unica($mov_tipo, $chave_mov, $id, $categoria, $descricao, $data_recebimento, $valor, $recebido) {


    #Verificando se já foi recebido
    if ($recebido == 'on') {
        $recebido = 1;
    } 
    else {
        $recebido = 0;
    }

    //Chama função de inserir
    $inseriu = insere_movimentacao($mov_tipo, $chave_mov, $id, 'Única', $categoria, $descricao, $data_recebimento, $valor, $recebido);
            if($inseriu){
                return TRUE;
            }
            else {
                return FALSE;
            }

}





//*****************************************RECEITA FIXA*************************************************************************

/* Função responsável por por inserir receita fixa */
function insere_movimentacao_fixa($mov_tipo, $chave_mov, $id, $categoria, $descricao, $data_fim, $valor, $recebido, $dia_recebimento) {
   
    #Verificando se já foi recebido
    if ($recebido == 'on') {
        $recebido = 1;
    } 
    else {
        $recebido = 0;
    }

    
    $dia_atual = date('d');
    $data_atual = date('Y-m');

    //Parte responsável pelo mês atual.. se o dia de receber for hoje, ou maior que hoje, a grana ainda cai nesse mes.
    //Ex: se hoje é dia 15, e ele recebe dia 20, dia 20 desse mes ele recebe a grana.
    //Ex2: se hoje é dia 15, e ele recebe dia 05, a grana só vai cair no mês que vem
    if ($dia_recebimento >= $dia_atual) {
        
        $data = date('Y-m-d', strtotime("$data_atual-$dia_recebimento"));
        //Chamada da função insere receita
        insere_movimentacao($mov_tipo, $chave_mov, $id, 'Fixa', $categoria, $descricao, $data, $valor, $recebido);
        $recebido = 0;
    }


//Enquanto a data de hoje for menor que a data final do recebimento, vai adicionando um mes no insert.
        while ($data_atual < $data_fim) {

            $data = date('Y-m', strtotime("+1 months", strtotime("$data_atual")));
            $data_atual = date('Y-m', strtotime("+1 months", strtotime("$data_atual")));

            verifica_dia_recebimento($mov_tipo, $chave_mov, $dia_recebimento, $data, $data_atual, $id, 'Fixa', $categoria, $descricao, $valor, $recebido);
            $recebido = 0;
            
        }
        
       
        RETURN true;

    }

//insere_receita_fixa
    
    
    
    
    
/*********************EXIBE MOVIMENTAÇÕES*****************/

function retorna_movimentacoes($id_usuario, $data_inicio, $data_fim) {

    $mensagem = "<h3 class='sem_movimentacoes'>Não há movimentações no período.</h3>";
    
    $imagem_pago_recebido = null;
    $titulo = null;
    $classe_linha = null;
    $dinheiro = null;
    $texto_dinheiro = null;
    $pago_recebido = null;
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND data between '$data_inicio' and '$data_fim' ORDER BY data ASC";
    $busca = mysql_query($sql);

    $tabela = "<table class='cor_de_fundo'>
                    <thead>
                        <th>Data</th>
                        <th>Movimentação</th>
                        <th>Tipo</th>
                        <th>Categoria</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                        <th></th>
                    </thead>";

    $linhas = mysql_num_rows($busca);
    
    if ($linhas == 0) {
        echo $mensagem;
    } 
    else {

        while ($retorno = mysql_fetch_array($busca)) {
            
            
            if ($retorno['pago_recebido'] == 1){
                $imagem_pago_recebido = "like";
                
                if($retorno['mov_tipo'] == "Receita"){
                    $titulo = "Clique aqui para marcar como não recebido.";
                    $classe_linha = "linha_receita";
                    $dinheiro = "dinheiro_azul";
                    $texto_dinheiro = "R$ ";
                    $pago_recebido = 'receita_recebida';
                }
                else{
                    $titulo = "Clique aqui para marcar como não pago.";
                    $classe_linha = "linha_despesa";
                    $dinheiro = "dinheiro_vermelho";
                    $texto_dinheiro = "R$ -";
                    $pago_recebido = 'despesa_paga';
                }
                
            }
            else{
                $imagem_pago_recebido = "dontlike";
                
                if($retorno['mov_tipo'] == "Receita"){
                    $titulo = "Clique aqui para marcar como recebido.";
                    $classe_linha = "linha_receita";
                    $dinheiro = "dinheiro_azul";
                    $texto_dinheiro = "R$ ";
                    $pago_recebido = 'receita_nao_recebida';
                }
                else{
                    $titulo = "Clique aqui para marcar como pago.";
                    $classe_linha = "linha_despesa";
                    $dinheiro = "dinheiro_vermelho";
                    $texto_dinheiro = "R$ -";
                    $pago_recebido = 'despesa_nao_paga';
                }
            }
            
            
            $tabela.="<tbody>
                        <tr class='$classe_linha $pago_recebido'>
                            <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                            <td>$retorno[mov_tipo]</td>
                            <td>$retorno[tipo]</td>
                            <td>$retorno[categoria]</td>
                            <td>$retorno[descricao]</td>
                            <td class='$dinheiro'>".$texto_dinheiro.number_format($retorno[valor], 2, ',', '.') . "</td>
                            <td><a href='http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=".$data_inicio."&dt_ate=".$data_fim."&acao=atualizar&keymov=".$retorno[chave_mov]."&idmov=$retorno[id_movimentacao]'><img src='img/$imagem_pago_recebido.png' title='$titulo'></a> <a href='http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=".$data_inicio."&dt_ate=".$data_fim."&acao=editar_mov&keymov=".$retorno[chave_mov]."&idmov=$retorno[id_movimentacao]'><img src='img/editar2.png' title='Editar movimentação'></a> <a href='http://localhost/GestaTudo/base.php?p=movimentacoes&dt_de=".$data_inicio."&dt_ate=".$data_fim."&acao=excluir_mov&keymov=".$retorno[chave_mov]."&idmov=$retorno[id_movimentacao]'><img src='img/trash9.png' title='Excluir movimentação'></a></td> 
                        </tr>
                    </tbody>";
        }
        $tabela.="</table>";
        echo $tabela;
        
    }
}







function altera_pago_recebido($id_usuario, $id_mov){
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND id_movimentacao = '$id_mov'";
    $busca = mysql_query($sql);
    $retorno = mysql_fetch_array($busca);
    
    if($retorno['pago_recebido'] == 0){
        $sql = "UPDATE movimentacoes SET pago_recebido = 1 WHERE id_usuario = '$id_usuario' AND id_movimentacao = '$id_mov'";
        $busca = mysql_query($sql);
    }
    else{
        $sql = "UPDATE movimentacoes SET pago_recebido = 0 WHERE id_usuario = '$id_usuario' AND id_movimentacao = '$id_mov'";
        $busca = mysql_query($sql);
    }
}






/*Verifica tipo da movimentação*/

function verifica_tipo_movimentacao($id_mov){
    $sql = "SELECT * FROM movimentacoes WHERE id_movimentacao = '$id_mov'";
    $busca = mysql_query($sql);
    $retorno = mysql_fetch_array($busca);
    return $retorno['tipo'];
}




function exclui_movimentacao_unica($id_usuario, $id_mov){
    $delete_mov = "DELETE FROM movimentacoes WHERE id_movimentacao = '$id_mov' and id_usuario = '$id_usuario' ";
    $executa = mysql_query($delete_mov);
    
    if($executa){
        return TRUE;
    }
    else{
        return FALSE;
    }
}



function exclui_proximas_movimentacoes_fixas($id_usuario, $id_mov, $chave_mov){
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND id_movimentacao = '$id_mov' AND chave_mov = '$chave_mov'";
    $busca = mysql_query($sql);
    $retorno = mysql_fetch_array($busca);
//    echo $retorno[data];
    $delete_proximas = "DELETE FROM movimentacoes WHERE chave_mov = '$chave_mov' AND data >= '$retorno[data]'";
    $deletou = mysql_query($delete_proximas);
    
    if($deletou){
        return TRUE;
    }
    else{
        return FALSE;
    }
}



function exclui_todas_movimentacoes_fixas_porChave($id_usuario, $chave_mov){
    $delete_todas = "DELETE FROM movimentacoes WHERE chave_mov = '$chave_mov' and id_usuario = '$id_usuario' ";
    $deletou = mysql_query($delete_todas);
    
    if($deletou){
        return TRUE;
    }
    else{
        return FALSE;
    }
}





function retorna_dados_movimentacao($id_usuario, $id_mov, $chave_mov){
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND chave_mov = '$chave_mov' AND id_movimentacao = '$id_mov'";
    $busca = mysql_query($sql);
    $movimentacao = mysql_fetch_array($busca);
    
    return $movimentacao;
}



function retorna_datafim_mov($id_usuario, $chave_mov){
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND chave_mov = '$chave_mov' ORDER BY data DESC LIMIT 1";
    $busca = mysql_query($sql);
    $retorno = mysql_fetch_array($busca);
    
    return $retorno['data'];
}




/*Função pra alterar movimentações*/
function edita_movimentacao_unica($id_usuario, $id_mov, $chave_mov, $mov_tipo, $categoria, $descricao, $data, $valor){
    $update = "UPDATE movimentacoes SET mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', data = '$data', valor = '$valor' WHERE id_movimentacao = '$id_mov' AND chave_mov = '$chave_mov' and id_usuario = $id_usuario";
    $executa = mysql_query($update);

    if($executa){
        return TRUE;
    }
    else{
        return FALSE;
    }
}  



/*Função pra alterar movimentações fixas*/
function edita_proximas_movimentacoes($id_usuario, $id_mov, $chave_mov, $mov_tipo, $categoria, $descricao, $data, $valor){
    
    if( (date("d", strtotime($data)) == '29') || (date("d", strtotime($data)) == '30') || (date("d", strtotime($data)) == '31')){
        $dia = date("d", strtotime($data));
        edita_movimentacoes_especificas($dia, $id_usuario, $id_mov, $chave_mov, $mov_tipo, $categoria, $descricao, $data, $valor);
    }
    
    else{
        $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND chave_mov = '$chave_mov' and id_movimentacao >= '$id_mov' order by id_movimentacao asc";
        $busca = mysql_query($sql);

        while ($retorno = mysql_fetch_array($busca)){

            $update = "UPDATE movimentacoes SET data = '$data',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
            mysql_query($update);

            //adiciona um mês
            $data = date('Y-m-d', strtotime("+1 months", strtotime("$data")));

        }
    }
    
    RETURN TRUE;
    
}


function edita_movimentacoes_especificas($dia, $id_usuario, $id_mov, $chave_mov, $mov_tipo, $categoria, $descricao, $data, $valor){
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND chave_mov = '$chave_mov' and id_movimentacao >= '$id_mov' order by id_movimentacao_asc";
    $busca = mysql_query($sql);
        
    while ($retorno = mysql_fetch_array($busca)){
        $dataDoBanco = date("Y-m", strtotime($retorno[data]));
        
        
        if ($dia == '29'){
            if( ($dia == '29') && (date("m", strtotime($dataDoBanco)) == '02') ){
                $dia_28 = $dia - 1;
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia_28',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
            elseif ( ($dia == '29') && (date("m", strtotime($dataDoBanco)) != '02') ){
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
        }
        
        
        
        
        if ($dia == '30'){
            if( ($dia == '30') && (date("m", strtotime($dataDoBanco)) == '02') ){
                $dia_28 = $dia - 2;;
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia_28',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
            elseif ( ($dia == '30') && (date("m", strtotime($dataDoBanco)) != '02') ){
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
        }
        
        
        
        
        if ($dia == '31'){
            if( ($dia == '31') && (date("m", strtotime($dataDoBanco)) == '02') ){
                $dia_28 = $dia - 3;
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia_28',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
            
            elseif ( ($dia == '31') && (date("m", strtotime($dataDoBanco)) == '04')  || ($dia == '31') && (date("m", strtotime($dataDoBanco)) == '06') || ($dia == '31') && (date("m", strtotime($dataDoBanco)) == '09') || ($dia == '31') && (date("m", strtotime($dataDoBanco)) == '11')             ){ 
                $dia_30 = $dia - 1;
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia_30',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
            else{
                $update = "UPDATE movimentacoes SET data = '$dataDoBanco-$dia',  mov_tipo = '$mov_tipo', categoria = '$categoria', descricao = '$descricao', valor = '$valor' WHERE id_movimentacao = '$retorno[id_movimentacao]' and chave_mov = '$retorno[chave_mov]' ";
                mysql_query($update);
            }
            
            
        }

    }
    
    
}