<?php

function retorna_contas_a_pagar($id_usuario) {

    $ha_resultados = false;
    $mensagem = "<h4 class='sem_resultados'>Não há contas a pagar.</h4>";
//    $tabela = "<table>
//                    <thead>
//                        <th>Categoria</th>
//                        <th>Descrição</th>
//                        <th>Data</th>
//                        <th>Valor</th>
//                        <th></th>
//                    </thead>";
    
    $diafinal_mes = retorna_dia_final_mes(date('m'));
    $data_final_mes = date('Y-m');
    $data_atual = date('Y-m-d');
    
    //Select das despesas atrasadas
    $sql_atrasadas = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Despesa' and pago_recebido = '0' and data < '$data_atual' order by data asc";
    $busca_despesas_atrasadas = mysql_query($sql_atrasadas);
    $tem_atrasadas = mysql_num_rows($busca_despesas_atrasadas);
    
    //Select das despesas quem vencem hoje
    $sql_hoje = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Despesa' and pago_recebido = '0' and data = '$data_atual' order by data asc";
    $busca_despesas_hoje = mysql_query($sql_hoje);
    $tem_despesas_hoje = mysql_num_rows($busca_despesas_hoje);
    
    //Select das próximas receitas a vencer nesse mes
    
    $sql_proximas = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Despesa' and pago_recebido = '0' and data and data > '$data_atual' and data <= '$data_final_mes-$diafinal_mes' order by data asc";
    $busca_despesas_proximas = mysql_query($sql_proximas);
    $tem_despesas_proximas = mysql_num_rows($busca_despesas_proximas);
    
    
    if($tem_atrasadas != 0){
        $ha_resultados = true;
        $tabela_atrasadas = "<table>";
        
        $tabela_atrasadas .= "<tbody>
                                    <tr>
                                        <td colspan='5' class='atrasadas titulo_contas'>Atrasadas &nbsp;<img src='img/warningRed2.ico'></td>
                                    </tr>
                                </tbody>";
        
        while ($retorno = mysql_fetch_array($busca_despesas_atrasadas)) {          
            $tabela_atrasadas.="<tbody>
                                    <tr class='linha_despesa'>
                                        <td>$retorno[categoria]</td>
                                        <td>$retorno[descricao]</td>
                                        <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                                        <td class='dinheiro_vermelho'>R$ -" . number_format($retorno[valor], 2, ',', '.') . "</td>
                                        <td><a href='config/atualiza_pagto.php?pag_mov=$retorno[id_movimentacao]&mes=".date('m', strtotime($retorno[data]))."&ano=".date('Y', strtotime($retorno[data]))."'><img src='img/dontlike.png' title='Clique para marcar como pago' class='nao_pago'></a></td>
                                    </tr>
                                </tbody>"; 
        }
        
        $tabela_atrasadas .= "</table>";
    }
    
    
    
    
    if($tem_despesas_hoje != 0){
        $ha_resultados = true;
        $tabela_hoje = "<table>";
        $tabela_hoje .= "<tbody>
                                <tr class='espaco_titulo_contas'>
                                    <td colspan='5' class='hoje titulo_contas'>Hoje</td>
                                </tr>
                            </tbody>";
        
        while ($retorno = mysql_fetch_array($busca_despesas_hoje)) {          
            $tabela_hoje .="<tbody>
                                <tr class='linha_despesa'>
                                    <td>$retorno[categoria]</td>
                                    <td>$retorno[descricao]</td>
                                    <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                                    <td class='dinheiro_vermelho'>R$ -" . number_format($retorno[valor], 2, ',', '.') . "</td>
                                    <td><a href='config/atualiza_pagto.php?pag_mov=$retorno[id_movimentacao]&mes=".date('m', strtotime($retorno[data]))."&ano=".date('Y', strtotime($retorno[data]))."'><img src='img/dontlike.png' title='Clique para marcar como pago' class='nao_pago'></a></td>
                                </tr>
                            </tbody>";
        }
         $tabela_hoje .= "</table>";
    }
    

    if($tem_despesas_proximas != 0){
        $ha_resultados = true;
        $tabela_proximas = "<table>";
        $tabela_proximas .= "<tbody>
                                    <tr class='espaco_titulo_contas'>
                                        <td colspan='5' class='proximas titulo_contas'>Próximas</td>
                                    </tr>
                                </tbody>";
        
        while ($retorno = mysql_fetch_array($busca_despesas_proximas)) {          
            $tabela_proximas.="<tbody>
                                <tr class='linha_despesa'>
                                    <td>$retorno[categoria]</td>
                                    <td>$retorno[descricao]</td>
                                    <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                                    <td class='dinheiro_vermelho'>R$ -" . number_format($retorno[valor], 2, ',', '.') . "</td>
                                    <td><a href='config/atualiza_pagto.php?pag_mov=$retorno[id_movimentacao]&mes=".date('m', strtotime($retorno[data]))."&ano=".date('Y', strtotime($retorno[data]))."'><img src='img/dontlike.png' title='Clique para marcar como pago' class='nao_pago'></a></td>
                                </tr>
                            </tbody>";
        }
        
        $tabela_proximas .= "</table>";
    }
    
    //Se não há nenhum resultado retorna a mensagem.
    if ($ha_resultados == false){
        echo $mensagem;
    }
    //Senão retorna a tabela com os resultados.
    else{
        echo "$tabela_atrasadas$tabela_hoje$tabela_proximas";
    }
    
}


function retorna_contas_a_receber($id_usuario) {

    $ha_resultados = false;
    $mensagem = "<h4 class='sem_resultados'>Não há contas a receber.</h4>";
//    $tabela = "<table>
//                    <thead>
//                        <th>Categoria</th>
//                        <th>Descrição</th>
//                        <th>Data</th>
//                        <th>Valor</th>
//                        <th></th>
//                    </thead>";
    
    
    $diafinal_mes = retorna_dia_final_mes(date('m'));
    $data_final_mes = date('Y-m');
    $data_atual = date('Y-m-d');
    
    //Select das despesas atrasadas
    $sql_atrasadas = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Receita' and pago_recebido = '0' and data < '$data_atual'";
    $busca_despesas_atrasadas = mysql_query($sql_atrasadas);
    $tem_atrasadas = mysql_num_rows($busca_despesas_atrasadas);
    
    //Select das despesas quem vencem hoje
    $sql_hoje = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Receita' and pago_recebido = '0' and data = '$data_atual'";
    $busca_despesas_hoje = mysql_query($sql_hoje);
    $tem_despesas_hoje = mysql_num_rows($busca_despesas_hoje);
    
    //Select das próximas receitas a vencer
    $sql_proximas = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' and mov_tipo = 'Receita' and pago_recebido = '0' and data > '$data_atual' and data <= '$data_final_mes-$diafinal_mes'";
    $busca_despesas_proximas = mysql_query($sql_proximas);
    $tem_despesas_proximas = mysql_num_rows($busca_despesas_proximas);
    
    
    if($tem_atrasadas != 0){
        $ha_resultados = true;
        $tabela_atrasadas = "<table>";
        
        $tabela_atrasadas .= "<tbody>
                                    <tr>
                                        <td colspan='5' class='atrasadas titulo_contas'>Atrasadas &nbsp;<img src='img/warningRed2.ico'></td>
                                    </tr>
                                </tbody>";
        
        while ($retorno = mysql_fetch_array($busca_despesas_atrasadas)) {          
            $tabela_atrasadas.="<tbody>
                                    <tr class='linha_despesa'>
                                        <td>$retorno[categoria]</td>
                                        <td>$retorno[descricao]</td>
                                        <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                                        <td class='dinheiro_azul'>R$ " . number_format($retorno[valor], 2, ',', '.') . "</td>
                                        <td><a href='config/atualiza_pagto.php?pag_mov=$retorno[id_movimentacao]&mes=".date('m', strtotime($retorno[data]))."&ano=".date('Y', strtotime($retorno[data]))."'><img src='img/dontlike.png' title='Clique para marcar como pago' class='nao_pago'></a></td>
                                    </tr>
                                </tbody>"; 
        }
        
        $tabela_atrasadas .= "</table>";
    }
    
    
    
    
    if($tem_despesas_hoje != 0){
        $ha_resultados = true;
        $tabela_hoje = "<table>";
        $tabela_hoje .= "<tbody>
                                <tr class='espaco_titulo_contas'>
                                    <td colspan='5' class='hoje titulo_contas'>Hoje</td>
                                </tr>
                            </tbody>";
        
        while ($retorno = mysql_fetch_array($busca_despesas_hoje)) {          
            $tabela_hoje .="<tbody>
                                <tr class='linha_despesa'>
                                    <td>$retorno[categoria]</td>
                                    <td>$retorno[descricao]</td>
                                    <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                                    <td class='dinheiro_azul'>R$ " . number_format($retorno[valor], 2, ',', '.') . "</td>
                                    <td><a href='config/atualiza_pagto.php?pag_mov=$retorno[id_movimentacao]&mes=".date('m', strtotime($retorno[data]))."&ano=".date('Y', strtotime($retorno[data]))."'><img src='img/dontlike.png' title='Clique para marcar como pago' class='nao_pago'></a></td>
                                </tr>
                            </tbody>";
        }
         $tabela_hoje .= "</table>";
    }
    

    if($tem_despesas_proximas != 0){
        $ha_resultados = true;
        $tabela_proximas = "<table>";
        $tabela_proximas .= "<tbody>
                                    <tr class='espaco_titulo_contas'>
                                        <td colspan='5' class='proximas titulo_contas'>Próximas</td>
                                    </tr>
                                </tbody>";
        
        while ($retorno = mysql_fetch_array($busca_despesas_proximas)) {          
            $tabela_proximas.="<tbody>
                                <tr class='linha_despesa'>
                                    <td>$retorno[categoria]</td>
                                    <td>$retorno[descricao]</td>
                                    <td>" . date("d/m/Y", strtotime($retorno[data])) . "</td>
                                    <td class='dinheiro_azul'>R$ " . number_format($retorno[valor], 2, ',', '.') . "</td>
                                    <td><a href='config/atualiza_pagto.php?pag_mov=$retorno[id_movimentacao]&mes=".date('m', strtotime($retorno[data]))."&ano=".date('Y', strtotime($retorno[data]))."'><img src='img/dontlike.png' title='Clique para marcar como pago' class='nao_pago'></a></td>
                                </tr>
                            </tbody>";
        }
        
        $tabela_proximas .= "</table>";
    }
    
    //Se não há nenhum resultado retorna a mensagem.
    if ($ha_resultados == false){
        echo $mensagem;
    }
    //Senão retorna a tabela com os resultados.
    else{
        echo "$tabela_atrasadas$tabela_hoje$tabela_proximas";
    }
    
}


function retorna_saldo_previsto($id_usuario, $mes, $ano) {
    
    
    $saldo_atual = retorna_saldo_atual($id_usuario);
    
    $total_receitas = retorna_total_receitas($id_usuario, $mes, $ano);
    $total_despesas = retorna_total_despesas($id_usuario, $mes, $ano);
    
    $totalPrevisto = ($saldo_atual + $total_receitas) - $total_despesas;
    
    return $totalPrevisto;
}



/**********************FUNÇÕES QUE RETORNAM O TOTAL DE RECEITAS E DESPESAS NO MÊS **************************** */

function retorna_total_receitas($id_usuario, $mes, $ano){
    $sql = "SELECT * FROM movimentacoes WHERE MONTH(data) = '$mes' and YEAR(data) = '$ano' and mov_tipo = 'Receita' and id_usuario = '$id_usuario' and pago_recebido = '0'";
    $busca = mysql_query($sql);
    
    $total_receitas = 0;
    while ($retorno = mysql_fetch_array($busca)) {
        $total_receitas += $retorno['valor'];
    }
    
    return  $total_receitas;
}

function retorna_total_despesas($id_usuario, $mes, $ano){
    $sql = "SELECT * FROM movimentacoes WHERE MONTH(data) = '$mes' and YEAR(data) = '$ano' and mov_tipo = 'Despesa' and id_usuario = '$id_usuario' and pago_recebido = '0'";
    $busca = mysql_query($sql);
    
    $total_despesas = 0;
    while ($retorno = mysql_fetch_array($busca)) {
        $total_despesas += $retorno['valor'];
    }
    
    return $total_despesas;
}

/****************************************************************************** */


/*----Funções do gráfico------ */

function retorna_receitas_mes($id_usuario, $mes, $ano){
    
    $sql = "SELECT * FROM movimentacoes WHERE MONTH(data) = '$mes' and YEAR(data) = '$ano' and mov_tipo = 'Receita' and id_usuario = '$id_usuario' and pago_recebido = '1'";
    $busca = mysql_query($sql);
    
    $total_receitas_mes = 0;
    while ($retorno = mysql_fetch_array($busca)) {
        $total_receitas_mes += $retorno['valor'];
    }
    
    return $total_receitas_mes;
    
}


function retorna_despesas_mes($id_usuario, $mes, $ano){
    
    $sql = "SELECT * FROM movimentacoes WHERE MONTH(data) = '$mes' and YEAR(data) = '$ano' and mov_tipo = 'Despesa' and id_usuario = '$id_usuario' and pago_recebido = '1'";
    $busca = mysql_query($sql);
    
    $total_despesas_mes = 0;
    while ($retorno = mysql_fetch_array($busca)) {
        $total_despesas_mes += $retorno['valor'];
    }
    
    return $total_despesas_mes;
    
}



function verificaMovimentacoesPendentes($id_usuario, $mes_atual, $ano_atual){
    
    $ultimoDia = retorna_dia_final_mes($mes_atual);
    $data = "$ano_atual-$mes_atual-$ultimoDia";
    
    $sql = "SELECT * FROM movimentacoes WHERE id_usuario = '$id_usuario' AND pago_recebido = 0 and data <= '$data'";
    $busca = mysql_query($sql);
    $retorno = mysql_num_rows($busca);
    
    //Se não tem movimentações pendentes retorna false, se tem retorna true
    if($retorno == 0){
        return FALSE;
    }
    else{
        return TRUE;
    }
}